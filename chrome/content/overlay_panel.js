Components.utils.import("chrome://kancolletimer/content/httpobserve.jsm");
Components.utils.import("chrome://kancolletimer/content/utils.jsm");

var __KanColleTimerPanel = {
    update: null,
    _update_bound: null,

    _update_start: function() {
	for (let k in this._update_bound)
	    KanColleDatabase[k].appendCallback(this._update_bound[k]);
    },
    _update_stop: function() {
	for (let k in this._update_bound)
	    KanColleDatabase[k].removeCallback(this._update_bound[k]);
    },

    _update_init: function() {
	if (!this._update_bound) {
	    this._update_bound = {};
	    if (this.update) {
		for (let k in this.update) {
		    let f = this.update[k];
		    let visited = {};   // loop detection
		    while (typeof(f) == 'string' && !visited[f]) {
			visited[f] = true;
			f = this.update[f];
		    }
		    this._update_bound[k] = f.bind(this);
		}
	    }
	}
    },
    _update_exit: function() {
	this._update_bound = null;
    },

    start: function() {
	this._update_start();
    },
    stop: function() {
	this._update_stop();
    },

    init: function() {
	this._update_init();
    },
    exit: function() {
	this._update_exit();
    },
};

var KanColleTimerFleetInfo = {
    debugprint:function(txt){
	KanColleTimerUtils.console.log(txt);
    },

    _ship_status: {},
    _escape: {},
    _slotitem: {},

    _set_escape_ships: function() {
	let that = this;
	let decks = KanColleDatabase.deck.list();
	decks.forEach(function(deckid) {
	    let res = KanColleDatabase.battle.get(deckid) || {};
	    let escape = res.escape || [];
	    that.debugprint('escape[' + deckid + ']: ' + escape.toSource());
	    escape.forEach(function(e,i){
		if (!that._escape[deckid])
		    that._escape[deckid] = [];
		that._escape[deckid][i] = e ? true : false;
	    });
	});
    },

    _discloseBattleResult: function(nowarn) {
	let warnnames_all = [];
	let that = this;
	KanColleDatabase.battle.list().forEach(function(deckid) {
	    let battle = KanColleDatabase.battle.get(deckid);
	    let warn = 0;
	    let warnnames = [];
	    if (!battle.result)
		return;
	    Object.keys(battle.result).forEach(function(i) {
		let res = battle.result[i];
		let ratio = res.cur / res.maxhp;
		// 大破警告
		//  敵艦,旗艦は除外
		//  出撃中なので入渠中ではない
		if (!nowarn && !isNaN(parseInt(deckid, 10)) && i > 1 && ratio <= 0.25) {
		    let shipslot = (that._slotitem[deckid] && that._slotitem[deckid][i]) || [];
		    let escape = (that._escape[deckid] && that._escape[deckid][i]) || false;
		    if (!shipslot[23] && !escape && that._ship_status[deckid][i]) {
			warn++;
			warnnames.push(that._ship_status[deckid][i].name + ' Lv' + that._ship_status[deckid][i].lv);
		    }
		}
	    });
	    if (!isNaN(parseInt(deckid, 10))) {
		if (warn) {
		    let deckname = KanColleDatabase.deck.get(deckid).api_name;
		    that.debugprint('#' + deckid + '(' + warn + '): ' + warnnames.join(','));
		    warnnames_all.push(deckname + 'の' + warnnames.join(','));
		}
	    }
	});
	if (warnnames_all.length && KanColleUtils.getBoolPref('popup.damage-warning')) {
	    this.playSound();
	    KanColleTimerUtils.alert.show(this.imageURL, '艦これタイマー',
				  warnnames_all.join(', ') + 'が大破しています。',
				  false, 'damage-warning', null);
	}
    },

    update: {
	deck: function(){
	    let l = KanColleDatabase.deck.list();

	    for ( let i = 0; i < l.length; i++ ){
		let fi = KanColleDatabase.deck.get(l[i]);
		let id = parseInt(fi.api_id, 10);
		for ( let j = 0; j < fi.api_ship.length; j++ ){
		    let ship_id = fi.api_ship[j];
		    let ship = KanColleDatabase.ship.get(ship_id);
		    let ship_name = "";
		    let ship_escape = (this._escape[id] && this._escape[id][j+1]) || false;
		    let shipslot = {};
		    if (ship) {
			ship_name = KanColleDatabase.masterShip.get(ship.api_ship_id).api_name;
			let ship_slot = JSON.parse(JSON.stringify(ship.api_slot));
			if (ship.api_slot_ex)
			    ship_slot.push(ship.api_slot_ex);
			for (let k = 0; k < ship_slot.length; k++) {
			    let itemid = ship_slot[k];
			    let item;
			    let itemtype;
			    let s = '';

			    if (itemid < 0)
				continue;
			    item = KanColleDatabase.slotitem.get(itemid);
			    itemtype = item ? KanColleDatabase.masterSlotitem.get(item.api_slotitem_id) : null;

			    if (!itemtype)
				continue;

			    shipslot[itemtype.api_type[2]] = shipslot[itemtype.api_type[2]] ?  shipslot[itemtype.api_type[2]] + 1 : 1;
			}
			if (!this._slotitem[id])
			    this._slotitem[id] = {};
			this._slotitem[id][j+1] = shipslot;
		    }
		    if (!this._ship_status[id])
			this._ship_status[id] = {};
		    this._ship_status[id][j+1] = {
			name: ship_name,
			lv: '' + (ship ? ship.api_lv : ''),
		    };
		}
	    }
	},
	ship: 'deck',
	slotitem: 'deck',
	battle: function(data){
	    switch(data) {
	    case -3:	// reset
		this._set_escape_ships();
		break;
	    case -2:	// enemy information (partial)
		break;
	    case -1:	// enemy information
		break;
	    case 0:	// before battle
		this._set_escape_ships();
		this._discloseBattleResult(true);
		break;
	    case 1:	// after battle
		break;
	    case 2:	// battle finished
		this._discloseBattleResult(KanColleDatabase.battle.get('e1').practice);
		break;
	    default:
	    }
	},
    },

    imageURL: "http://pics.dmm.com/freegame/app/854854/200.jpg",

    playSound: function(){
	let path = KanColleUtils.getUnicharPref("sound.damage-warning");
	let IOService;
	let file;
	let uri;
	let sound;

	if (!path)
	    return;

	IOService = Components.classes['@mozilla.org/network/io-service;1'].getService(Components.interfaces.nsIIOService);
	file = KanColleTimerUtils.file.initWithPath(path);
	uri = IOService.newFileURI(file);
	if( KanColleUtils.getIntPref('sound.api') ){
	    // nsisound
	    sound = Components.classes["@mozilla.org/sound;1"].createInstance(Components.interfaces.nsISound);
	    sound.play(uri);
	} else {
	    // html5
	    sound = new Audio(uri.spec);
	    sound.play();
	}
    },

    /**
     * ページロード時の自動処理.
     * @param e DOMイベント
     */
    onPageLoad:function(e){
	let url = e.target.location.href;

	if( url.match(/^http:\/\/[^/]*dmm\.com\/netgame\/.*app_id=854854/) ){
	    this.init();
	    this.start();

	    let auto_open = KanColleUtils.getBoolPref("window.auto-open");
	    if (auto_open)
		KanColleTimerOverlay.open();
	    auto_open = KanColleUtils.getBoolPref("window.auto-open-shiplist");
	    if (auto_open)
		KanColleTimerOverlay.openShipList();
	}
    },

    onPageUnload:function(e){
	let url = e.target.location.href;
	if( url.match(/^http:\/\/[^/]*dmm\.com\/netgame\/.*app_id=854854/) ){
	    this.stop();
	    this.exit();
	}
    },

    init: function(){
	this.debugprint("KanColleTimerFleetInfo.init()");
	this._update_init();
    },
    exit: function() {
	this._update_exit();
	this.debugprint("KanColleTimerFleetInfo.exit()");
    },

    _init: function(_e){
	let appcontent = document.getElementById("appcontent");   // ブラウザ
	if(appcontent){
	    appcontent.addEventListener("DOMContentLoaded",
					function(e){
					    KanColleTimerFleetInfo.onPageLoad(e);
					},true);
	    appcontent.addEventListener("beforeunload",
					function(e){
					    KanColleTimerFleetInfo.onPageUnload(e);
					},true);
	}
	KanColleDatabase.init();
    },

    _exit: function(e){
	KanColleDatabase.exit();
    }
};
KanColleTimerFleetInfo.__proto__ = __KanColleTimerPanel;

window.addEventListener("load", function(e){
    KanColleTimerFleetInfo._init(e);
}, false);
window.addEventListener("unload", function(e){
    KanColleTimerFleetInfo._exit(e);
}, false);
