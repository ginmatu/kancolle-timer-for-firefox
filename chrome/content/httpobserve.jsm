/* -*- mode: js2;-*- */
// vim: set ts=8 sw=4 sts=4 ff=dos :

var EXPORTED_SYMBOLS = ["KanColleRemainInfo", "KanColleDatabase"];

Components.utils.import("chrome://kancolletimer/content/utils.jsm");
Components.utils.import("resource://gre/modules/FileUtils.jsm");

/*
 * Database
 */
//
// タイムスタンプ管理
//
var Timestamp = function() {};
Timestamp.prototype = {
    _ts: 0,
    get: function() { return this._ts; },
    set: function() { this._ts = (new Date).getTime(); return this._ts; },
};

//
// ハンドラ管理
//
var Callback = function(opt) {
    this._cb = [];
    if (opt && opt.match)
	this._match = opt.match;
};
Callback.prototype = {
    _cb: null,
    _match: function(f1, o1, f2, o2) {
	return f1 == f2;
    },
    notify: function(now, data, extradata) {
	for (let i = 0; i < this._cb.length; i++) {
	    let e = this._cb[i];
	    if (e.opt === true || (e.opt && e.opt.compat))
		e.func(Math.floor(now / 1000), data, extradata);
	    else
		e.func(extradata);
	}
    },
    append: function(f, o) {
	this._cb.push({ func: f, opt: o, });
    },
    remove: function(f, o) {
	let count = 0;
	for (let i = 0; i < this._cb.length; i++) {
	    if (this._match(this._cb[i].func, this._cb[i].opt,
			    f, o)) {
		this._cb.splice(i, 1);
		i--;
	    }
	}
	return count;
    },
    flush: function() {
	let count = 0;
	while (this._cb.length > 0) {
	    let e = this._cb.shift();
	    if (e)
		count++;
	}
	return count;
    },
};

//
// データベース
//
var KanColleDB = function() {
    this._init.apply(this, arguments);
};
KanColleDB.prototype = {
    _cb: null,
    _ts: null,
    _req: null,
    _raw: null,
    _db: null,
    _keys: null,
    _pkey: null,

    timestamp: function() { return this._ts.get(); },

    prepare: function(data) {
	this._req = data;
    },

    update: function(data, extradata) {
	let now = this._ts.set();

	this._raw = data;
	this._db = null;
	this._keys = null;

	this._cb.notify(now, data, extradata);
    },

    _parse: function() {
	let hash = {};
	if (!this._raw || this._db)
	    return this._db;
	for (let i = 0; i < this._raw.length; i++)
	    hash[this._raw[i][this._pkey]] = this._raw[i];
	this._db = hash;
	this._keys = null;
	return hash;
    },

    get: function(key) {
	if (key) {
	    let db = this._parse();
	    return db ? db[key] : null;
	}
	return this._raw;
    },

    get_req: function() {
	return this._req;
    },

    _list: function() {
	if (!this._keys)
	    this._keys = Object.keys(this._parse());
	return this._keys;
    },

    list: function() {
	return this._raw ? this._list() : [];
    },

    count: function() {
	return this._raw ? this._list().length : undefined;
    },

    appendCallback: function(f, c) { this._cb.append(f, c); },
    removeCallback: function(f) { this._cb.remove(f); },

    _init: function(opt) {
	this._ts = new Timestamp;
	this._cb = new Callback;
	this._pkey = 'api_' + (opt && opt.primary_key ? opt_primary_key : 'id');
    },
};

//
// 複合データベース
//
var KanColleCombinedDB = function() {
    this._init.apply(this);
};
KanColleCombinedDB.prototype = {
    _cb: null,
    _ts: null,
    _db: null,

    // 更新タイムスタンプの取得
    timestamp: function() { return this._ts; },

    // [オーバーライド可] 依存データベースと処理の列挙。
    // 更新を知りたいデータベースをキーに、処理すべき
    // 関数(配列)を指定する。
    // 各関数の返値は、このデータベースの変更の通知方法。
    // 配列オブジェクトまたは false と評価されるもの。
    // 配列オブジェクトの場合は第0要素をextradataとする。
    // false の場合、通知されない。
    _update: null,

    // 内部実装
    _update_list: null,
    _update_init: function() {
	let _this = this;
	let _update_list;

	if (this._update_list || !this._update)
	    return;

	_update_list = {};
	for (let [k, f] in Iterator(this._update)) {
	    if (!Array.isArray(f))
		f = [ f ];
	    _update_list[k] = f.map(function(_f) {
					return function(extra) {
					    let _ret = _f.apply(_this, [extra]);
					    if (_ret)
						_this._cb.notify.apply(_this._cb, [_this._ts, _this.get()].concat(_ret));
					};
				    });
	};
	this._update_list = _update_list;
    },

    // [オーバーライド可] データベースを得る。
    get: function() { return this._db; },

    // 通知の追加削除
    appendCallback: function(f, c) { this._cb.append(f, c); },
    removeCallback: function(f) { this._cb.remove(f); },

    // 初期化
    init: function() {
	if (this._preinit_hook)
	    this._preinit_hook();
	if (!this._update_list) {
	    this._update_init();
	    for (let [k,fs] in Iterator(this._update_list)) {
		if (!KanColleDatabase[k]) {
		    debugprint('KanColleDatabase["' + k + '"] is not initialized yet.  Please fix it.');
		    continue;
		}
		for (let f of fs)
		    KanColleDatabase[k].appendCallback(f);
	    }
	}
	if (this._postinit_hook)
	    this._postinit_hook();
    },

    // 後始末
    exit: function() {
	if (this._preexit_hook)
	    this._preexit_hook();
	if (this._update_list) {
	    for (let [k,fs] in Iterator(this._update_list)) {
		if (!KanColleDatabase[k])
		    continue;
		for (let f of fs)
		    KanColleDatabase[k].removeCallback(f);
	    }
	    this._update_list = null;
	}
	if (this._postexit_hook)
	    this._postexit_hook();
    },

    // コンストラクタ
    _init: function() {
	this._ts = 0;
	this._cb = new Callback;
    },
};

/**
 * 母港データベース。
 * 主に泊地修理タイマー、資源/士気更新時間を管理する。
 */
var KanCollePortDB = function() {
    this._init();

    this._factor = {
	fuel: 3,
	bullet: 3,
	steel: 3,
	bauxite: 1,
    };

    this._db = {
	repair_timer: Number.NaN,
	now: null,
	last: null,
    };

    this._update = {
	memberBasic: function() {
	    let notify = this._db.repair_timer_notify;
	    delete this._db.repair_timer_notify;
	    return notify;
	},
	material: function() {
	    let that = this;
	    let now = this._db.now;
	    let last = this._db.last && this._db.last.material;
	    let list = KanColleDatabase.material.list();
	    if (!now)
		return;
	    if (!KanColleDatabase.material.timestamp())
		return;
	    this._db.now.material = list.map(function(e) {
		return KanColleDatabase.material.get(e);
	    });
	    if (!last)
		return;
	    this._db.now.material_cycles =
		Math.max.apply(Math, Object.keys(this._db.now.material).map(function(e) {
				let factor = that._factor[e];
				return factor ? Math.ceil((that._db.now.material[e] - that._db.last.material[e]) / factor) : 0;
			       }));
	    this._countcycles();
	},
	ship: function() {
	    let that = this;
	    let now = this._db.now;
	    let last = this._db.last && this._db.last.ship;
	    let list = KanColleDatabase.ship.list();
	    if (!now)
		return;
	    if (!KanColleDatabase.ship.timestamp())
		return;
	    this._db.now.ship = list.map(function(e) {
		let ship = KanColleDatabase.ship.get(e);
		if (!ship)
		    return;
		return {
		    // condと泊地修理
		    api_id: ship.api_id,
		    api_cond: ship.api_cond,
		    api_nowhp: ship.api_nowhp,
		    api_maxhp: ship.api_nowhp,
		};
	    }).filter(function(e) {
		return e !== null;
	    }).reduce(function(p,c) {
		p[c.api_id] = c;
		return p;
	    }, {});
	    if (!last)
		return;
	    this._db.now.ship_cycles =
		Math.max.apply(Math, Object.keys(that._db.now.ship).map(function(e) {
				/* 修理から戻ると cond が 40 になるのを誤認識
				 * するのを避けるため、40未満同士、または 40以上同士
				 * のみに限定
				 */
				if ((that._db.last.ship[e].api_cond < 40 && that._db.last.ship[e].api_cond < 40) ||
				    (that._db.last.ship[e].api_cond >= 40 && that._db.last.ship[e].api_cond >= 40)) {
				    return Math.ceil((that._db.now.ship[e].api_cond - that._db.last.ship[e].api_cond) / 3);
				} else {
				    return 0;
				}
			       }));
	    this._countcycles();
	},
	deck: function(extradata) {
	    let time = (new Date).getTime();
	    // extradata: true => reset repair timer
	    if (!extradata || !extradata.restart_repair_timer)
		return;
	    debugprint('(re)starting repair timer.');
	    this._db.repair_timer = time;
	    return [];
	},
    };

    this._countcycles = function() {
	if (!this._db.now.elapsed_time) {
	    let time_offset = KanColleUtils.getIntPref('time-offset', Number.NaN);
	    let time_sub = !isNaN(time_offset) ? (this._db.last.time - time_offset) % 180000 : 0;
	    let last_time = this._db.last.time - time_sub;
	    this._db.now.elapsed_time = this._db.now.time - last_time;
	    this._db.now.elapsed_cycles = Math.floor(this._db.now.elapsed_time / 180000);
	    this._db.now.elapsed_reminder = this._db.now.elapsed_time % 180000;
	    debugprint('time offset: ' + time_offset);
	    debugprint('Port: ' +
		       this._db.now.elapsed_time + ' msec [' +
		       this._db.now.elapsed_cycles + ' + ' +
		       this._db.now.elapsed_reminder + ' msec]');
	}
	if (isNaN(this._db.now.material_cycles) &&
	    isNaN(this._db.now.ship_cycles)) {
	    this._db.now.cycles = Number.NaN;
	} else {
	    this._db.now.cycles = Math.max(
		isNaN(this._db.now.material_cycles) ? 0 : this._db.now.material_cycles,
		isNaN(this._db.now.ship_cycles) ? 0 : this._db.now.ship_cycles
	    );
	}
	debugprint('cycles = ' + this._db.now.cycles);
	if (this._db.now.cycles > this._db.now.elapsed_cycles) {
	    let time_offset = this._db.now.time % 180000;
	    debugprint('time offset (new): ' + time_offset);
	    KanColleUtils.setIntPref('time-offset', time_offset);
	}
    };

    this.mark = function(path) {
	let now = (new Date).getTime();
	if (path === null) {
	    // port: set
	    let last;
	    //debugprint('set');
	    this._ts = now;
	    last = this._db.now;
	    this._db.now = { time: this._ts };
	    this._db.last = last;
	    // 泊地修理のチェック
	    if (isNaN(this._db.repair_timer)) {
		this._db.repair_timer = now;
		this._db.repair_timer_notify = [];
	    } else {
		debugprint('repair timer = ' + (now - this._db.repair_timer) + 'msec');
		if (now - this._db.repair_timer >= 1200000) {
		    debugprint('reset repair timer.');
		    this._db.repair_timer = now;
		    this._db.repair_timer_notify = [];
		}
	    }
	} else if (path.match(/\/kcsapi\/api_member\/(record|preset_deck|picture_book|payitem|questlist|ndock)/)) {
	    // safe API, just ignore
	} else {
	    // not port: unset
	    //debugprint('reset');
	    this._ts = now;
	    this._db.last = this._db.now = null;
	}
    };

    this.get = function() {
	let data = {};
	if (this._db.last) {
	    data.time = this._db.last.time;
	    data.elapsed_time = this._db.last.elapsed_time;
	    data.elapsed_cycles = this._db.last.elapsed_cycles;
	    data.elapsed_reminder = this._db.last.elapsed_reminder;
	}
	data.repair_timer = this._db.repair_timer;
	return data;
    };
};
KanCollePortDB.prototype = new KanColleCombinedDB();

/**
 * 記録データベース。
 * 司令部レベル、艦船/装備数を管理する。
 */
var KanColleRecordDB = function() {
    this._init();

    this._db = null;

    this._deepcopy = function(force) {
	if (!this._db || force) {
	    let d = KanColleDatabase.memberRecord.get();
	    if (d)
		this._db = JSON.parse(JSON.stringify(d));
	    else {
		this._db = {
		    api_ship: [],
		    api_slotitem: [],
		    api_level: undefined,
		};
	    }
	}
	return this._db;
    };

    this._update = {
	memberRecord: function() {
	    let t = KanColleDatabase.memberRecord.timestamp();

	    this._deepcopy(true);

	    this._ts = t;
	    return [];
	},

	memberBasic: function() {
	    let t = KanColleDatabase.memberBasic.timestamp();
	    let d = KanColleDatabase.memberBasic.get();

	    this._deepcopy();

	    this._db.api_ship[1] = d.api_max_chara;
	    this._db.api_slotitem[1] = d.api_max_slotitem;
	    this._db.api_level = d.api_level;

	    this._ts = t;
	    return [];
	},

	ship: function() {
	    let t = KanColleDatabase.ship.timestamp();
	    let n = KanColleDatabase.ship.count();

	    this._deepcopy();

	    this._db.api_ship[0] = n;

	    this._ts = t;
	    return [];
	},

	slotitem: function() {
	    let t = KanColleDatabase.slotitem.timestamp();
	    let n = KanColleDatabase.slotitem.count();

	    this._deepcopy();

	    this._db.api_slotitem[0] = n;

	    this._ts = t;

	    return [];
	},
    };

    this.get = function() {
	return this._deepcopy();
    };
};
KanColleRecordDB.prototype = new KanColleCombinedDB();

/**
 * 資源データベース。
 * 資源/資材を管理する。
 */
var KanColleMaterialDB = function() {
    this._init();

    this._db = {
	fuel: Number.NaN,
	bullet: Number.NaN,
	steel: Number.NaN,
	bauxite: Number.NaN,
	burner: Number.NaN,
	bucket: Number.NaN,
	devkit: Number.NaN,
	screw: Number.NaN,
    };
    this._log = null;

    this._update = {
	memberBasic: function() {
	    this._readResourceData();
	    return [];
	},
	memberMaterial: function() {
	    let t = KanColleDatabase.memberMaterial.timestamp();
	    let keys = {
		fuel: 1,
		bullet: 2,
		steel: 3,
		bauxite: 4,
		burner: 5,
		bucket: 6,
		devkit: 7,
		screw: 8,
	    };
	    let ok = 0;

	    for (let k in keys) {
		let d = KanColleDatabase.memberMaterial.get(keys[k]);
		if (typeof(d) != 'object' || d == null)
		    continue;
		this._db[k] = d.api_value;
		ok++;
	    }

	    if (!ok)
		return;

	    this._ts = t;
	    this._updateResourceData();
	    return [];
	},

	reqHokyuCharge: function() {
	    let t = KanColleDatabase.reqHokyuCharge.timestamp();
	    let data = KanColleDatabase.reqHokyuCharge.get();

	    if (!this._ts)
		return;

	    this._db.fuel    = data.api_material[0];
	    this._db.bullet  = data.api_material[1];
	    this._db.steel   = data.api_material[2];
	    this._db.bauxite = data.api_material[3];

	    this._ts = t;
	    this._updateResourceData();
	    return [];
	},

	reqNyukyoStart: function() {
	    let t = KanColleDatabase.reqNyukyoStart.timestamp();
	    let req = KanColleDatabase.reqNyukyoStart.get_req();

	    if (!this._ts || isNaN(this._db.bucket) ||
		isNaN(req._highspeed) || !req._highspeed)
		return;

	    this._db.bucket--;

	    this._ts = t;
	    this._updateResourceData();
	    return [];
	},

	reqKousyouCreateShipSpeedChange: function() {
	    let t = KanColleDatabase.reqKousyouCreateShipSpeedChange.timestamp();
	    let req = KanColleDatabase.reqKousyouCreateShipSpeedChange.get_req();
	    let kdock;
	    let delta_burner = 1;

	    if (!this._ts || isNaN(this._db.burner) ||
		isNaN(req._kdock_id))
		return;

	    kdock = KanColleDatabase.kdock.get(req._kdock_id);
	    if (kdock &&
		(kdock.api_item1 >= 1000 || kdock.api_item2 >= 1000 ||
		 kdock.api_item3 >= 1000 || kdock.api_item4 >= 1000 ||
		 kdock.api_item5 > 1)) {
		// 大型建造
		delta_burner = 10;
	    }

	    this._db.burner -= delta_burner;

	    this._ts = t;
	    this._updateResourceData();
	    return [];
	},

	reqKousyouRemodelSlot: function() {
	    let t = KanColleDatabase.reqKousyouRemodelSlot.timestamp();
	    let data = KanColleDatabase.reqKousyouRemodelSlot.get();

	    if (!this._ts || !data)
		return;

	    this._db.fuel    = data.api_after_material[0];
	    this._db.bullet  = data.api_after_material[1];
	    this._db.steel   = data.api_after_material[2];
	    this._db.bauxite = data.api_after_material[3];
	    this._db.burner  = data.api_after_material[4];
	    this._db.bucket  = data.api_after_material[5];
	    this._db.devkit  = data.api_after_material[6];
	    this._db.screw   = data.api_after_material[7];

	    this._ts = t;
	    this._updateResourceData();
	    return [];
	},

	reqNyukyoSpeedChange: function() {
	    let t = KanColleDatabase.reqNyukyoStart.timestamp();

	    if (!this._ts || isNaN(this._db.bucket))
		return;

	    this._db.bucket--;

	    this._ts = t;
	    this._updateResourceData();
	    return [];
	},
    };

    this._updateResourceData = function() {
	let now = Math.floor(KanColleDatabase.material.timestamp() / 1000);
	let res = this.get_log();
	let last_data = res[ res.length-1 ];
	let data = new Object();
	let resnames = {
	    fuel: 1,
	    bullet: 2,
	    steel: 3,
	    bauxite: 4,
	    bucket: 6,
	};
	let count = 0;
	let flush = false;

	if (!now)
	    return;

	for (let k in resnames) {
	    let v = KanColleDatabase.material.get(k);
	    if (isNaN(v))
		continue;
	    data[k] = v;
	    if (!res.length || last_data[k] != data[k])
		count++;
	    if (!res.length || (!last_data[k] && !data[k]) ||
		Math.abs(last_data[k] - data[k]) / Math.max(last_data[k], data[k]) > 0.001)
		flush = true;
	}

	data.recorded_time = now; // 記録日時

	if (count)
	    res.push( data );

	this._flushResourceData(flush);
    };
    this._readResourceData = function(){
	let data = KanColleUtils.readObject('resourcehistory', []);
	let d = this._log;

	if (d) {
	    let t1 = data.length && data[ data.length-1 ].recorded_time;
	    let t2 = d.length && d[ d.length-1 ].recorded_time;
	    if (t2 >= t1)
		return;
	}
	this._log = data;
    };
    this._writeResourceData = function(){
	let data = this._log;
	if( data.length > 15000 ){
	    // 自然回復が一日480回あるので、それを最低1ヶ月分記録するとしたら
	    // 15000件保存できればいいので。
	    data = data.slice(-15000);
	}
	KanColleUtils.writeObject('resourcehistory', data );
    };
    this._flushResourceData = function(forced) {
	let ts_mm = KanColleDatabase.memberMaterial.timestamp(),
	    ts_m = KanColleDatabase.material.timestamp();
	if (!ts_mm || !ts_m || ts_mm != ts_m)
	    return;
	if (!forced && this._last_flush &&
	    ts_m - this._last_flush <= 1800000)
	    return;
	this._last_flush = ts_m;
	this._writeResourceData();
    };

    this.list = function() {
	return Object.keys(this._db);
    };
    this.get = function(key) { return this._db[key]; };
    this.get_log = function() {
	if (!this._log)
	    this._readResourceData();
	return this._log;
    };
};
KanColleMaterialDB.prototype = new KanColleCombinedDB();

/**
 * 艦船データベース。
 * 所持各艦船を管理する。
 * member/ship[23] をもとに、他の操作による更新を追跡。
 * 新たに入手した艦船のファイルへの記録も行う。
 */
var KanColleShipDB = function() {
    this._init();

    this._db = {
	ship: null,
	dead: {},   // 艦船解体/改装時において、消滅する艦船の装備を
		    // 削除する必要がある。艦船を先に削除してしまうと
		    // 装備がわからなくなってしまうので、完全には削除
		    // せず、暫時IDで検索できるようにする。
		    // 次の全体更新で完全削除。
	list: null,
	ndock: null,
    };

    this._deepcopy = function() {
	if (!this._db.ship) {
	    let ships = KanColleDatabase._memberShip2.list();
	    if (!ships)
		return;

	    this._db.ship = new Object;

	    for (let i = 0; i < ships.length; i++)
		this._db.ship[ships[i]] = JSON.parse(JSON.stringify(KanColleDatabase._memberShip2.get(ships[i])));

	    this._db.list = Object.keys(this._db.ship);

	    //debugprint('hash: ' + this._db.ship.toSource());
	    //debugprint('list: ' + this._db.list.toSource());
	}
    };

    this._update = {
	_memberShip2: function() {
	    this._ts = KanColleDatabase._memberShip2.timestamp();
	    this._db.ship = null;
	    this._db.list = null;
	    this._db.dead = {};
	    this._deepcopy();
	    this._save();
	    return [];
	},

	// リクエストに api_shipid が含まれる場合のみ呼ばれる
	//  XXX: リクエストの api_shipid と整合性を確認すべき?
	_memberShip3: function() {
	    let t = KanColleDatabase._memberShip3.timestamp();
	    let data = KanColleDatabase._memberShip3.get();

	    if (!this._ts)
		return;

	    this._deepcopy();

	    for (let i = 0; i < data.length; i++) {
		let ship_id = data[i].api_id;
		this._db.ship[ship_id] = data[i];
	    }

	    this._db.list = null;
	    this._db.dead = {};

	    this._ts = t;
	    this._save();

	    return [];
	},

	reqHokyuCharge: function() {
	    let t = KanColleDatabase.reqHokyuCharge.timestamp();
	    let data = KanColleDatabase.reqHokyuCharge.get().api_ship;

	    if (!this._ts)
		return;

	    this._deepcopy();

	    // Update
	    for (let i = 0; i < data.length; i++) {
		let ship_id = data[i].api_id;
		for (let k in data[i])
		    this._db.ship[ship_id][k] = data[i][k];
	    }

	    this._ts = t;
	    this._save();

	    // Notification
	    return [];
	},

	reqHenseiLock: function() {
	    let t = KanColleDatabase.reqHenseiLock.timestamp();
	    let req = KanColleDatabase.reqHenseiLock.get_req();
	    let data = KanColleDatabase.reqHenseiLock.get();

	    if (!this._ts || isNaN(req._ship_id))
		return;

	    this._deepcopy();

	    this._db.ship[req._ship_id].api_locked = data.api_locked;

	    this._ts = t;
	    this._save();

	    return [];
	},

	reqKaisouPowerup: function() {
	    let t = KanColleDatabase.reqKaisouPowerup.timestamp();
	    let req = KanColleDatabase.reqKaisouPowerup.get_req();

	    if (!this._ts || !req._id_items)
		return;

	    this._deepcopy();

	    this._delete_ships(req._id_items);
	    this._db.list = null;

	    this._ts = t;
	    this._save();

	    return [];
	},

	reqKaisouSlotExchangeIndex: function() {
	    let data = KanColleDatabase.reqKaisouSlotExchangeIndex.get();
	    let t = KanColleDatabase.reqKaisouSlotExchangeIndex.timestamp();
	    let req = KanColleDatabase.reqKaisouSlotExchangeIndex.get_req();
	    let ship;

	    if (!this._ts || isNaN(req._id))
		return;

	    this._deepcopy();

	    ship = this._db.ship[req._id];
	    if (!ship)
		return;

	    ship.api_slot = JSON.parse(JSON.stringify(data.api_slot));

	    this._ts = t;
	    this._save();

	    return [];
	},

	reqKousyouDestroyShip: function() {
	    let t = KanColleDatabase.reqKousyouDestroyShip.timestamp();
	    let req = KanColleDatabase.reqKousyouDestroyShip.get_req();
	    let fleet;

	    if (!this._ts || isNaN(req._ship_id))
		return;

	    this._ts = KanColleDatabase.reqKousyouDestroyShip.timestamp();

	    this._deepcopy();

	    this._delete_ships([req._ship_id]);
	    this._db.list = null;

	    this._ts = t;

	    return [];
	},

	reqKousyouGetShip: function() {
	    let t = KanColleDatabase.reqKousyouGetShip.timestamp();
	    let data = KanColleDatabase.reqKousyouGetShip.get().api_ship;
	    let ship;

	    if (!this._ts)
		return;

	    this._deepcopy();

	    this._db.ship[data.api_id] = data;
	    this._db.list = null;

	    ship = KanColleDatabase.masterShip.get(data.api_ship_id);

	    this.recordNewShip(t,
			       'Created', '',
			       KanColleDatabase.masterStype.get(ship.api_stype).api_name,
			       ship.api_name, '');

	    this._ts = t;
	    this._save();
	    return [];
	},

	reqNyukyoStart: function() {
	    let t = KanColleDatabase.reqNyukyoStart.timestamp();
	    let req = KanColleDatabase.reqNyukyoStart.get_req();
	    let ship;

	    if (!this._ts ||
		isNaN(req._ndock_id) || isNaN(req._ship_id) || isNaN(req._highspeed))
		return;

	    this._deepcopy();

	    ship = this._db.ship[req._ship_id];
	    if (!ship)
		return;

	    // 高速修復 または 1分以下
	    if (req._highspeed || ship.api_ndock_time <= 60000)
		this._repair_ship(ship);

	    this._ts = t;
	    this._save();

	    return [];
	},

	ndock: function() {
	    let t = KanColleDatabase.ndock.timestamp();
	    let notify = false;

	    if (!this._db.ndock) {
		let dock = KanColleDatabase.ndock.list();
		this._db.ndock = {};
		for (let i = 0; i < dock.length; i++) {
		    this._db.ndock[dock[i]] = JSON.parse(JSON.stringify(KanColleDatabase.ndock.get(dock[i])));
		}

		return;
	    }

	    if (!this._ts)
		return;

	    for (let dock in this._db.ndock) {
		let ndock = KanColleDatabase.ndock.get(dock);
		let shipid;
		let ship = null;

		if (ndock &&
		    this._db.ndock[dock].api_state == 1 &&
		    ndock.api_state == 0) {

		    // 修復完了(api_state: 1 => 0)
		    this._deepcopy();

		    shipid = this._db.ndock[dock].api_ship_id;
		    ship = this._db.ship[shipid];
		    if (!ship) {
			debugprint('ship not found: ' + shipid)
			continue;
		    }

		    this._repair_ship(ship);
		    notify = true;
		}

		this._db.ndock[dock] = JSON.parse(JSON.stringify(ndock));
	    }

	    if (!notify)
		return;

	    this._ts = t;
	    this._save();

	    return [];
	},

	battle: function(state) {
	    let that = this;
	    let t = KanColleDatabase.battle.timestamp();
	    let decks = KanColleDatabase.battle.list();
	    let notify = false;

	    if (state != -2)
		return;

	    this._deepcopy();

	    decks.forEach(function(e) {
		let deck = KanColleDatabase.deck.get(e);
		let battle = KanColleDatabase.battle.get(e);
		let ship;

		if (!deck || !battle || !battle.consumed)
		    return;

		ship = that._db.ship[deck.api_ship[0]];
		if (!ship) {
		    debugprint('no ship: ' + deck.api_ship[0]);
		    return;
		}

		Object.keys(battle.consumed).forEach(function(itemid) {
		    if (ship.api_slot_ex == itemid) {
			ship.api_slot_ex = -1;
			notify = true;
		    } else {
			let idx = ship.api_slot.indexOf(itemid);
			if (idx >= 0) {
			    ship.api_slot = ship.api_slot.splice(idx, 1);
			    ship.api_slot.push(-1);
			    notify = true;
			}
		    }
		    if (notify)
			ship.api_nowhp = battle.consumed[itemid].nowhp;
		});
	    });

	    if (!notify)
		return;

	    this._ts = t;
	    this._save();

	    return [];
	},

	reqMemberGetIncentive: function() {
	    if (!this._load())
		return;
	    return [];
	},
    };

    this._delete_ships = function(ids) {
	let count = 0;
	if (ids) {
	    for (let i = 0; i < ids.length; i++) {
		let id = ids[i];
		if (!this._db.ship[id])
		    continue;
		debugprint('deleting ship: ' + id);
		this._db.dead[id] = this._db.ship[id];
		delete(this._db.ship[id]);
		count++;
	    }
	}
	return count;
    };

    this._repair_ship = function(ship) {
	// HP, Condなどを補正
	ship.api_nowhp = ship.api_maxhp;
	ship.api_ndock_time = 0;
	for (let i = 0; i < ship.api_ndock_item.length; i++)
	    ship.api_ndock_item[i] = 0;
	if (ship.api_cond < 40)
	    ship.api_cond = 40;
    };

    this._recordNewShip = function(time, area, enemy, stype_name, ship_name, win_rank) {
	let file = KanColleUtils.getDataFile('getship.dat');
	let writer = KanColleTimerUtils.file.openWriter(file,
							FileUtils.MODE_WRONLY|FileUtils.MODE_CREATE|FileUtils.MODE_APPEND,
							0);
	let line = [ area, enemy, stype_name, ship_name, time / 1000, win_rank ]
		    .map(function(s) {
			s = ("" + s).replace(/[\r\n]/g, ' ');
			if (s.match(/[",]/)) {
			    return '"' + s.replace(/"/g, '""') + '"';
			} else
			    return s;
		    })
		    .join(',');
	writer.writeString(line + '\n');
	writer.close();
    };
    this.recordNewShip = function(time, area, enemy, stype_name, ship_name, win_rank) {
	if (!KanColleUtils.getBoolPref('record.ships'))
	   return;
	this._recordNewShip(time, area, enemy, stype_name, ship_name, win_rank);
    };

    this._save = function() {
	let that = this;
	let hash;

	if (!this._ts)
	    return;

	// shipは大きすぎるので、DeckDB, NdockDB に
	// 関連するものに限定する
	hash = Object.keys(this._db.ship)
	    .map(function(e) { return that._db.ship[e]; })
	    .filter(function(e) {
		// Check DeckDB
		if (KanColleDatabase.deck.lookup(e.api_id))
		    return true;
		// Check Ndock
		if (KanColleDatabase.ndock.find(e.api_id))
		    return true;
		return false;
	    }).reduce(function(p,c) {
		p[c.api_id] = c;
		return p;
	    }, {});

	debugprint('ship = ' + (Object.keys(this._db.ship).length) + ', draft = ' + (Object.keys(hash).length));

	KanColleUtils.writeObject('ship', {
	    hash: hash,
	    ndock: this._db.ndock,
	    _ts: this._ts,
	});
    };

    this._load = function() {
	let obj = KanColleUtils.readObject('ship', null);
	if (!obj)
	    return;
	// We will get real data later.
	this._db.ship = null;
	this._db.dead = obj.hash;
	this._db.list = null;
	this._db.fleet = {};
	this._db.ndock = obj.ndock;
	// We do NOT update timestamp here.
	//this._ts = obj._ts;
	return true;
    };

    this.get = function(id, key) {
	if (key == null) {
	    let ret;
	    if (this._db.ship) {
		ret = this._db.ship[id];
		if (!ret)
		    ret = this._db.dead[id];
	    } else
		ret = KanColleDatabase._memberShip2.get(id);
	    return ret;
	}
    };

    this.list = function() {
	if (!this._db.ship)
	    return KanColleDatabase._memberShip2.list();
	if (!this._db.list)
	    this._db.list = Object.keys(this._db.ship);
	return this._db.list;
    };

    this.count = function() {
	if (!this._db.ship)
	    return KanColleDatabase._memberShip2.count();
	return this.list().length;
    };
};
KanColleShipDB.prototype = new KanColleCombinedDB();

/**
 * デッキデータベース。
 * 主に艦隊編成、遠征状態を管理。
 */
var KanColleDeckDB = function() {
    this._init();

    this._db = {
	fleet: {},
	deck: null,
	list: null,
	timer: {},
    };
    this._timer = null;

    this._update_fleet = function() {
	let db = {};
	let ids = this.list();
	for (let i = 0; i < ids.length; i++) {
	    let deck = this.get(ids[i]);
	    for (let j = 0; j < deck.api_ship.length; j++) {
		if (deck.api_ship[j] < 0)
		    continue;
		db[deck.api_ship[j]] = {
		    fleet: deck.api_id,
		    pos: j,
		};
	    }
	}
	this._db.fleet = db;
    };

    this._check_repair_ship = function(deckid) {
	let deck = this._db.deck[deckid];
	let shipid = deck.api_ship[0];
	let ship = shipid >= 0 ? KanColleDatabase.ship.get(shipid) : null;
	let shiptype = ship ? KanColleDatabase.masterShip.get(ship.api_ship_id) : null;
	if (!shiptype)
	    return false;
	return shiptype.api_stype == 19;	//工作艦
    };

    this._deepcopy = function() {
	if (!this._db.deck) {
	    let decks = KanColleDatabase.memberDeck.list();
	    if (!decks)
		return;

	    this._db.deck = new Object;

	    for (let i = 0; i < decks.length; i++)
		this._db.deck[decks[i]] = JSON.parse(JSON.stringify(KanColleDatabase.memberDeck.get(decks[i])));

	    this._db.list = Object.keys(this._db.deck);

	    //debugprint('hash: ' + this._db.deck.toSource());
	    //debugprint('list: ' + this._db.list.toSource());
	}
    };

    this._update = {
	memberDeck: function() {
	    this._ts = KanColleDatabase.memberDeck.timestamp();
	    this._db.deck = null;
	    this._db.list = null;
	    this._update_fleet();
	    this._deepcopy();
	    this._update_timer(this._ts);
	    this._save();
	    return [{}];
	},
	__memberDeck: function() {
	    let decks;

	    if (!this._ts)
		return;

	    this._ts = KanColleDatabase.__memberDeck.timestamp();
	    this._deepcopy();
	    this._db.list = null;

	    decks = KanColleDatabase.__memberDeck.list();
	    for (let i = 0; i < decks.length; i++) {
		let deck = KanColleDatabase.__memberDeck.get(decks[i]);
		this._db.deck[deck.api_id] = deck;
	    }

	    this._update_fleet();
	    this._update_timer(this._ts);
	    this._save();
	    return [{}];
	},
	reqHenseiChange: function() {
	    let notify_data = {};
	    let req = KanColleDatabase.reqHenseiChange.get_req();
	    let deck;

	    if (!this._ts ||
		isNaN(req._id) || isNaN(req._ship_id) || isNaN(req._ship_idx))
		return;

	    this._ts = KanColleDatabase.reqHenseiChange.timestamp();

	    this._deepcopy();

	    // 編成
	    //	req._id: 艦隊ID(or -1)
	    //	req._ship_idx: 艦隊でのindex(or -1)
	    //	req._ship_id: 変更後のID(or -1 or -2)

	    deck = this._db.deck[req._id];
	    if (!deck)
		return;

	    if (req._ship_id == -2) {
		// 随伴艦解除
		for (let i = 1; i < deck.api_ship.length; i++)
		    deck.api_ship[i] = -1;
	    } else if (req._ship_id == -1) {
		// 解除
		deck.api_ship.splice(req._ship_idx, 1);
		deck.api_ship.push(-1);
		if (this._check_repair_ship(req._id))
		    notify_data.restart_repair_timer = true;
	    } else if (req._ship_id >= 0) {
		// 配置換え

		// 現在艦隊に所属する艦船ID
		let ship_id = deck.api_ship[req._ship_idx];
		// 配置しようとする艦の所属艦隊
		let ship_fleet = req._ship_id >= 0 ? KanColleDatabase.deck.lookup(req._ship_id) : null;

		// 新しい艦を配置
		deck.api_ship[req._ship_idx] = req._ship_id;
		if (ship_fleet) {
		    // 所属元艦隊での処理
		    let odeck = this._db.deck[ship_fleet.fleet];
		    if (ship_id >= 0) {
			// 配置
			odeck.api_ship[ship_fleet.pos] = ship_id;
		    } else {
			// 解除
			odeck.api_ship.splice(ship_fleet.pos, 1);
			odeck.api_ship.push(-1);
		    }
		    if (ship_fleet.fleet != req._id &&
			this._check_repair_ship(ship_fleet.fleet)) {
			notify_data.restart_repair_timer = true;
		    }
		}
		if (this._check_repair_ship(req._id))
		    notify_data.restart_repair_timer = true;
	    }
	    this._update_fleet();
	    this._save();
	    return [notify_data];
	},
	reqHenseiPresetSelect: function() {
	    let t = KanColleDatabase.reqHenseiPresetSelect.timestamp();
	    let req = KanColleDatabase.reqHenseiPresetSelect.get_req();
	    let data = KanColleDatabase.reqHenseiPresetSelect.get();
	    let d;

	    if (!this._ts || isNaN(req._deck_id))
		return;

	    this._deepcopy();

	    this._db.deck[req._deck_id] = JSON.parse(JSON.stringify(data));

	    this._ts = t;
	    this._update_fleet();

	    this._save();
	    return [{}];
	},
	reqKousyouDestroyShip: function() {
	    let req = KanColleDatabase.reqKousyouDestroyShip.get_req();
	    let fleet;

	    if (!this._ts || isNaN(req._ship_id))
		return;

	    fleet = KanColleDatabase.deck.lookup(req._ship_id);
	    if (!fleet)
		return;

	    this._ts = KanColleDatabase.reqKousyouDestroyShip.timestamp();

	    this._deepcopy();
	    this._db.deck[fleet.fleet].api_ship.splice(fleet.pos,1);
	    this._db.deck[fleet.fleet].api_ship.push(-1);

	    this._update_fleet();
	    this._save();
	    return [{}];
	},
	reqMemberUpdateDeckName: function() {
	    let req = KanColleDatabase.reqMemberUpdateDeckName.get_req();
	    let deck;

	    if (!this._ts || isNaN(req._deck_id))
		return;

	    this._deepcopy();

	    deck = this.get(req._deck_id);
	    if (!deck)
		return;

	    deck.api_name = req.api_name;

	    this._ts = KanColleDatabase.reqMemberUpdateDeckName.timestamp();
	    this._save();
	    return [{}];
	},
	reqMissionReturnInstruction: function() {
	    let req = KanColleDatabase.reqMissionReturnInstruction.get_req();
	    let deck;
	    let d;

	    if (!this._ts || isNaN(req._deck_id))
		return;

	    d = KanColleDatabase.reqMissionReturnInstruction.get();

	    this._deepcopy();

	    deck = this.get(req._deck_id);
	    if (!deck)
		return;

	    deck.api_mission = JSON.parse(JSON.stringify(d.api_mission));

	    this._ts = KanColleDatabase.reqMissionReturnInstruction.timestamp();
	    this._update_timer(this._ts);

	    this._save();

	    return [{}];
	},

	reqMemberGetIncentive: function() {
	    if (!this._load())
		return;
	    return [{ init: true }];
	},
    };

    this._update_timer = function(now) {
	let decks = this.list();
	let next = Number.POSITIVE_INFINITY;
	debugprint('mission: update timers');
	for (let deck of decks) {
	    let d = this.get(deck);
	    let k = d.api_id;
	    let state = this._db.timer[k] || 0;
	    let t;

	    if (d.api_mission[0] > 0)
		t = d.api_mission[2] - now;
	    else
		t = Number.NaN;

	    debugprint('mission: deck = ' + deck + ', state = ' + state + ', d.api_mission[0] = ' + d.api_mission[0] + ', complete_time = ' + d.api_mission[2] + ', t = ' + t);

	    if (isNaN(t))
		state = -1;
	    else if (t <= 0) {
		if (state < 3) {
		    // notify
		    state = 3;
		    this._timer_notify(k, state);
		}
	    } else if (t <= 60000) {
		if (state < 2) {
		    // notify
		    state = 2;
		    this._timer_notify(k, state);
		}
		if (next > t)
		    next = t;
	    } else {
		if (state < 1) {
		    state = 1;
		    this._timer_notify(k, state);
		}
		if (next > t - 60000)
		    next = t - 60000;
	    }
	    if (this._db.timer[k] != state)
		debugprint('mission: => state = ' + state);
	    this._db.timer[k] = state;
	}
	this._stop_timer();
	debugprint('mission: now = ' + now + ', next = ' + next);
	this._start_timer(now + next)
    };

    this._init_timer = function() {
    };
    this._start_timer = function(next) {
	if (next != Number.POSITIVE_INFINITY)
	    this._timer = KanColleTimerUtils.timer.startDelayedEventAt(this, next);
    };
    this._stop_timer = function() {
	KanColleTimerUtils.timer.stop(this._timer);
	this._timer = null;
    };
    this._timer_notify = function(id, state) {
	debugprint('mission: notify(deck = ' + id + ', state = ' + state + ')');
	//0        1  2    3  4  5    6        7          8  9    10
	//まもなく|第|艦隊|が|の|遠征|していま|が完了しま|す|した|。
	const _m = '\u307e\u3082\u306a\u304f|\u7b2c|\u8266\u968a|\u304c|\u306e|\u9060\u5f81|\u3057\u3066\u3044\u307e|\u304c\u5b8c\u4e86\u3057\u307e|\u3059|\u3057\u305f|\u3002'.split(/\|/);
	let msg = null;
	let popup = KanColleUtils.getBoolPref('popup.mission');
	let sound = null;
	let deck = this.get(id);
	let deckname = deck ? deck.api_name : (_m[1] + id + _m[2]);
	switch (state) {
	case 3:
	    msg =         deckname         + _m[4] + _m[5]         + _m[7]         + _m[9] + _m[10];
	    sound = KanColleUtils.getUnicharPrefFileURI('sound.mission');
	    break;
	case 2:
	    msg = _m[0] + deckname         + _m[4] + _m[5]         + _m[7] + _m[8]         + _m[10];
	    sound = KanColleUtils.getUnicharPrefFileURI('sound.1min.mission');
	    popup = popup && KanColleUtils.getBoolPref('popup.1min-before');
	    break;
	case 1:
	    msg =         deckname + _m[3]         + _m[5] + _m[6]         + _m[8]         + _m[10];
	    popup = false;
	    sound = null;
	    break;
	default:
	    return;
	}
	debugprint('mission: popup = ' + popup + ', sound = ' + (sound ? sound.spec : '') + ': ' + msg);
	if (popup)
	    KanColleTimerUtils.alert.show(KanColleUtils.IconURL, KanColleUtils.Title, msg, false, msg, null);
	if (sound)
	    KanColleTimerUtils.sound.play(sound);
	this._cb.notify(this._ts, this.get(), { state: state, message: msg, });
    };

    this.observe = function(subject, topic, data) {
	switch (topic) {
	case 'timer-callback':
	    this._update_timer((new Date).getTime());
	    break;
	default:;
	}
    };

    this._save = function() {
	if (!this._ts)
	    return;
	KanColleUtils.writeObject('deck', {
	    hash: this._db.deck,
	    _ts: this._ts,
	});
    };

    this._load = function() {
	let obj = KanColleUtils.readObject('deck', null);
	if (!obj)
	    return;
	this._db.deck = obj.hash;
	this._db.list = null;
	this._db.fleet = null;
	this._db.timer = {};
	this._ts = obj._ts;
	this._update_fleet();
	this._update_timer(this._ts);
	return true;
    };

    this.lookup = function(ship_id) {
	return this._db.fleet[ship_id];
    };

    this.get = function(id, key) {
	if (key == null) {
	    return this._db.deck ? this._db.deck[id] : KanColleDatabase.memberDeck.get(id);
	}
    };

    this.list = function() {
	if (!this._db.deck)
	    return KanColleDatabase.memberDeck.list();
	if (!this._db.list)
	    this._db.list = Object.keys(this._db.deck);
	return this._db.list;
    };

    this.count = function() {
	return this._db.deck ? this._db.list.length : KanColleDatabase.memberDeck.count();
    };

    this._postinit_hook = function() {
	this._init_timer();
    };
    this._preexit_hook = function() {
	this._stop_timer();
    };
};
KanColleDeckDB.prototype = new KanColleCombinedDB();

/**
 * 装備データベース。
 * 各所持装備を管理。強化状態毎などに分類。
 */
var KanColleSlotitemDB = function() {
    this._init();

    this._stat = {
	create: 0,
	destroy: 0,
    };

    this._db = {
	owner: {},
	hash: null,
	list: null,
    };

    this._levelkeys = [
	{ key: 'api_level', label: '*', },
	{ key: 'api_alv',   label: '+', },
    ];

    this._deepcopy = function() {
	if (!this._db.hash) {
	    let ids = KanColleDatabase._memberSlotitem.list();
	    if (!ids)
		return;

	    this._db.hash = new Object;

	    for (let i = 0; i < ids.length; i++)
		this._db.hash[ids[i]] = JSON.parse(JSON.stringify(KanColleDatabase._memberSlotitem.get(ids[i])));

	    this._db.list = Object.keys(this._db.hash);

	    //debugprint('hash: ' + this._db.hash.toSource());
	    //debugprint('list: ' + this._db.list.toSource());
	}
    };

    this._shipname = function(ship_id) {
	try{
	    // member/ship2 には艦名がない。艦艇型から取得
	    let ship = KanColleDatabase.ship.get(ship_id);
	    let shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
	    return shiptype.api_name;
	} catch (x) {
	}
	return "";
    },

    this._update_owner = function() {
	let db = {};
	let items;
	let ships;

	if (!this._ts || !KanColleDatabase.ship.timestamp() ||
	    !KanColleDatabase.masterSlotitem.timestamp())
	    return -1;

	items = KanColleDatabase.slotitem.list();
	ships = KanColleDatabase.ship.list();

	for (let i = 0; i < items.length; i++) {
	    let item = KanColleDatabase.slotitem.get(items[i]);
	    let itemtypeid = item.api_slotitem_id;
	    let itemtype = KanColleDatabase.masterSlotitem.get(itemtypeid);
	    if (!db[itemtypeid]) {
		db[itemtypeid] = {
				    id: itemtypeid,
				    name: itemtype.api_name,
				    type: itemtype.api_type,
				    list: {},
				    totalnum: 0,
				    num: 0,
				    lv: {},
		};
		for (let ki = 0; ki < this._levelkeys.length; ki++) {
		    db[itemtypeid].lv[this._levelkeys[ki].key] = [];
		}
	    }
	    db[itemtypeid].totalnum++;

	    for (let ki = 0; ki < this._levelkeys.length; ki++) {
		let key = this._levelkeys[ki].key;
		let itemlv = item[key] || 0;
		if (!db[itemtypeid].lv[key][itemlv]) {
		    db[itemtypeid].lv[key][itemlv] = {
			list: {},
			totalnum: 0,
			num: 0,
		    };
		}
		db[itemtypeid].lv[key][itemlv].totalnum++;
	    }
	}

	for (let i = 0; i < ships.length; i++) {
	    let ship = KanColleDatabase.ship.get(ships[i]);
	    let ship_slot = JSON.parse(JSON.stringify(ship.api_slot));
	    if (ship.api_slot_ex)
		ship_slot.push(ship.api_slot_ex);

	    //debugprint(this._shipname(ship.api_id) + ': ');

	    for (let j = 0; j < ship_slot.length; j++) {
		let item;
		let itemtypeid;

		if (ship_slot[j] < 0)
		    continue;

		item = KanColleDatabase.slotitem.get(ship_slot[j]);
		// member/slotitem might be out-of-date for a while.
		if (!item)
		    return -1;

		itemtypeid = item.api_slotitem_id;

		db[itemtypeid].list[ship.api_id]++;
		db[itemtypeid].num++;

		for (let ki = 0; ki < this._levelkeys.length; ki++) {
		    let key = this._levelkeys[ki].key;
		    let itemlv = item[key] || 0;

		    //debugprint(itemtypeid + ': ' + itemtype.api_name + ': ' + key + '=' + itemlv);

		    db[itemtypeid].lv[key][itemlv].list[ship.api_id]++;
		    db[itemtypeid].lv[key][itemlv].num++;
		}
	    }
	}

	for ( let k in db ){
	    let s = [];
	    for ( let l in db[k].list ){
		s.push(this._shipname(parseInt(l, 10)));
	    }
	    //debugprint(db[k].name + ': ' + s.join(','));
	}

	//debugprint(db.toSource());
	this._db.owner = db;

	return 0;
    };

    this._update = {
	ship: function() {
	    let t = KanColleDatabase.ship.timestamp();

	    if (!this._ts)
		return;

	    if (this._update_owner() < 0)
		return;

	    this._ts = t;
	    return [];
	},

	_memberSlotitem: function() {
	    let t = KanColleDatabase._memberSlotitem.timestamp();
	    this._db.hash = null;
	    this._db.list = null;
	    this._ts = t;
	    this._update_owner();
	    return [];
	},

	reqKaisouPowerup: function() {
	    let t = KanColleDatabase.reqKaisouPowerup.timestamp();
	    let req = KanColleDatabase.reqKaisouPowerup.get_req();

	    if (!this._ts || !req._id_items)
		return;

	    this._deepcopy();

	    for (let i = 0; i < req._id_items.length; i++)
		this._delete_ship(req._id_items[i]);

	    this._db.list = Object.keys(this._db.hash);

	    this._ts = t;
	    this._update_owner();
	    return [];
	},

	reqKousyouCreateItem: function() {
	    let t = KanColleDatabase.reqKousyouCreateItem.timestamp();
	    let data = KanColleDatabase.reqKousyouCreateItem.get();
	    let slotitem = data.api_slot_item;

	    if (!this._ts || !slotitem)
		return;

	    this._deepcopy();
	    this._db.hash[slotitem.api_id] = slotitem;
	    this._db.list = Object.keys(this._db.hash);

	    this._stat.create++;

	    this._ts = t;
	    this._update_owner();
	    return [];
	},

	reqKousyouDestroyItem2: function() {
	    let t = KanColleDatabase.reqKousyouDestroyItem2.timestamp();
	    let req = KanColleDatabase.reqKousyouDestroyItem2.get_req();

	    if (!this._ts || !req._slotitem_ids)
		return;

	    this._deepcopy();

	    this._delete_slotitems(req._slotitem_ids);

	    this._db.list = Object.keys(this._db.hash);

	    this._stat.destroy++;

	    this._ts = t;
	    this._update_owner();
	    return [];
	},

	reqKousyouDestroyShip: function() {
	    let t = KanColleDatabase.reqKousyouDestroyShip.timestamp();
	    let req = KanColleDatabase.reqKousyouDestroyShip.get_req();
	    let ship;
	    let ship_slot;

	    if (!this._ts || isNaN(req._ship_id))
		return;

	    this._deepcopy();
	    this._delete_ship(req._ship_id);
	    this._db.list = Object.keys(this._db.hash);

	    this._ts = t;
	    this._update_owner();
	    return [];
	},

	reqKousyouGetShip: function() {
	    let data = KanColleDatabase.reqKousyouGetShip.get().api_slotitem;
	    let t = KanColleDatabase.reqKousyouGetShip.timestamp();

	    if (!this._ts || !data)
		return;

	    this._deepcopy();
	    for (let i = 0; i < data.length; i++)
		this._db.hash[data[i].api_id] = data[i];
	    this._db.list = Object.keys(this._db.hash);
	    this._ts = t;
	    this._update_owner();
	    return [];
	},

	reqKousyouRemodelSlot: function() {
	    let data = KanColleDatabase.reqKousyouRemodelSlot.get();
	    let t = KanColleDatabase.reqKousyouRemodelSlot.timestamp();

	    if (!this._ts || !data)
		return;

	    this._deepcopy();

	    this._delete_slotitems(data.api_use_slot_id);

	    if (data.api_after_slot && data.api_after_slot.api_id)
		this._db.hash[data.api_after_slot.api_id] = data.api_after_slot;

	    this._db.list = Object.keys(this._db.hash);
	    this._ts = t;
	    this._update_owner();
	    return [];
	},

	battle: function(stage) {
	    let t = KanColleDatabase.battle.timestamp();
	    let decks;
	    let that = this;
	    let updated = 0;

	    if (stage != -2 && stage != 2)
		return;

	    if (!this._ts)
		return;

	    this._deepcopy();

	    decks = KanColleDatabase.battle.list();
	    decks.forEach(function(e) {
		let ids = KanColleDatabase.battle.get(e).consumed;
		if (!ids)
		    return;
		updated += that._delete_slotitems(Object.keys(ids));
	    });

	    if (!updated)
		return;

	    this._db.list = Object.keys(this._db.hash);

	    this._ts = t;
	    this._update_owner();
	    return [];
	},
    };

    this._delete_slotitems = function(ids) {
	let count = 0;
	if (ids) {
	    for (let i = 0; i < ids.length; i++) {
		let id = ids[i];
		if (id < 0 || !this._db.hash[id])
		    continue;
		debugprint('deleting slotitem: ' + id);
		delete(this._db.hash[id]);
		count++;
	    }
	}
	return count;
    };

    this._delete_ship = function(id) {
	let count = 0;
	let ship = KanColleDatabase.ship.get(id);
	if (ship) {
	    count += this._delete_slotitems([ship.api_slot_ex]);
	    count += this._delete_slotitems(ship.api_slot);
	}
	return count;
    };

    this.get = function(id, key) {
	if (key == 'owner') {
	    return id ? this._db.owner[id] : this._db.owner;
	} else if (key == null) {
	    return this._db.hash ? this._db.hash[id] : KanColleDatabase._memberSlotitem.get(id);
	}
    };

    this.list = function() {
	if (!this._db.hash)
	    return KanColleDatabase._memberSlotitem.list();
	if (!this._db.list)
	    this._db.list = Object.keys(this._db.hash);
	return this._db.list;
    };

    this.count = function() {
	return this._db.hash ? this._db.list.length : KanColleDatabase._memberSlotitem.count();
    };

    this.stat = function() {
	return this._stat;
    };
};
KanColleSlotitemDB.prototype = new KanColleCombinedDB();

/**
 * 任務（クエスト）データベース。
 */
var KanColleQuestDB = function() {
    this._init();

    this._db = {
	hash: {},
	timestamp: {},
	list: [],
    };

    this._update = {
	memberQuestlist: function() {
	    let t = KanColleDatabase.memberQuestlist.timestamp();
	    let d = KanColleDatabase.memberQuestlist.get();
	    let quests = this._db;
	    let oldest = Number.MAX_VALUE;

	    if (!t)
		return;

	    if (!quests.pages)
		quests.pages = [];
	    quests.pages[d.api_disp_page] = t;

	    debugprint('num = ' + d.api_page_count + ', pages = ' + quests.pages.toSource());

	    for (let i = 1; i <= d.api_page_count; i++) {
		let v = quests.pages[i] || 0;
		if (v < oldest)
		    oldest = v;
	    }

	    if (d.api_list) {
		if (Array.isArray(d.api_list)) {
		    d.api_list.forEach(function(q) {
			if (typeof(q) != 'object')
			    return;
			quests.hash[q.api_no] = q;
			quests.timestamp[q.api_no] = t;
		    });
		} else {
		    debugprint('q.api_list is non-array.');
		}
	    } else {
		debugprint('d.api_list is null: ' + d.toSource());
	    }

	    // Clean-up obsolete quests.
	    debugprint('t=' + t + ', oldest=' + oldest);
	    Object.keys(quests.hash).filter(function(e) {
		// - エントリの最終更新が全ページの更新より古ければ、
		//   もう表示されないエントリ。
		//debugprint(' ' + e + ': ' + quests.timestamp[e]);
		return quests.timestamp[e] < oldest;
	    }).forEach(function(e) {
		debugprint('delete obsolete quest: ' + e);
		delete quests.hash[e];
	    });

	    this._db.list = null;
	    this._ts = t;

	    this._save_quest();

	    return [];
	},
	reqQuestClearitemget: function() {
	    let req = KanColleDatabase.reqQuestClearitemget.get_req();
	    let t = KanColleDatabase.reqQuestClearitemget.timestamp();
	    debugprint('delete cleared quest: ' + req._quest_id);
	    delete this._db.hash[req._quest_id];
	    delete this._db.timestamp[req._quest_id];

	    this._db.list = null;
	    this._ts = t;

	    this._save_quest();

	    return [];
	},
	reqMemberGetIncentive: function() {
	    if (!this._load_quest())
		return;
	    return [];
	},
    };

    this._load_quest = function() {
	let obj = KanColleUtils.readObject('quest', null);
	if (!obj)
	    return;

	this._db.hash = obj.hash;
	this._db.list = null;
	this._db.timestamp = obj.timestamp;

	this._ts = obj._ts;

	return true;
    };

    this._save_quest = function() {
	if (!this._ts)
	    return;

	KanColleUtils.writeObject('quest', {
	    hash: this._db.hash,
	    timestamp: this._db.timestamp,
	    _ts: this._ts,
	});
    };

    this.list = function() {
	if (!this._db.list) {
	    let list = Object.keys(this._db.hash);
	    list.sort(function(a,b) { return a - b; });
	    this._db.list = list;
	}
	return this._db.list;
    };

    this.get = function(id) {
	return this._db.hash[id];
    };

    this.timestamp = function(id) {
	if (id !== undefined)
	    return this._db.timestamp[id];
	return this._ts;
    };
};
KanColleQuestDB.prototype = new KanColleCombinedDB();

/**
 * 遠征データベース。
 */
var KanColleMissionDB = function() {
    this._init();

    this._stat = {};
    this._db = {};

    this._update = {
	masterMission: function() {
	    let list = KanColleDatabase.masterMission.list();
	    for (let i = 0; i < list.length; i++) {
		this._db[list[i]] = KanColleDatabase.masterMission.get(list[i]);
	    }
	    return [];
	},
	reqMissionResult: function() {
	    let t = KanColleDatabase.reqMissionResult.timestamp();
	    let req = KanColleDatabase.reqMissionResult.get_req();
	    let d = KanColleDatabase.reqMissionResult.get();
	    let deck;
	    let mission;

	    if (isNaN(req._deck_id))
		return;

	    deck = KanColleDatabase.deck.get(req._deck_id);
	    if (!deck)
		return;

	    mission = deck.api_mission;
	    if (mission && mission[0] && mission[1]) {
		if (!this._stat[mission[1]])
		    this._stat[mission[1]] = {};
		if (!this._stat[mission[1]][d.api_clear_result])
		    this._stat[mission[1]][d.api_clear_result] = 0
		this._stat[mission[1]][d.api_clear_result]++;
	    }
	    return [];
	},
    };

    this.list = function() {
	return Object.keys(this._db);
    };

    this.get = function(id) {
	return this._db[id];
    };

    this.stat = function() {
	return this._stat;
    };
};
KanColleMissionDB.prototype = new KanColleCombinedDB();

/**
 * 演習データベース。
 */
var KanCollePracticeDB = function() {
    this._init();

    this._db = {
	hash: {},
	info: {},
    };

    this._update = {
	memberPractice: function() {
	    let t = KanColleDatabase.memberPractice.timestamp();
	    let list = KanColleDatabase.memberPractice.list();

	    // 演習は3時,15時(6時,18時UTC)に更新
	    if (!this._ts || Math.floor((this._ts - 21600000) / 43200000) < Math.floor((t - 21600000) / 43200000)) {
		this._db.hash = {};
		this._db.info = {};
	    };

	    for (let i = 0; i < list.length; i++) {
		let p = KanColleDatabase.memberPractice.get(list[i]);
		this._db.hash[p.api_id] = p;
	    }

	    this._ts = t;
	    return [];
	},

	reqMemberGetPracticeEnemyInfo: function() {
	    let t = KanColleDatabase.reqMemberGetPracticeEnemyInfo.timestamp();
	    let info = KanColleDatabase.reqMemberGetPracticeEnemyInfo.get();

	    if (!this._ts)
		return;

	    this._db.info[info.api_member_id] = info;

	    this._ts = t;
	    return [];
	},
    };

    this.get = function(id) {
	return this._db.hash[id];
    };

    this.list = function() {
	return Object.keys(this._db.hash);
    };

    this.find = function(id) {
	return this._db.info[id];
    };
};
KanCollePracticeDB.prototype = new KanColleCombinedDB();

/**
 * 建造ドックデータベース。
 */
var KanColleKdockDB = function() {
    this._init();

    this._db = {
	timestamp: {},
	hash: null,
	list: null,
	timer: {},
    };
    this._timer = null;

    this._deepcopy = function() {
	if (!this._db.hash) {
	    let ids = KanColleDatabase._memberKdock.list();
	    if (!ids)
		return;

	    this._db.hash = new Object;

	    for (let i = 0; i < ids.length; i++)
		this._db.hash[ids[i]] = JSON.parse(JSON.stringify(KanColleDatabase._memberKdock.get(ids[i])));

	    this._db.list = Object.keys(this._db.hash);

	    //debugprint('hash: ' + this._db.hash.toSource());
	    //debugprint('list: ' + this._db.list.toSource());
	}
    };

    this._update = {
	_memberKdock: function() {
	    let that = this;
	    let t = KanColleDatabase._memberKdock.timestamp();
	    let list = KanColleDatabase._memberKdock.list();

	    this._db.hash = null;
	    this._db.list = null;

	    this._deepcopy();

	    list.forEach(function(e) {
		that._update_timestamp(e, t);
	    });

	    this._update_timer(this._ts);

	    this._ts = t;
	    this._save();

	    return [{}];
	},

	reqKousyouCreateShipSpeedChange: function() {
	    let t = KanColleDatabase.reqKousyouCreateShipSpeedChange.timestamp();
	    let req = KanColleDatabase.reqKousyouCreateShipSpeedChange.get_req();

	    if (!this._ts)
		return;

	    this._deepcopy();

	    kdock = this._db.hash[req._kdock_id];
	    kdock.api_state = 3;	    // completed
	    kdock.api_complete_time = 0;

	    this._update_timestamp(req._kdock_id, t);
	    this._update_timer(this._ts);

	    this._ts = t;
	    this._save();

	    return [{}];
	},
	reqMemberGetIncentive: function() {
	    if (!this._load())
		return;
	    return [{ init: true }];
	}
    };

    this._update_timestamp = function(e, t) {
	let d = this._db.hash[e];
	if (!d)
	    return;
	if (!this._db.timestamp[e])
	    this._db.timestamp[e] = {};

	if (d.api_state == 1 || d.api_state == 2) {
	    if (!this._db.timestamp[e].start) {
		this._db.timestamp[e].start = t;
		this._db.timestamp[e].complete = d.api_complete_time;
	    }
	} else if (d.api_state == 0) {
	    this._db.timestamp[e].start = 0;
	}

	this._db.timestamp[e].updated = t;
    };

    this._update_timer = function(now) {
	let docks = this.list();
	let next = Number.POSITIVE_INFINITY;
	debugprint('kdock: update timers');
	for (let dock of docks) {
	    let d = this.get(dock);
	    let k = d.api_id;
	    let state = this._db.timer[k] || 0;
	    let t;

	    if (d.api_state > 0)
		t = d.api_complete_time - now;
	    else
		t = Number.NaN;

	    debugprint('kdock: dock = ' + dock + ', state = ' + state + ', d.api_state = ' + d.api_state + ', complete_time = ' + d.api_complete_time + ', t = ' + t);

	    if (isNaN(t))
		state = -1;
	    else if (t <= 0) {
		if (state < 3) {
		    // notify
		    state = 3;
		    this._timer_notify(k, state);
		}
	    } else if (t <= 60000) {
		if (state < 2) {
		    // notify
		    state = 2;
		    this._timer_notify(k, state);
		}
		if (next > t)
		    next = t;
	    } else {
		if (state < 1) {
		    state = 1;
		    this._timer_notify(k, state);
		}
		if (next > t - 60000)
		    next = t - 60000;
	    }
	    if (this._db.timer[k] != state)
		debugprint('kdock: => state = ' + state);
	    this._db.timer[k] = state;
	}
	this._stop_timer();
	debugprint('kdock: now = ' + now + ', next = ' + next);
	this._start_timer(now + next)
    };

    this._init_timer = function() {
    };
    this._start_timer = function(next) {
	if (next != Number.POSITIVE_INFINITY)
	    this._timer = KanColleTimerUtils.timer.startDelayedEventAt(this, next);
    };
    this._stop_timer = function() {
	KanColleTimerUtils.timer.stop(this._timer);
	this._timer = null;
    };
    this._timer_notify = function(id, state) {
	debugprint('kdock: notify(dock = ' + id + ', state = ' + state + ')');
	//0        1      2  3  4    5        6          7  8    9
	//まもなく|ドック|で|の|建造|していま|が完了しま|す|した|。
	const _m = '\u307e\u3082\u306a\u304f|\u30c9\u30c3\u30af|\u3067|\u306e|\u5efa\u9020|\u3057\u3066\u3044\u307e|\u304c\u5b8c\u4e86\u3057\u307e|\u3059|\u3057\u305f|\u3002'.split(/\|/);
	let msg = null;
	let popup = KanColleUtils.getBoolPref('popup.kdock');
	let sound = null;
	switch (state) {
	case 3:
	    msg =         _m[1] + id         + _m[3] + _m[4]         + _m[6]         + _m[8] + _m[9];
	    sound = KanColleUtils.getUnicharPrefFileURI('sound.kdock');
	    break;
	case 2:
	    msg = _m[0] + _m[1] + id         + _m[3] + _m[4]         + _m[6] + _m[7]         + _m[9];
	    sound = KanColleUtils.getUnicharPrefFileURI('sound.1min.kdock');
	    popup = popup && KanColleUtils.getBoolPref('popup.1min-before');
	    break;
	case 1:
	    msg =         _m[1] + id + _m[2]         + _m[4] + _m[5]         + _m[7]         + _m[9];
	    popup = false;
	    sound = null;
	    break;
	default:
	    return;
	}
	debugprint('kdock: popup = ' + popup + ', sound = ' + (sound ? sound.spec : '') + ': ' + msg);
	if (popup)
	    KanColleTimerUtils.alert.show(KanColleUtils.IconURL, KanColleUtils.Title, msg, false, msg, null);
	if (sound)
	    KanColleTimerUtils.sound.play(sound);
	this._cb.notify(this._ts, this.get(), { state: state, message: msg, });
    };

    this.observe = function(subject, topic, data) {
	switch (topic) {
	case 'timer-callback':
	    this._update_timer((new Date).getTime());
	    break;
	default:;
	}
    };

    this._save = function() {
	if (!this._ts)
	    return;

	KanColleUtils.writeObject('kdock', {
	    hash: this._db.hash,
	    timestamp: this._db.timestamp,
	    _ts: this._ts,
	});
    };

    this._load = function() {
	let obj = KanColleUtils.readObject('kdock', null);
	if (!obj)
	    return;
	this._db.hash = obj.hash;
	this._db.timer = {};
	this._db.list = null;
	this._db.timestamp = obj.timestamp;
	this._ts = obj._ts;
	this._update_timer(this._ts);
	return true;
    };

    this.get = function(id) {
	return this._db.hash ? this._db.hash[id] : KanColleDatabase._memberKdock.get(id);
    };

    this.list = function() {
	if (!this._db.hash)
	    return KanColleDatabase._memberKdock.list();
	if (!this._db.list)
	    this._db.list = Object.keys(this._db.hash);
	return this._db.list;
    };

    this.count = function() {
	return this._db.hash ? this._db.list.length : KanColleDatabase._memberKdock.count();
    };

    this.timestamp = function(id) {
	if (!id)
	    return this._ts;
	return this._db.timestamp[id];
    };

    this._postinit_hook = function() {
	this._init_timer();
    };
    this._preexit_hook = function() {
	this._stop_timer();
    };
};
KanColleKdockDB.prototype = new KanColleCombinedDB();

/**
 * 入渠ドックデータベース。
 */
var KanColleNdockDB = function() {
    this._init();

    this._db = {
	ship: null,
	hash: null,
	list: null,
	timer: {},
    };
    this._timer = null;

    this._deepcopy = function() {
	if (!this._db.hash) {
	    let ids = KanColleDatabase._memberNdock.list();
	    if (!ids)
		return;

	    this._db.hash = new Object;

	    for (let i = 0; i < ids.length; i++)
		this._db.hash[ids[i]] = JSON.parse(JSON.stringify(KanColleDatabase._memberNdock.get(ids[i])));

	    //debugprint('hash: ' + this._db.hash.toSource());
	    //debugprint('list: ' + this._db.list.toSource());
	}
    };

    this._update = {
	_memberNdock: function() {
	    let t = KanColleDatabase._memberNdock.timestamp();

	    this._db.hash = null;
	    this._db.list = null;
	    this._db.ship = null;

	    this._deepcopy();

	    this._ts = t;

	    this._update_timer(this._ts);

	    this._save();

	    return [{}];
	},

	reqNyukyoSpeedChange: function() {
	    let t = KanColleDatabase.reqNyukyoSpeedChange.timestamp();
	    let req = KanColleDatabase.reqNyukyoSpeedChange.get_req();

	    if (!this._ts || isNaN(req._ndock_id))
		return;

	    this._deepcopy();

	    kdock = this._db.hash[req._ndock_id];
	    kdock.api_state = 0;	    //empty
	    kdock.api_complete_time = 0;

	    this._db.ship = null;

	    this._ts = t;

	    this._update_timer(this._ts);

	    this._save();

	    return [{}];
	},

	reqMemberGetIncentive: function() {
	    if (!this._load())
		return;
	    return [{ init: true }];
	},
    };

    this._update_timer = function(now) {
	let docks = this.list();
	let next = Number.POSITIVE_INFINITY;
	debugprint('ndock: update timers');
	for (let dock of docks) {
	    let d = this.get(dock);
	    let k = d.api_id;
	    let state = this._db.timer[k] || 0;
	    let t;

	    if (d.api_state > 0)
		t = d.api_complete_time - now;
	    else if (state > 0)
		t = 0;
	    else
		t = Number.NaN;

	    debugprint('ndock: dock = ' + dock + ', state = ' + state + ', d.api_state = ' + d.api_state + ', complete_time = ' + d.api_complete_time + ', t = ' + t);

	    if (isNaN(t))
		state = -1;
	    else if (t <= 0) {
		if (state < 3) {
		    // notify
		    state = 3;
		    this._timer_notify(k, state);
		}
	    } else if (t <= 60000) {
		if (state < 2) {
		    // notify
		    state = 2;
		    this._timer_notify(k, state);
		}
		if (next > t)
		    next = t;
	    } else {
		if (state < 1) {
		    state = 1;
		    this._timer_notify(k, state);
		}
		if (next > t - 60000)
		    next = t - 60000;
	    }
	    if (this._db.timer[k] != state)
		debugprint('ndock: => state = ' + state);
	    this._db.timer[k] = state;
	}
	this._stop_timer();
	debugprint('ndock: now = ' + now + ', next = ' + next);
	this._start_timer(now + next)
    };

    this._init_timer = function() {
    };
    this._start_timer = function(next) {
	if (next != Number.POSITIVE_INFINITY)
	    this._timer = KanColleTimerUtils.timer.startDelayedEventAt(this, next);
    };
    this._stop_timer = function() {
	KanColleTimerUtils.timer.stop(this._timer);
	this._timer = null;
    };
    this._timer_notify = function(id, state) {
	debugprint('ndock: notify(dock = ' + id + ', state = ' + state + ')');
	//0        1      2  3  4    5        6          7  8    9
	//まもなく|ドック|で|の|修復|していま|が完了しま|す|した|。
	const _m = '\u307e\u3082\u306a\u304f|\u30c9\u30c3\u30af|\u3067|\u306e|\u4fee\u5fa9|\u3057\u3066\u3044\u307e|\u304c\u5b8c\u4e86\u3057\u307e|\u3059|\u3057\u305f|\u3002'.split(/\|/);
	let msg = null;
	let popup = KanColleUtils.getBoolPref('popup.ndock');
	let sound = null;
	switch (state) {
	case 3:
	    msg =         _m[1] + id         + _m[3] + _m[4]         + _m[6]         + _m[8] + _m[9];
	    sound = KanColleUtils.getUnicharPrefFileURI('sound.ndock');
	    break;
	case 2:
	    msg = _m[0] + _m[1] + id         + _m[3] + _m[4]         + _m[6] + _m[7]         + _m[9];
	    sound = KanColleUtils.getUnicharPrefFileURI('sound.1min.ndock');
	    popup = popup && KanColleUtils.getBoolPref('popup.1min-before');
	    break;
	case 1:
	    msg =         _m[1] + id + _m[2]         + _m[4] + _m[5]         + _m[7]         + _m[9];
	    popup = false;
	    sound = null;
	    break;
	default:
	    return;
	}
	debugprint('ndock: popup = ' + popup + ', sound = ' + (sound ? sound.spec : '') + ': ' + msg);
	if (popup)
	    KanColleTimerUtils.alert.show(KanColleUtils.IconURL, KanColleUtils.Title, msg, false, msg, null);
	if (sound)
	    KanColleTimerUtils.sound.play(sound);
	this._cb.notify(this._ts, this.get(), { state: state, message: msg, });
    };

    this.observe = function(subject, topic, data) {
	switch (topic) {
	case 'timer-callback':
	    this._update_timer((new Date).getTime());
	    break;
	default:;
	}
    };

    this._save = function() {
	if (!this._ts)
	    return;
	KanColleUtils.writeObject('ndock', {
	    hash: this._db.hash,
	    _ts: this._ts,
	});
    };

    this._load = function() {
	let obj = KanColleUtils.readObject('ndock', null);
	if (!obj)
	    return;
	this._db.hash = obj.hash;
	this._db.ship = null;
	this._db.list = null;
	this._db.timer = {};
	this._ts = obj._ts;
	this._update_timer(this._ts);
	return true;
    };

    this.get = function(id) {
	return this._db.hash ? this._db.hash[id] : KanColleDatabase._memberNdock.get(id);
    };

    this._find = function() {
	if (!this._db.ship) {
	    let that = this;
	    this._db.ship = this.list().map(function(e) {
		return that.get(e);
	    }).reduce(function(p,c) {
		if (c && c.api_ship_id)
		    p[c.api_ship_id] = c.api_id;
		return p;
	    }, {});
	}
	return this._db.ship;
    };

    this.find = function(shipid) {
	return this._find()[shipid];
    };

    this._list = function() {
	if (!this._db.list)
	    this._db.list = Object.keys(this._db.hash);
	return this._db.list;
    };

    this.list = function() {
	if (!this._db.hash)
	    return KanColleDatabase._memberNdock.list();
	return this._list();
    };

    this.count = function() {
	return this._db.hash ? this._db._list().length : KanColleDatabase._memberNdock.count();
    };

    this._postinit_hook = function() {
	this._init_timer();
    };
    this._preexit_hook = function() {
	this._stop_timer();
    };
};
KanColleNdockDB.prototype = new KanColleCombinedDB();

/**
 * 戦闘/対戦データベース。
 * 戦闘の進行に応じて何度かnotifyする。
 *	-3: reset
 *	-2: enemy information (partial)
 *	-1: enemy information
 *	 0: before update
 *	 1: after update
 *	 2: battle finished
 *	 3: battle finished, final
 */
var KanColleBattleDB = function() {
    this._init();

    this._stat = {
	first: 0,
	map: {
	    // practice, boss, count, rank by map (e.g., 1-1-1)
	},
	type: {},
    };
    this._db = {};

    this._update = {
	// 敵/対戦情報
	memberPractice: function() {
	    let t = KanColleDatabase.memberPractice.timestamp();
	    this._init_battle();
	    this._ts = t;
	    return [-3];
	},
	memberMission: function() {
	    let t = KanColleDatabase.memberMission.timestamp();
	    this._init_battle();
	    this._ts = t;
	    return [-3];
	},
	memberMapinfo: function() {
	    let t = KanColleDatabase.memberMapinfo.timestamp();
	    this._init_battle();
	    this._ts = t;
	    return [-3];
	},
	reqMemberGetPracticeEnemyInfo: [
	    function() {
		let t = KanColleDatabase.reqMemberGetPracticeEnemyInfo.timestamp();
		this._init_battle();
		this._ts = t;
		return [-3];
	    },
	    function() {
		let d = KanColleDatabase.reqMemberGetPracticeEnemyInfo.get();

		this._db['e1'].practice = true;
		this._db['e1'].first = false;
		this._db['e1']._first = false;
		this._db['e1'].boss = false;
		this._db['e1'].last = false;
		this._db['e1'].name = '\u6f14\u7fd2';	//演習
		this._db['e1'].fullname = d.api_deckname;
		this._db['e1'].ships = null;

		return [-2];
	    },
	    function() {
		let d = KanColleDatabase.reqMemberGetPracticeEnemyInfo.get();
		let ships = (d.api_deck.api_ships || []).map(function(e) {
		    return e.api_ship_id || -1;
		});
		ships.unshift(-1);

		this._db['e1'].ships = ships;

		return [-1];
	    },
	],
	reqMapStart: [
	    function(start) {
		let t = KanColleDatabase.reqMapStart.timestamp();

		if (start)
		    return;

		this._reset_battle();

		this._ts = t;
		return [-3];
	    },
	    function(start) {
		let t = KanColleDatabase.reqMapStart.timestamp();
		let req = KanColleDatabase.reqMapStart.get_req();
		let d = KanColleDatabase.reqMapStart.get();

		this._db['e1'].practice = false;
		this._db['e1'].first = start;
		this._db['e1'].boss = d.api_event_id == 5;  // d.api_bosscell_no does not work
		this._db['e1'].last = !d.api_next;
		this._db['e1'].name = d.api_maparea_id + '-' + d.api_mapinfo_no + '-' + d.api_no;
		this._db['e1'].fullname = null;
		this._db['e1'].ships = null;	// will be updated later

		if (start)
		    this._db['e1']._first = true;

		if (req._recovery_type) {
		    let useitem = ([ 0, 42, 43 ])[req._recovery_type];

		    if (!this._db[res._deck_id])
			this._db[res._deck_id] = {};
		    this._db[res._deck_id].consumed = {},

		    this._revive_ship(req._deck_id, 1, useitem);
		}
		this._ts = t;
		return [-2];
	    },
	],
	reqSortieBattle: [
	    function(mode) {
		let t = KanColleDatabase.reqSortieBattle.timestamp();
		let data = KanColleDatabase.reqSortieBattle.get();

		if (mode.practice)
		    return;

		this._db['e1'].ships = data.api_ship_ke;

		this._ts = t;
		return [-1];
	    },
	    function(mode) {
		let t = KanColleDatabase.reqSortieBattle.timestamp();
		let data = KanColleDatabase.reqSortieBattle.get();
		let deckid = data.api_deck_id || data.api_dock_id;  //typo
		let deckid2 = mode.combined ? (deckid % 4) + 1 : deckid;

		this._update_escape(deckid, data.api_escape_idx);
		this._update_escape(deckid2, data.api_escape_idx_combined);

		this._start_battle(deckid, data.api_nowhps.slice(0,7), data.api_maxhps.slice(0,7));

		debugprint('maxhps: ' + data.api_maxhps.toSource());
		debugprint('nowhps: ' + data.api_nowhps.toSource());

		if (mode.combined) {
		    this._start_battle(deckid2, data.api_nowhps_combined, data.api_maxhps_combined);

		    debugprint('maxhps2: ' + data.api_maxhps_combined.toSource());
		    debugprint('nowhps2: ' + data.api_nowhps_combined.toSource());
		}

		this._start_battle('e1', data.api_nowhps.slice(6), data.api_maxhps.slice(6));
		this._fixup_battle('e1');

		this._ts = t;
		return [0];
	    },
	    function(mode) {
		let data = KanColleDatabase.reqSortieBattle.get();
		let deckid = data.api_deck_id || data.api_dock_id;  //typo
		let deckid2 = mode.combined ? (deckid % 4) + 1 : deckid;

		// 索敵
		if (data.api_stage_flag[0])
		    this._update_battle_raibak(data.api_kouku.api_stage1, deckid);
		if (data.api_stage_flag[1])
		    this._update_battle_raibak(data.api_kouku.api_stage2, deckid);
		if (data.api_stage_flag[2])
		    this._update_battle_raibak(data.api_kouku.api_stage3, deckid);
		// 索敵(連合艦隊用フラグなし？)
		if (data.api_kouku.api_stage3_combined)
		    this._update_battle_raibak(data.api_kouku.api_stage3_combined, deckid2);
		// 支援
		switch (data.api_support_flag) {
		case undefined:
		case 0:
		    break;
		case 1: /* 航空支援 */
		    if (data.api_support_info.api_support_airatack.api_stage_flag[2])
			this._update_battle_raibak(data.api_support_info.api_support_airatack.api_stage3, deckid);
		    break;
		case 2: /* 支援射撃 */
		case 3: /* 支援長距離雷撃 */
		    this._update_battle_support(data.api_support_info.api_support_hourai);
		    break;
		default:
		    debugprint('support: unknown ' + data.api_support_flag);
		}
		// 開幕
		if (data.api_opening_flag)
		    this._update_battle_raibak(data.api_opening_atack, deckid2); // attackではない

		// 航空戦闘マス
		if (data.api_stage_flag2) {
		    if (data.api_stage_flag2[0])
			this._update_battle_raibak(data.api_kouku2.api_stage1, deckid);
		    if (data.api_stage_flag2[1])
			this._update_battle_raibak(data.api_kouku2.api_stage2, deckid);
		    if (data.api_stage_flag2[2])
			this._update_battle_raibak(data.api_kouku2.api_stage3, deckid);
		    if (data.api_kouku2.api_stage3_combined)
			this._update_battle_raibak(data.api_kouku2.api_stage3_combined, deckid2);
		}

		if (data.api_hourai_flag) {
		    if (!mode.combined || mode.water) {
			// 通常艦隊砲雷撃
			// 連合艦隊砲雷撃 (通常マス; 第一艦隊砲撃(x2)->第二艦隊砲撃->第二艦隊雷撃
			if (data.api_hourai_flag[0])
			    this._update_battle_hourai(data.api_hougeki1, deckid);
			if (data.api_hourai_flag[1])
			    this._update_battle_hourai(data.api_hougeki2, deckid);
			if (data.api_hourai_flag[2])
			    this._update_battle_hourai(data.api_hougeki3, deckid2);
			if (data.api_hourai_flag[3])
			    this._update_battle_raibak(data.api_raigeki, deckid2);
		    } else {
			// 連合艦隊砲雷撃 (通常マス; 第二艦隊砲撃->第二艦隊雷撃->第一艦隊砲撃(x2)
			if (data.api_hourai_flag[0])
			    this._update_battle_hourai(data.api_hougeki1, deckid2);
			if (data.api_hourai_flag[1])
			    this._update_battle_raibak(data.api_raigeki, deckid2);
			if (data.api_hourai_flag[2])
			    this._update_battle_hourai(data.api_hougeki2, deckid);
			if (data.api_hourai_flag[3])
			    this._update_battle_hourai(data.api_hougeki3, deckid);
		    }
		}

		this._fixup_battle(deckid);
		if (deckid != deckid2)
		    this._fixup_battle(deckid2);
		this._fixup_battle('e1');

		return [1];
	    },
	],
	reqBattleMidnightBattle: [
	    function(mode) {
		let t = KanColleDatabase.reqBattleMidnightBattle.timestamp();
		let data = KanColleDatabase.reqBattleMidnightBattle.get();

		if (mode.practice)
		    return;

		this._db['e1'].ships = data.api_ship_ke;

		this._ts = t;
		return [-1];
	    },
	    function(mode) {
		let t = KanColleDatabase.reqBattleMidnightBattle.timestamp();
		let data = KanColleDatabase.reqBattleMidnightBattle.get();
		let deckid = data.api_deck_id || data.api_dock_id;  // typo?
		let deckid2 = (deckid % 4) + 1;

		if (mode.combined) {
		    this._update_escape(deckid, data.api_escape_idx);
		    this._update_escape(deckid2, data.api_escape_idx_combined);
		}

		this._start_battle(deckid, data.api_nowhps.slice(0,7), data.api_maxhps.slice(0,7));

		debugprint('maxhps: ' + data.api_maxhps.toSource());
		debugprint('nowhps: ' + data.api_nowhps.toSource());

		if (mode.combined) {
		    debugprint('maxhps2: ' + data.api_maxhps_combined.toSource());
		    debugprint('nowhps2: ' + data.api_nowhps_combined.toSource());

		    this._start_battle(deckid2, data.api_nowhps_combined, data.api_maxhps_combined);
		}

		this._start_battle('e1', data.api_nowhps.slice(6), data.api_maxhps.slice(6));
		this._fixup_battle('e1');

		this._ts = t;
		return [0];
	    },
	    function(mode) {
		let data = KanColleDatabase.reqBattleMidnightBattle.get();
		let deckid = data.api_deck_id || data.api_dock_id;  // typo?
		let deckid2 = mode.combined ? (deckid % 4) + 1 : deckid;

		// 索敵
		if (data.api_hougeki)
		    this._update_battle_hourai(data.api_hougeki, deckid2);

		this._fixup_battle(deckid);
		if (deckid != deckid2)
		    this._fixup_battle(deckid2);
		this._fixup_battle('e1');

		return [1];
	    },
	],
	reqCombinedBattleGobackPort: function(mode) {
	    let t = KanColleDatabase.reqCombinedBattleGobackPort.timestamp();
	    let battle = KanColleDatabase.reqSortieBattleResult.get();
	    let escape = battle ? battle.api_escape : null;

	    if (!escape) {
		debugprint('no escape information:' + (battle ? battle.toSource() : 'null'));
		return;
	    }
	    debugprint('escape ships: ' + escape.api_escape_idx.toSource());
	    debugprint('tow    ships: ' + escape.api_tow_idx.toSource());

	    this._fixup_escape(1, 2, [ escape.api_escape_idx[0], escape.api_tow_idx[0] ]);

	    this._ts = t;
	    return [0];
	},
	reqSortieBattleResult: [
	    function(mode) {
	        let t = KanColleDatabase.reqSortieBattleResult.timestamp();
		// Notify listeners and update slotitems.
		let that = this;
		Object.keys(this._db).forEach(function(deckid) {
		    if (that._db[deckid].consumed)
			debugprint('Consumed items: ' + that._db[deckid].consumed.toSource());
		});
		this._ts = t;
		return [2];
	    },
	    function(mode) {
	        let t = KanColleDatabase.reqSortieBattleResult.timestamp();
		let data = KanColleDatabase.reqSortieBattleResult.get();
		this._recordNewShip(t, data);
		this._update_battlestat(data.api_win_rank);
		return [3];
	    },
	],
	memberBasic: function() {
	    let t = KanColleDatabase.memberBasic.timestamp();
	    this._init_battle();
	    this._ts = t;
	    return [-3];
	},
    };

    this._recordNewShip = function(time, data) {
	if (!data.api_get_ship)
	    return;
	KanColleDatabase.ship.recordNewShip(
	    time,
	    data.api_quest_name,
	    data.api_enemy_info.api_deck_name,
	    data.api_get_ship.api_ship_type,
	    data.api_get_ship.api_ship_name,
	    data.api_win_rank
	);
    };

    this._update_battle_raibak = function(data, deckid) {
	let that = this;
	debugprint('====RAIBAK====');

	function __parse_xdam(damages, sdeck, ddeck) {
	    if (!damages)
		return;
	    for (let i = 0; i <= 6; i++) {
		if (damages[i] === undefined || damages[i] < 0)
		    continue;
		that._update_battle(sdeck, -1, ddeck, i, Math.floor(damages[i]));
	    }
	}

	function __parse_xydam(dposs, damages, sdeck, ddeck) {
	    if (!dposs || !damages)
		return;
	    for (let i = 0; i <= 6; i++) {
		if (!dposs[i] || damages[i] === undefined || damages[i] < 0)
		    continue;
		that._update_battle(sdeck, i, ddeck, dposs[i], Math.floor(damages[i]));
	    }
	}

	//自軍 => 敵軍
	if (data.api_frai)
	    __parse_xydam(data.api_frai, data.api_fydam, deckid, 'e1');
	else
	    __parse_xdam(data.api_edam, deckid, 'e1');

	//敵軍 => 自軍
	if (data.api_erai)
	    __parse_xydam(data.api_erai, data.api_eydam, 'e1', deckid);
	else
	    __parse_xdam(data.api_fdam, 'e1', deckid);

	this._check_downed_ships(deckid);
    };

    this._update_battle_hourai = function(data, deckid) {
	debugprint('====HOURAI====');
	for (let i = 0; i < data.api_at_list.length; i++) {
	    let sdeck, spos;

	    if (data.api_at_list[i] < 0 || !data.api_df_list[i])
		continue;

	    sdeck = deckid;
	    spos = data.api_at_list[i];
	    if (spos > 6) {
		sdeck = 'e1';
		spos -= 6;
	    }

	    for (let j = 0; j < data.api_df_list[i].length; j++) {
		let ddeck, dpos, damage;

		ddeck = deckid;
		dpos = data.api_df_list[i][j];
		if (dpos > 6) {
		    ddeck = 'e1';
		    dpos -= 6;
		}

		damage = data.api_damage[i][j];
		if (damage === undefined)
		    continue;

		this._update_battle(sdeck, spos, ddeck, dpos, Math.floor(damage));
		if (ddeck != 'e1')
		    this._check_downed_ships(deckid);
	    }
	}
    };

    this._update_battle_support = function(data) {
	debugprint('====SUPPORT====');
	for (let i = 0; i < data.api_damage.length; i++) {
	    let damage = data.api_damage[i];
	    this._update_battle(-1, -1, 'e1', i, Math.floor(damage));
	}
    };

    this._update_battle = function(sdeck, spos, ddeck, dpos, damage) {
	let oldhp, newhp;

	if (damage < 0)
	    return -1;

	oldhp = this._db[ddeck].nowhps[dpos];
	newhp = oldhp - damage;
	if (newhp < 0)
	    newhp = 0;

	this._db[ddeck].damages[dpos] += damage;
	this._db[ddeck].nowhps[dpos] = newhp;

	if (ddeck == 'e1') {
	    debugprint(sdeck + '[' + spos + '] --> ' + ddeck + '[' + dpos + ']: ' +
		       damage + ' (' + oldhp + ' => ' + newhp + ')');
	} else {
	    debugprint(ddeck + '[' + dpos + '] <-- ' + sdeck + '[' + spos + ']: ' +
		       damage + ' (' + oldhp + ' => ' + newhp + ')');
	}

	return newhp > 0 ? 1 : 0;
    };

    this._check_downed_ships = function(deckid) {
	let deck = KanColleDatabase.deck.get(deckid);
	if (!deck)
	    return;
	if (this._db['e1'].practice)
	    return;
	debugprint('--- CHECK DOWNED SHIPS ---');
	for (let i = 2; i <= 6; i++) {
	    let maxhp = this._db[deckid].maxhps[i];
	    if (maxhp < 0)
		continue;
	    if (this._db[deckid].nowhps[i] <= 0) {
		let newhp = this._revive_ship(deckid, i);
		if (newhp > 0)
		    this._db[deckid].nowhps[i] = newhp;
	    }
	}
    };

    this._revive_ship = function(deckid, pos, useitem) {
	let deck = KanColleDatabase.deck.get(deckid);
	let ship = KanColleDatabase.ship.get(deck.api_ship[pos - 1]);
	let slotitem;
	let slot = Number.NaN;
	let item = null;
	let itemid = -1;
	let newhp = -1;

	debugprint(deckid + '#' + pos);

	slotitem = ship.api_slot.slice();
	ship.api_slot.unshift(ship.api_slot_ex);

	for (let i = 0; i < slotitem.length; i++) {
	    let itemtype;

	    item = KanColleDatabase.slotitem.get(slotitem[i]);
	    itemid = item ? item.api_slotitem_id : -1;
	    itemtype = item ? KanColleDatabase.masterSlotitem.get(itemid) : null;

	    if (!itemtype || itemtype.api_type[2] != 23 ||
		(useitem && itemid != useitem)) {
		item = null;
		itemid = -1;
		continue;
	    }

	    //1戦闘で2度轟沈はない

	    slot = i - 1;
	    break;
	}

	if (!isNaN(slot)) {
	    let dcinfo = { ship: pos, slot: slot, item: item.api_id };

	    switch (itemid) {
	    case 42:	//応急修理要員
		newhp = Math.floor(maxhp * (useitem ? 0.5 : 0.2));
		break;
	    case 43:	//応急修理女神
		newhp = maxhp;
		break;
	    default:
		newhp = 0;
	    }
	    dcinfo.nowhp = newhp;

	    this._db[deckid].consumed[item.api_id] = dcinfo;
	}
	return newhp;
    };

    this._update_escape = function(deckid, data) {
	if (!data)
	    data = [];
	debugprint('escape' + deckid + ':' + data.toSource());
	if (!Array.isArray(data))
	    return;
	if (!this._db[deckid])
	    this._db[deckid] = {};
	this._db[deckid].escape = data.reduce(function(p,c) {
	    p[c] = 1;
	    return p;
	}, [-1,0,0,0,0,0,0]);
    };
    this._fixup_escape = function(deckid1, deckid2, data) {
	let deckid;
	for (let i = 0; i < data.length; i++) {
	    let v = data[i];
	    if (v <= 6) {
		deckid = deckid1;
	    } else {
		deckid = deckid2;
		v -= 6;
	    }
	    this._db[deckid].escape[v] = 1;
	}
    };

    this._init_battle = function() {
	let decks = KanColleDatabase.deck.list().slice();
	decks.push('e1');

	this._db = {};
	for (let i = 0; i < decks.length; i++) {
	    let deck = decks[i];
	    this._db[deck] = {
		escape: [-1,0,0,0,0,0,0],
		damages: [-1,0,0,0,0,0,0],
		result: [],
		consumed: {},
	    };
	}
    };

    this._reset_battle = function(softreset) {
	let decks = KanColleDatabase.deck.list().slice();
	decks.push('e1');

	for (let i = 0; i < decks.length; i++) {
	    let deck = decks[i];
	    let _escape = this._db[deck] ? this._db[deck].escape : [-1,0,0,0,0,0,0];
	    this._db[deck] = {
		escape: _escape,
		damages: [-1,0,0,0,0,0,0],
		result: [],
		consumed: {},
	    };
	}
    };

    this._reset_battle = function(softreset) {
	let decks = KanColleDatabase.deck.list().slice();
	decks.push('e1');

	for (let i = 0; i < decks.length; i++) {
	    let deckid = decks[i];
	    let _escape = this._db[deckid] ? this._db[deckid].escape : [-1,0,0,0,0,0,0];
	    let _first = this._db[deckid] ? !!this._db[deckid]._first : false;
	    this._db[deckid] = {
		escape: _escape,
		_first: _first,
		damages: [-1,0,0,0,0,0,0],
		result: [],
		consumed: {},
	    };
	}
    };

    this._start_battle = function(deckid, nowhps, maxhps) {
	this._db[deckid].nowhps = nowhps.slice();
	this._db[deckid].nowhps[0] = -1;
	this._db[deckid].maxhps = maxhps.slice();
	this._db[deckid].maxhps[0] = -1;
    };

    this._fixup_battle = function(deckid) {
	let s = '';

	for (let i = 0; i <= 6; i++) {
	    let cur;
	    let ratio;
	    let nowhps;
	    let maxhps;
	    let battle_data;

	    maxhp = this._db[deckid].maxhps[i];
	    if (maxhp < 0)
		continue;
	    cur = this._db[deckid].nowhps[i];

	    ratio = cur / maxhp;

	    s += deckid + '#' + i + ': ' + cur + '/' + maxhp + ' = ' +
		 ratio.toFixed(3) +
		 (Math.floor(ratio * 1000) != ratio * 1000 ? '+' : '') +
		 '\n';

	    this._db[deckid].result[i] = {
		maxhp: maxhp,
		cur: cur,
		damage: this._db[deckid].damages[i],
	    };
	}
	debugprint(s);
    };

    this._update_battlestat = function(rank) {
	let ships;
	let map;

	if (!this._db['e1'].practice) {
	    if (this._db['e1']._first) {
		this._stat.first++;
		delete this._db['e1']._first;
	    }

	    ships = this._db['e1'].ships;
	    for (let i = 0; i < ships.length; i++) {
		let ship = ships[i];
		let type = KanColleDatabase.masterShip.get(ship);
		let res = this._db['e1'].result[i];
		if (!type)
		    continue;
		if (res.cur == 0) {
		    let stype = type.api_stype;
		    if (!this._stat.type[stype])
			this._stat.type[stype] = 0;
		    this._stat.type[stype]++;
		}
	    }
	}

	map = this._stat.map;
	if (!map[this._db['e1'].name]) {
	    map[this._db['e1'].name] = {
		practice: this._db['e1'].practice,
		boss: this._db['e1'].boss,
		count: 0,
		rank: {},
	    };
	}

	map[this._db['e1'].name].count++;

	if (!map[this._db['e1'].name].rank[rank])
	     map[this._db['e1'].name].rank[rank] = 0;
	map[this._db['e1'].name].rank[rank]++;

	debugprint(this._stat.toSource());
    };

    this.get = function(deckid) {
	return this._db[deckid];
    };

    this.list = function() {
	return Object.keys(this._db);
    };

    this.stat = function() {
	return this._stat;
    };
};
KanColleBattleDB.prototype = new KanColleCombinedDB();

/**
 * 改修工廠データベース。
 */
var KanCollePowerupDB = function(){
    this._init();

    this._db = {};

    this.save = function(db, do_write){
	this._db = db;
	if(do_write) KanColleUtils.writeObject('powerup.json', db);
    };

    this._set_data = function(data, lv){
	let deck = KanColleDatabase.deck.get(1);
	if(!deck){
	    return;
	}

	let db = this._db;

	let deck_ship_id = deck.api_ship[1];
	let ship_id = -1;
	if( parseInt(deck_ship_id, 10) >= 0 ){
	    let ship = KanColleDatabase.ship.get(deck_ship_id);
	    if(ship){
		ship_id = ship.api_ship_id;
	    }
	}

	let ship_name = parseInt(ship_id) >= 0 ? KanColleDatabase.masterShip.get(ship_id).api_name : "";

	let weekofday = (new Date).getDay();
	if(!db[weekofday]){
	    db[weekofday] = new Object();
	}
	if(!db[weekofday][ship_name]){
	    db[weekofday][ship_name] = new Object();
	}

	for(let d of data){
	    let slot_id = d.api_slot_id;
	    let equip_name = KanColleDatabase.masterSlotitem.get(slot_id).api_name;
	    if(!db[weekofday][ship_name][equip_name]){
		db[weekofday][ship_name][equip_name] = [{},{},{}];
	    }

	    if(lv>0){
		for(let key in db[weekofday][ship_name][equip_name][0]){
		    db[weekofday][ship_name][equip_name][lv][key] = db[weekofday][ship_name][equip_name][0][key];
		}
	    }
	    for(let key in d){
		db[weekofday][ship_name][equip_name][lv][key] = d[key];
	    }
	    if(d.api_req_slot_id){
		db[weekofday][ship_name][equip_name][lv].api_req_slot_name = KanColleDatabase.masterSlotitem.get(d.api_req_slot_id).api_name;
	    }

	    for(let day in db){
		if(day != weekofday){
		    for(let lv in db[weekofday][ship_name][equip_name]){
			if(db[day][""][equip_name]){
			    db[day][""][equip_name][lv] = db[weekofday][ship_name][equip_name][lv];
			} else if(db[day][ship_name]&&db[day][ship_name][equip_name]){
			    db[day][ship_name][equip_name][lv] = db[weekofday][ship_name][equip_name][lv];
			}
		    }
		} else if(ship_name==""){
		    for(let _ship_name in db[weekofday]){
			if(_ship_name!=""&&db[weekofday][_ship_name][equip_name]){
			    delete db[weekofday][_ship_name][equip_name];
			}
		    }
		} else if(db[weekofday][""]&&db[weekofday][""][equip_name]){
		    for(let lv = 0; lv < 3; lv++){
			for(let _key in db[weekofday][ship_name][equip_name][lv]){
			    db[weekofday][""][equip_name][lv][_key] = db[weekofday][ship_name][equip_name][lv][_key];
			}
		    }
		    delete db[weekofday][ship_name][equip_name];
		}
	    }
	}

	this.save(db, true);
    };

    this._update = {
	reqKousyouRemodelSlotlist: function(){
	    let data = KanColleDatabase.reqKousyouRemodelSlotlist.get();

	    if (!data)
		return;

	    this._set_data(data, 0);

	    return [];
	},
	reqKousyouRemodelSlotlistDetail: function(){
	    let data = KanColleDatabase.reqKousyouRemodelSlotlistDetail.get();
	    let req = KanColleDatabase.reqKousyouRemodelSlotlistDetail.get_req();
	    let item = KanColleDatabase.slotitem.get(req._slot_id);
	    if(!item)
		return;

	    let req_slot_id = item.api_slotitem_id;
	    let lv = item.api_level < 6 ? 0 : item.api_level < 10 ? 1 : 2;
	    let _data = {};
	    _data.api_id = parseInt(req._id,10);
	    _data.api_slot_id = parseInt(req_slot_id,10);

	    for(let k in data)
		_data[k] = data[k];

	    this._set_data([_data], lv);

	    return [];
	},
    };

    this.get = function(id) {
	return this._db[id] || {};
    };

    this.list = function() {
	return Object.keys(this._db);
    };

    this._update_init();
};
KanCollePowerupDB.prototype = new KanColleCombinedDB();

/*
 * ハンドラ一覧。
 * 2段階で要求/応答に応じたハンドラを定義。
 */
var KanColleTimerHandlers = [
    {
	regexp: /^https?:\/\/.*(\/kcsapi\/.*)/,
	request: null,
	response: function(s, match) {
	    let data;
	    try {
		let d = JSON.parse(s.substring(s.indexOf('svdata=') + 7));
		if (d.api_result == 1)
		    data = d;
	    } catch(x) {}
	    if (data) {
		// portの場合 null, それ以外は match[1]
		KanColleDatabase.port.mark(match[1].match(/kcsapi\/api_port\/port/) ? null : match[1]);
	    }
	    return data;
	},
	handlers: [
	    {
		regexp: /kcsapi\/api_start2/,
		response: function(data, match) {
		    this.masterMaparea.update(data.api_data.api_mst_maparea);
		    this.masterMapinfo.update(data.api_data.api_mst_mapinfo);
		    this.masterMission.update(data.api_data.api_mst_mission);
		    this.masterShip.update(data.api_data.api_mst_ship);
		    this.masterSlotitem.update(data.api_data.api_mst_slotitem);
		    this.masterSlotitemEquiptype.update(data.api_data.api_mst_slotitem_equiptype);
		    this.masterStype.update(data.api_data.api_mst_stype);
		}
	    },{
		regexp: /kcsapi\/api_get_master\/maparea/,
		response: function(data, match) {
		    this.masterMaparea.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_get_master\/mission/,
		response: function(data, match) {
		    this.masterMission.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_get_master\/ship/,
		response: function(data, match) {
		    this.masterShip.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_get_master\/slotitem/,
		response: function(data, match) {
		    this.masterSlotitem.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_get_master\/stype/,
		response: function(data, match) {
		    this.masterStype.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_get_member\/basic/,
		response: function(data, match) {
		    this.memberBasic.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_get_member\/deck(_port)?/,
		response: function(data, match) {
		    this.memberDeck.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_get_member\/kdock/,
		response: function(data, match) {
		    this._memberKdock.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_get_member\/mission/,
		response: function(data, match) {
		    this.memberMission.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_get_member\/mapinfo/,
		response: function(data, match) {
		    this.memberMapinfo.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_get_member\/material/,
		response: function(data, match) {
		    this.memberMaterial.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_get_member\/ndock/,
		response: function(data, match) {
		    this._memberNdock.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_get_member\/practice/,
		response: function(data, match) {
		    this.memberPractice.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_get_member\/questlist/,
		response: function(data, match) {
		    this.memberQuestlist.update(data.api_data);
		}
	    },{
		// 2016/04/01
		regexp: /kcsapi\/api_get_member\/require_info/,
		response: function(data, match) {
		    this._memberSlotitem.update(data.api_data.api_slot_item);
		    this.memberUnsetslot.update(data.api_data.api_unsetslot);
		    this._memberKdock.update(data.api_data.api_kdock);
		}
	    },{
		regexp: /kcsapi\/api_get_member\/record/,
		response: function(data, match) {
		    this.memberRecord.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_get_member\/ship2/,
		response: function(data, match) {
		    this._memberShip2.update(data.api_data);
		    this.memberDeck.update(data.api_data_deck);
		}
	    },{
		regexp: /kcsapi\/api_get_member\/ship3/,
		request: function(data, match) {
		    this._memberShip3.prepare(data);
		},
		response: function(data, match) {
		    // もし request に api_shipid が含まれていたら、
		    // その艦船についてのみ送られてくる
		    if (this._memberShip3.get_req().api_shipid)
			this._memberShip3.update(data.api_data.api_ship_data);
		    else
			this._memberShip2.update(data.api_data.api_ship_data);
		    this.memberDeck.update(data.api_data.api_deck_data);
		    this.memberUnsetslot.update(data.api_data.api_slot_data);
		}
	    },{
		regexp: /kcsapi\/api_get_member\/ship_deck/,
		response: function(data, match) {
		    // 2015/05/18
		    this._memberShip3.update(data.api_data.api_ship_data);
		    this.__memberDeck.update(data.api_data.api_deck_data);
		}
	    },{
		regexp: /kcsapi\/api_get_member\/slotitem/,
		response: function(data, match) {
		    this._memberSlotitem.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_get_member\/slot_item/,
		response: function(data, match) {
		    this._memberSlotitem.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_get_member\/unsetslot/,
		response: function(data, match) {
		    this.memberUnsetslot.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_port\/port/,
		response: function(data, match) {
		    this.memberBasic.update(data.api_data.api_basic);
		    this.memberDeck.update(data.api_data.api_deck_port);
		    this.memberMaterial.update(data.api_data.api_material);
		    this._memberShip2.update(data.api_data.api_ship);
		    this._memberNdock.update(data.api_data.api_ndock);
		}
	    },{
		regexp: /kcsapi\/api_req_battle_midnight\/(sp_)?(battle|midnight)/,
		response: function(data, match) {
		    this.reqBattleMidnightBattle.update(data.api_data, {});
		}
	    },{
		regexp: /kcsapi\/api_req_combined_battle\/battleresult/,
		response: function(data, match) {
		    this.reqSortieBattleResult.update(data.api_data, { combined: true });
		}
	    },{
		regexp: /kcsapi\/api_req_combined_battle\/battle_water/,
		response: function(data, match) {
		    this.reqSortieBattle.update(data.api_data, { combined: true, water: true });
		}
	    },{
		regexp: /kcsapi\/api_req_combined_battle\/(ld_air|air)?battle/,
		response: function(data, match) {
		    this.reqSortieBattle.update(data.api_data, { combined: true });
		}
	    },{
		regexp: /kcsapi\/api_req_combined_battle\/goback_port/,
		response: function(data, match) {
		    this.reqCombinedBattleGobackPort.update(data.api_data, { combined: true });
		}
	    },{
		regexp: /kcsapi\/api_req_combined_battle\/(midnight_battle|sp_midnight)/,
		response: function(data, match) {
		    this.reqBattleMidnightBattle.update(data.api_data, { combined: true });
		}
	    },{
		regexp: /kcsapi\/api_req_hensei\/change/,
		request: function(data, match) {
		    data._id = parseInt(data.api_id, 10);
		    data._ship_id = parseInt(data.api_ship_id, 10);
		    data._ship_idx = parseInt(data.api_ship_idx, 10);
		    this.reqHenseiChange.prepare(data);
		},
		response: function(data, match) {
		    this.reqHenseiChange.update();
		}
	    },{
		regexp: /kcsapi\/api_req_hensei\/lock/,
		request: function(data, match) {
		    data._ship_id = parseInt(data.api_ship_id, 10);
		    this.reqHenseiLock.prepare(data);
		},
		response: function(data, match) {
		    this.reqHenseiLock.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_req_hensei\/preset_select/,
		request: function(data, match) {
		    data._deck_id = parseInt(data.api_deck_id, 10);
		    this.reqHenseiPresetSelect.prepare(data);
		},
		response: function(data, match) {
		    this.reqHenseiPresetSelect.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_req_hokyu\/charge/,
		response: function(data, match) {
		    this.reqHokyuCharge.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_req_kaisou\/powerup/,
		request: function(data, match) {
		    data._id_items = data.api_id_items.split(/,/)
					.map(function(v) {
					    return parseInt(v, 10);
					});
		    this.reqKaisouPowerup.prepare(data);
		},
		response: function(data, match) {
		    this.reqKaisouPowerup.update(data.api_data);
		    this.memberDeck.update(data.api_data.api_deck);
		    if (data.api_data.api_ship)
			this._memberShip3.update([data.api_data.api_ship]);
		}
	    },{
		regexp: /kcsapi\/api_req_kaisou\/slot_exchange_index/,
		request: function(data, match) {
		    data._id = parseInt(data.api_id, 10);
		    this.reqKaisouSlotExchangeIndex.prepare(data);
		},
		response: function(data, match) {
		    this.reqKaisouSlotExchangeIndex.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_req_kousyou\/createitem/,
		response: function(data, match) {
		    this.reqKousyouCreateItem.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_req_kousyou\/createship_speedchange/,
		request: function(data, match) {
		    data._kdock_id = parseInt(data.api_kdock_id, 10);
		    this.reqKousyouCreateShipSpeedChange.prepare(data);
		},
		response: function(data, match) {
		    this.reqKousyouCreateShipSpeedChange.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_req_kousyou\/destroyitem2/,
		request: function(data, match) {
		    data._slotitem_ids = data.api_slotitem_ids.split(/,/)
					    .map(function(v) {
						return parseInt(v, 10);
					    });
		    this.reqKousyouDestroyItem2.prepare(data);
		},
		response: function(data, match) {
		    this.reqKousyouDestroyItem2.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_req_kousyou\/destroyship/,
		request: function(data, match) {
		    data._ship_id = parseInt(data.api_ship_id, 10);
		    this.reqKousyouDestroyShip.prepare(data);
		},
		response: function(data, match) {
		    this.reqKousyouDestroyShip.update();
		}
	    },{
		regexp: /kcsapi\/api_req_kousyou\/getship/,
		response: function(data, match) {
		    this.reqKousyouGetShip.update(data.api_data);
		    this._memberKdock.update(data.api_data.api_kdock);
		}
	    },{
		regexp: /kcsapi\/api_req_kousyou\/remodel_slot$/,
		response: function(data, match) {
		    this.reqKousyouRemodelSlot.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_req_kousyou\/remodel_slotlist$/,
		response: function(data, match) {
		    this.reqKousyouRemodelSlotlist.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_req_kousyou\/remodel_slotlist_detail$/,
		request: function(data, match) {
		    data._id = parseInt(data.api_id, 10);
		    data._slot_id = parseInt(data.api_slot_id, 10);
		    this.reqKousyouRemodelSlotlistDetail.prepare(data);
		},
		response: function(data, match) {
		    this.reqKousyouRemodelSlotlistDetail.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_req_map\/(start|next)/,
		request: function(data, match) {
		    // req_map/start
		    if (data.api_deck_id !== undefined)
			data._deck_id = parseInt(data.api_deck_id, 10);
		    if (data.api_maparea_id !== undefined)
			data._maparea_id = parseInt(data.api_maparea_id, 10);
		    if (data.api_mapinfo_no !== undefined)
			data._mapinfo_no = parseInt(data.api_mapinfo_no, 10);
		    if (data.api_formation_id !== undefined)
			data._formation_id = parseInt(data.api_formation_id, 10);

		    // req_map/next
		    if (data.api_recovery_type !== undefined)
			data._recovery_type = parseInt(data.api_recovery_type, 10);
		    this.reqMapStart.prepare(data);
		},
		response: function(data, match) {
		    this.reqMapStart.update(data.api_data, match[1] == 'start');
		}
	    },{
		regexp: /kcsapi\/api_req_member\/get_incentive/,
		response: function(data, match) {
		    this.reqMemberGetIncentive.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_req_member\/get_practice_enemyinfo/,
		response: function(data, match) {
		    this.reqMemberGetPracticeEnemyInfo.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_req_member\/updatedeckname/,
		request: function(data, match) {
		    data._deck_id = parseInt(data.api_deck_id, 10);
		    this.reqMemberUpdateDeckName.prepare(data);
		},
		response: function(data, match) {
		    this.reqMemberUpdateDeckName.update();
		}
	    },{
		regexp: /kcsapi\/api_req_mission\/result/,
		request: function(data, match) {
		    data._deck_id = parseInt(data.api_deck_id, 10);
		    this.reqMissionResult.prepare(data);
		},
		response: function(data, match) {
		    this.reqMissionResult.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_req_mission\/return_instruction/,
		request: function(data, match) {
		    data._deck_id = parseInt(data.api_deck_id, 10);
		    this.reqMissionReturnInstruction.prepare(data);
		},
		response: function(data, match) {
		    this.reqMissionReturnInstruction.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_req_nyukyo\/speedchange/,
		request: function(data, match) {
		    data._ndock_id = parseInt(data.api_ndock_id, 10);
		    this.reqNyukyoSpeedChange.prepare(data);
		},
		response: function(data, match) {
		    this.reqNyukyoSpeedChange.update();
		}
	    },{
		regexp: /kcsapi\/api_req_nyukyo\/start/,
		request: function(data, match) {
		    data._ndock_id = parseInt(data.api_ndock_id, 10);
		    data._ship_id = parseInt(data.api_ship_id, 10);
		    data._highspeed = parseInt(data.api_highspeed, 10);
		    this.reqNyukyoStart.prepare(data);
		},
		response: function(data, match) {
		    this.reqNyukyoStart.update();
		}
	    },{
		regexp: /kcsapi\/api_req_practice\/battle_result/,
		response: function(data, match) {
		    this.reqSortieBattleResult.update(data.api_data, { practice: true });
		}
	    },{
		regexp: /kcsapi\/api_req_practice\/battle/,
		response: function(data, match) {
		    this.reqSortieBattle.update(data.api_data, { practice: true });
		}
	    },{
		regexp: /kcsapi\/api_req_practice\/midnight_battle/,
		response: function(data, match) {
		    this.reqBattleMidnightBattle.update(data.api_data, { practice: true });
		}
	    },{
		regexp: /kcsapi\/api_req_sortie\/battleresult/,
		response: function(data, match) {
		    this.reqSortieBattleResult.update(data.api_data, {});
		}
	    },{
		regexp: /kcsapi\/api_req_sortie\/(ld_air|air)?battle/,
		response: function(data, match) {
		    this.reqSortieBattle.update(data.api_data, {});
		}
	    },{
		regexp: /kcsapi\/api_req_quest\/clearitemget/,
		request: function(data, match) {
		    data._quest_id = parseInt(data.api_quest_id, 10);
		    this.reqQuestClearitemget.prepare(data);
		},
		response: function(data, match) {
		    this.reqQuestClearitemget.update(data.api_data);
		}
	    }
	]
    },{
	regexp: /^https?:\/\/.*\.touken-ranbu\.jp\//,
	request: null,
	response: function(s, match) {
	    let data;
	    try {
		d = JSON.parse(s);
		if (d.status == 0)
		    data = d;
	    } catch(x) {
	    }
	    return data;
	},
	handlers: [
	    {
		regexp: /\/conquest\/complete/,
		response: function(data, match) {
		    this.tourabuConquest.update(data, data.now);
		}
	    },{
		regexp: /\/conquest\/start/,
		response: function(data, match) {
		    this.tourabuConquest.update(data, data.now);
		}
	    },{
		regexp: /\/conquest/,
		response: function(data, match) {
		    this.tourabuConquest.update(data, data.now);
		}
	    },{
		regexp: /\/forge\/complete/,
		request: function(data, match) {
		    this.tourabuForge.prepare(data);
		},
		response: function(data, match) {
		    let d = {};
		    let req = KanColleDatabase.tourabuForge.get_req();
		    d[req.slot_no] = data;
		    this.tourabuForge.update(d, data.now);
		}
	    },{
		regexp: /\/forge\/start/,
		response: function(data, match) {
		    let d = {};
		    d[data.slot_no] = data;
		    this.tourabuForge.update(d, data.now);
		}
	    },{
		regexp: /\/forge/,
		response: function(data, match) {
		    this.tourabuForge.update(data.forge, data.now);
		}
	    },{
		regexp: /\/login\/start/,
		response: function(data, match) {
		    this.tourabuLoginStart.update(data, data.now);
		}
	    },{
		regexp: /\/repair\/repair/,
		response: function(data, match) {
		    this.tourabuRepair.update(data.repair, data.now);
		}
	    },{
		regexp: /\/repair\/complete/,
		response: function(data, match) {
		    // 完了時刻に手入れ画面にいると呼ばれる。
		    // 通知とのraceがあるのでここでは何もしない。
		}
	    },{
		regexp: /\/repair/,
		response: function(data, match) {
		    this.tourabuSword.update(data.sword, data.now);
		    this.tourabuRepair.update(data.repair, data.now);
		}
	    },{
		regexp: /\/sarry/,
		response: function(data, match) {
		    this.tourabuSword.update(data.sword, data.now);
		    this.tourabuRepair.update(data.repair, data.now);
		}
	    }
	]
    }
];

/*
 * 全データベース。
 */
var KanColleDatabase = {
    // Internal variable
    _refcnt: null,

    _db: [
	{   key: 'masterMaparea',		    persist: false },	// master/maparea
	{   key: 'masterMapinfo',		    persist: false },	// master/mapinfo
	{   key: 'masterMission',		    persist: false },	// master/mission
	{   key: 'masterShip',			    persist: true},	// master/ship
	{   key: 'masterSlotitem',		    persist: true },	// master/slotiem
	{   key: 'masterSlotitemEquiptype',	    persist: true },	// api_start2[mst_slotitem_equip_type]
	{   key: 'masterStype',			    persist: true },	// master/stype
	{   key: 'memberBasic',			    persist: false },	// member/basic
	{   key: 'memberDeck',			    persist: false },	// member/deck, member/deck_port
									// or member/ship2[api_data_deck]
									// or member/ship3[api_deck_data]
	{   key: '__memberDeck',		    persist: false },	// member/ship_deck[api_deck_data]
	{   key: '_memberKdock',		    persist: false },	// member/kdock
	{   key: 'memberMaterial',		    persist: false },	// member/material
	{   key: 'memberMapinfo',		    persist: false },	// member/mapinfo
	{   key: 'memberMission',		    persist: false },	// member/mission
	{   key: '_memberNdock',		    persist: false },	// member/ndock
	{   key: 'memberPractice',		    persist: false },	// member/practice
	{   key: 'memberQuestlist',		    persist: false },	// member/questlist
	{   key: 'memberRecord',		    persist: false },	// member/record
	{   key: '_memberShip2',		    persist: false },	// member/ship2
	{   key: '_memberShip3',		    persist: false },	// member/ship3
	{   key: '_memberSlotitem',		    persist: false },	// member/slotitem
	{   key: 'memberUnsetslot',		    persist: false },	// member/unsetslot
									// or member/ship3[api_slot_data]
	{   key: 'reqHenseiChange',		    persist: false },	// req_hensei/change
	{   key: 'reqHenseiLock',		    persist: false },	// req_hensei/lock
	{   key: 'reqHenseiPresetSelect',	    persist: false },	// req_hensei/preset_select
	{   key: 'reqKaisouPowerup',		    persist: false },	// req_kaisou/powerup
	{   key: 'reqKaisouSlotExchangeIndex',	    persist: false },	// req_kaisou/slot_exchange_index
	{   key: 'reqKousyouCreateItem',	    persist: false },	// req_kousyou/create_item
	{   key: 'reqKousyouCreateShipSpeedChange', persist: false },	// req_kousyou/createship_speedchange
	{   key: 'reqKousyouDestroyItem2',	    persist: false },	// req_kousyou/destroyitem2
	{   key: 'reqKousyouDestroyShip',	    persist: false },	// req_kousyou/destroyship
	{   key: 'reqKousyouGetShip',		    persist: false },	// req_kousyou/get_ship
	{   key: 'reqKousyouRemodelSlot',	    persist: false },	// req_kousyou/remodel_slot
	{   key: 'reqKousyouRemodelSlotlist',	    persist: false },	// req_kousyou/remodel_slotlist
	{   key: 'reqKousyouRemodelSlotlistDetail', persist: false },	// req_kousyou/remodel_slotlist_detail
	{   key: 'reqHokyuCharge',		    persist: false },	// req_hokyu/charge
	{   key: 'reqMapStart',			    persist: false },	// req_map/start, req_map/next
	{   key: 'reqMemberGetIncentive',	    persist: false },	// req_member/get_incentive
	{   key: 'reqMemberGetPracticeEnemyInfo',   persist: false },	// req_member/get_practice_enemyinfo
	{   key: 'reqMemberUpdateDeckName',	    persist: false },	// req_member/updatedeckname
	{   key: 'reqMissionResult',		    persist: false },	// req_mission/result
	{   key: 'reqMissionReturnInstruction',	    persist: false },	// req_mission/return_instruction
	{   key: 'reqNyukyoSpeedChange',	    persist: false },	// req_nyukyo/speedchange
	{   key: 'reqNyukyoStart',		    persist: false },	// req_nyukyo/start
	{   key: 'reqQuestClearitemget',	    persist: false },	// req_quest/clearitemget
	{   key: 'reqSortieBattle',		    persist: false },	// req_sortie/battle
	{   key: 'reqSortieBattleResult',	    persist: false },	// req_sortie/battle_result
	{   key: 'reqBattleMidnightBattle',	    persist: false },	// req_battle/midnight_battle
	{   key: 'reqCombinedBattleGobackPort',	    persist: false },	// req_combined_battle/go_back_port
	{   key: 'ndock',			    persist: false,	func: KanColleNdockDB },	// 入渠ドック
	{   key: 'battle',			    persist: false,	func: KanColleBattleDB },	// 戦闘/対戦
	{   key: 'ship',			    persist: false,	func: KanColleShipDB },		// 艦船
	{   key: 'deck',			    persist: false,	func: KanColleDeckDB },		// デッキ
	{   key: 'slotitem',			    persist: false,	func: KanColleSlotitemDB },	// 装備保持艦船
	{   key: 'record',			    persist: false,	func: KanColleRecordDB },	// 艦船/装備
	{   key: 'quest',			    persist: false,	func: KanColleQuestDB },	// 任務(クエスト)
	{   key: 'mission',			    persist: false,	func: KanColleMissionDB },	// 遠征一覧
	{   key: 'practice',			    persist: false,	func: KanCollePracticeDB },	// 演習
	{   key: 'material',			    persist: false,	func: KanColleMaterialDB },	// 資源/資材
	{   key: 'kdock',			    persist: false,	func: KanColleKdockDB },	// 建造ドック
	{   key: 'port',			    persist: false,	func: KanCollePortDB },		// 母港
	{   key: 'powerup',			    persist: false,	func: KanCollePowerupDB },	// 改修工廠
	// TouRabu
	{   key: 'tourabuConquest',		    persist: false },	// conquest, conquest/start, conquest/complete
	{   key: 'tourabuForge',		    persist: false },	// forge, forge/start, forge/complete
	{   key: 'tourabuLoginStart',		    persist: false },	// login/start
	{   key: 'tourabuRepair',		    persist: false },	// repair, repair/repair, repair/complete
	{   key: 'tourabuSword',		    persist: false },	// sword information
    ],

    // Callback
    _callback: function(req, s, mode) {
	let that = KanColleDatabase;
	let url = req.name;
	let m;

	if (!mode || mode == 'http-on-examine-response') {
	    m = 'response';
	} else if (mode == 'http-on-modify-request') {
	    m = 'request';
	} else {
	    return;
	}

	KanColleTimerHandlers.find(function(domain) {
	    let data = undefined;
	    let match = domain.regexp.exec(url);
	    if (!match)
		return false;
	    //debugprint('Domain match: ' + match.toSource());
	    if (domain[m]) {
		//debugprint('Parser: ' + m);
		data = domain[m].call(that, s, match);
	    } else {
		//debugprint('Parser(default): ' + m);
		if (m == 'request') {
		    let postdata = s.substring(s.indexOf('\r\n\r\n') + 4).split('&');
		    let k,v,t;
		    let d = new Object();
		    for (let i = 0; i < postdata.length; i++){
			let idx;
			let e;

			t = postdata[i];
			idx = t.indexOf('=');
			try{
			    if (idx >= 0) {
				k = decodeURIComponent(t.substring(0, idx));
				v = decodeURIComponent(t.substring(idx + 1));
			    }
			    if (d[k])
				debugprint('overriding data for ' + k + '; ' + d[k]);
			    d[k] = v;
			} catch(e) {
			}
		    }
		    data = d;
		} else {
		    // no default handler.
		}
	    }
	    if (data !== undefined) {
		domain.handlers.find(function(handler) {
		    let match = handler.regexp.exec(url);
		    if (!match)
			return false;
		    //debugprint('Handler match: ' + match.toSource());
		    if (handler[m])
			handler[m].call(that, data, match);
		    return true;
		});
	    }
	    return true;
	});
	//debugprint('done');
    },

    // database registration
    _registerDatabase: function() {
	for (let i = 0; i < this._db.length; i++) {
	    let param = this._db[i];
	    let func;
	    let obj;

	    if (this[param.key] && param.persist)
		continue;

	    debugprint("Registering database " + param.key);

	    func = param.func || KanColleDB;
	    obj = this[param.key] = new func;
	    if (obj.init)
		obj.init();
	    this._db[param.key] = param.persist === true;
	}
	debugprint("KanColleDatabase initialized.");
    },
    _unregisterDatabase: function() {
	for (let i = this._db.length - 1; i >= 0; i--) {
	    let param = this._db[i];
	    if (!this[param.key] || param.persist)
		return;

	    debugprint("Unregistering database " + param.key);

	    if (this[param.key].exit)
		this[param.key].exit();
	    delete this[param.key];
	}
	debugprint("KanColleDatabase cleared.");
    },

    // Initialization
    init: function() {
	if (this._refcnt === null) {
	    this._callback = this._callback.bind(this);
	    this._refcnt = 0;
	}

	if (!this._refcnt++) {
	    // Initialize
	    KanColleHttpRequestObserver.init();
	    this._registerDatabase();
	    // Start
	    KanColleHttpRequestObserver.addCallback(this._callback);
	}
    },

    exit: function() {
	if (!--this._refcnt) {
	    // Stop
	    KanColleHttpRequestObserver.removeCallback(this._callback);
	    // Unregister
	    this._unregisterDatabase();
	    KanColleHttpRequestObserver.destroy();
	}
    },
};

/*
 * 雑多な共有情報。
 * 本当はなくしたい。
 */
var KanColleRemainInfo = {
    cookie: {},	//重複音対策

    mission:	{}, // 遠征名

    ndock_memo: [], // 入渠ドック用のメモ

    // 残り時間
    ndock: [],
    kdock: [],
    fleet: [],
    tourabu_ndock: [],
    tourabu_kdock: [],
    tourabu_fleet: [],
};

/*
 * ライブラリ
 */
const Cc = Components.classes;
const Ci = Components.interfaces;

function debugprint(str){
    KanColleTimerUtils.console.log(str);
}

function CCIN(cName, ifaceName) {
    return Cc[cName].createInstance(Ci[ifaceName]);
}

/*
 * 通信傍受
 */
var callback = new Array();

function TracingListener() {
    this.originalListener = null;
}
TracingListener.prototype =
{
    onDataAvailable: function(request, context, inputStream, offset, count) {
        var binaryInputStream = CCIN("@mozilla.org/binaryinputstream;1",
				     "nsIBinaryInputStream");
        var storageStream = CCIN("@mozilla.org/storagestream;1", "nsIStorageStream");
        var binaryOutputStream = CCIN("@mozilla.org/binaryoutputstream;1",
				      "nsIBinaryOutputStream");

        binaryInputStream.setInputStream(inputStream);
        storageStream.init(8192, count, null);
        binaryOutputStream.setOutputStream(storageStream.getOutputStream(0));

        // Copy received data as they come.
        var data = binaryInputStream.readBytes(count);
        this.receivedData.push(data);

        binaryOutputStream.writeBytes(data, count);

        this.originalListener.onDataAvailable(request, context,
					      storageStream.newInputStream(0), offset, count);
    },

    onStartRequest: function(request, context) {
        this.originalListener.onStartRequest(request, context);
	this.receivedData = new Array();
    },

    onStopRequest: function(request, context, statusCode) {
        this.originalListener.onStopRequest(request, context, statusCode);

	var s = this.receivedData.join('');
	for( var k in  callback ){
	    var f = callback[k];
	    if( typeof f=='function' ){
		f( request, s, 'http-on-examine-response' );
	    }
	}
    },

    QueryInterface: function (aIID) {
        if (aIID.equals(Ci.nsIStreamListener) ||
            aIID.equals(Ci.nsISupports)) {
            return this;
        }
        throw Components.results.NS_NOINTERFACE;
    }
};

var KanColleHttpRequestObserver =
{
    counter: 0,

    observe: function(aSubject, aTopic, aData){
        if (aTopic == "http-on-examine-response"){
	    var httpChannel = aSubject.QueryInterface(Components.interfaces.nsIHttpChannel);

	    if (KanColleTimerHandlers.some(function(domain) {
						return !!domain.regexp.exec(httpChannel.URI.spec);
					   })
	       ) {
		//debugprint(httpChannel.URI.spec);
		var newListener = new TracingListener();
		aSubject.QueryInterface(Ci.nsITraceableChannel);
		newListener.originalListener = aSubject.setNewListener(newListener);
	    }
	}

	if( aTopic=="http-on-modify-request" ){
	    var httpChannel = aSubject.QueryInterface(Components.interfaces.nsIHttpChannel);
	    if (httpChannel.requestMethod == 'POST' &&
	        KanColleTimerHandlers.some(function(domain) {
						return !!domain.regexp.exec(httpChannel.URI.spec);
					   })
	       ) {
		httpChannel.QueryInterface(Components.interfaces.nsIUploadChannel);
		var us = httpChannel.uploadStream;
		us.QueryInterface(Components.interfaces.nsISeekableStream);
		var ss = CCIN("@mozilla.org/scriptableinputstream;1",
			      "nsIScriptableInputStream");
		ss.init(us);
		us.seek(0, 0);
		var n = ss.available();
		var postdata = ss.read(n);
		us.seek(0, 0);

		for( let k in callback ){
		    let f = callback[k];
		    if (typeof(f) == 'function')
			f({ name: httpChannel.URI.spec }, postdata, aTopic);
		}

	    }
        }
    },

    QueryInterface : function (aIID) {
        if (aIID.equals(Ci.nsIObserver) || aIID.equals(Ci.nsISupports)){
            return this;
        }
        throw Components.results.NS_NOINTERFACE;
    },

    addCallback: function(f){
	callback.push( f );
	debugprint( 'add callback='+callback.length );
    },

    removeCallback: function( f ){
	for( var k in callback ){
	    if( callback[k]==f ){
		callback.splice(k,1);
		debugprint( 'remove callback='+callback.length );
	    }
	}
    },

    init: function(){
	if( this.counter==0 ){
	    this.observerService = Components.classes["@mozilla.org/observer-service;1"]
		.getService(Components.interfaces.nsIObserverService);
	    this.observerService.addObserver(this, "http-on-examine-response", false);
	    this.observerService.addObserver(this, "http-on-modify-request", false);
	    debugprint("start kancolle observer.");
	}
	this.counter++;
    },

    destroy: function(){
	this.counter--;
	if( this.counter<=0 ){
	    this.observerService.removeObserver(this, "http-on-examine-response");
	    this.observerService.removeObserver(this, "http-on-modify-request");
	    this.counter = 0;
	    debugprint("stop kancolle observer.");
	}
    }

};
