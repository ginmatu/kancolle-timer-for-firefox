// vim: set ts=8 sw=4 sts=4 ff=dos :

var SSTweet = {

    getTempFile:function(){
        let file = Components.classes["@mozilla.org/file/directory_service;1"].getService(Components.interfaces.nsIProperties).get("TmpD", Components.interfaces.nsIFile);
        file.append("kancolletimer-ss-temp.jpg");
	return file;
    },

    presend: function() {
	$('send-button').disabled = true;

	let data = $('ss-image').src;

	let file = this.getTempFile();
	debugprint(file.path);

	const IO_SERVICE = Components.classes['@mozilla.org/network/io-service;1']
            .getService(Components.interfaces.nsIIOService);
	try {
	    data = IO_SERVICE.newURI(data, null, null);
	} catch(x) {
	    debugprint('failed to get screenshot.');
	    return;
	}

	debugprint('saving file...');

	KanColleTimerUtils.file.saveURI(file, data, function() {
					    debugprint('done.');
					    $('send-button').disabled = false;
					});
    },

    send: function(){
	let text = $('text').value;
	let file = this.getTempFile();
	Twitter.updateStatusWithMedia(text, File(file.path));
	//Twitter.updateStatus(text);
	setTimeout( function(){ $('send-button').disabled = false; }, 1000 );
    },

    init:function(){
	let data = window.arguments && window.arguments[0];
	if (!data)
	    data = TakeKanColleScreenshot(true);
	if (data)
	    $('ss-image').src = data.spec;
	$('text').focus();

	if( !Twitter.getScreenName() ){
	    Twitter.getRequestToken();
	}else{
	    $('screen-name').value = "@" + Twitter.getScreenName();
	}

	this.presend();
    },
    destroy: function(){
    }

};

window.addEventListener("load", function(e){ SSTweet.init(); }, false);
window.addEventListener("unload", function(e){ SSTweet.destroy(); }, false);
