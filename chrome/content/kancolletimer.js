// vim: set ts=8 sw=4 sts=4 ff=dos :

// http://www.dmm.com/netgame/social/application/-/detail/=/app_id=854854/

Components.utils.import("chrome://kancolletimer/content/httpobserve.jsm");
Components.utils.import("chrome://kancolletimer/content/utils.jsm");

var KanColleTimer = {
    imageURL: "http://pics.dmm.com/freegame/app/854854/200.jpg",
    tourabu_imageURL: "http://pics.dmm.com/freegame/app/825012/200.gif",

    panels: [
	KanColleTimerHeadQuarterInfo,
	KanColleTimerDeckInfo,
	KanColleTimerNdockInfo,
	KanColleTimerKdockInfo,
	KanColleTimerQuestInfo,
	KanColleTimerMapInfo,
	KanColleTimerFleetInfo,
	KanColleTimerShipTable,
	KanColleTimerPracticeInfo,
	KanColleTimerMissionBalanceInfo,
	TouRabuTimerDeckInfo,
	TouRabuTimerNdockInfo,
	TouRabuTimerKdockInfo,
    ],

    tourabu_ndock: [],
    tourabu_kdock: [],
    tourabu_fleet: [],

    cookie:{},

    // 通知
    notify: function(type,i,str,imageURL){
	let coretype = type.replace(/^1min\./,'');
	let sound_conf = 'sound.' + (type.replace(/tourabu_/, ''));
	let popup_conf = 'popup.' + (coretype.replace(/tourabu_/, ''));
	let is1min = type != coretype;
	let cookie = 'kancolletimer.' + coretype + '.' + i;
	let path = KanColleTimerConfig.getUnichar(sound_conf);

	if (!imageURL)
	    imageURL = this.imageURL;

	$(sound_conf).play();
	if( KanColleTimerConfig.getBool(popup_conf) &&
	    (!is1min || KanColleTimerConfig.getBool('popup.1min-before')) ){
	    KanColleTimerUtils.alert.show(imageURL, '艦これタイマー', str, false, cookie, null);
	}
    },

    // ウィンドウを最前面にする
    setWindowOnTop:function(){
	let checkbox = $('window-stay-on-top');
	if (checkbox)
	    WindowOnTop( window, checkbox.hasAttribute('checked') );
    },

    setGeneralTimerByTime: function(timeout){
	KanColleTimerHeadQuarterInfo.start_general_timer(timeout);
    },

    setGeneralTimer: function(sec){
	KanColleTimerHeadQuarterInfo.start_general_timer_by_delay(sec);
    },

    update: function(){
	let cur = (new Date).getTime();
	let that = this;

	function check_cookie(cookie,type,no,time) {
	    let k;
	    let v;
	    k = type + '_' + no;
	    v = cookie[k];

	    //debugprint('k=' + k + '; v=' + v + ', time=' + time);

	    if (!v || time > cur) {
		ret = v != time;
		if (ret)
		    cookie[k] = time;
	    } else {
		cookie[k] = time;
		ret = false;
	    }
	    return ret;
	}

	function check_timeout(type,list,titlefunc,imageURL){
	    for( let i in KanColleRemainInfo[list] ){
		i = parseInt(i,10);
		let t = KanColleRemainInfo[list][i].finishedtime;
		let d = t - cur;

		if (isNaN(d) || d >= 60000) {
		    check_cookie(that.cookie,type,i,0);
		    check_cookie(KanColleRemainInfo.cookie,type,i,0);
		} else if (d >= 0) {
		    let str = 'まもなく' + titlefunc(i) + 'します。\n';
		    if (check_cookie(KanColleRemainInfo.cookie,'1min.' + type,i,t))
			that.notify('1min.' + type,i,str,imageURL);
		} else {
		    let str = titlefunc(i) + 'しました。\n';
		    if (check_cookie(that.cookie,type,i,t))
			AddLog(str);
		    if (check_cookie(KanColleRemainInfo.cookie,type,i,t))
			that.notify(type,i, str,imageURL);
		}
	    }
	}

	check_timeout('tourabu_mission', 'tourabu_fleet', function(i){ return '第' + (i+1) + '部隊が遠征から帰還'; }, this.tourabu_imageURL);
	check_timeout('tourabu_ndock',   'tourabu_ndock', function(i){ return '第' + (i+1) + '工房の手入が完了'; }, this.tourabu_imageURL);
	check_timeout('tourabu_kdock',   'tourabu_kdock', function(i){ return '第' + (i+1) + '工房の錬刀が完了'; }, this.tourabu_imageURL);
    },

    getNowDateString: function(){
	var d = new Date();
	var month = d.getMonth()+1;
	month = month<10 ? "0"+month : month;
	var date = d.getDate()<10 ? "0"+d.getDate() : d.getDate();
	var hour = d.getHours()<10 ? "0"+d.getHours() : d.getHours();
	var min = d.getMinutes()<10 ? "0"+d.getMinutes() : d.getMinutes();
	var sec = d.getSeconds()<10 ? "0"+d.getSeconds() : d.getSeconds();
	var ms = d.getMilliseconds();
	if( ms<10 ){
	    ms = "000" + ms;
	}else if( ms<100 ){
	    ms = "00" + ms;
	}else if( ms<1000 ){
	    ms = "0" + ms;
	}
	return "" + d.getFullYear() + month + date + hour + min + sec + ms;
    },

    /**
     * スクリーンショット撮影
     * @param isjpeg JPEGかどうか
     */
    _takeScreenshot: function(isjpeg){
	let url = TakeKanColleScreenshot(isjpeg);
	if (!url) {
	    AlertPrompt("艦隊これくしょんのページが見つかりませんでした。","艦これタイマー");
	    return null;
	}
	return url;
    },

    /**
     * スクリーンショット撮影
     */
    takeScreenshot: function(){
	let ret;
	let defaultdir = KanColleTimerConfig.getUnichar("screenshot.path");
	let isjpeg = KanColleTimerConfig.getBool("screenshot.jpeg");
	let nsIFilePicker = Components.interfaces.nsIFilePicker;
	let fp = Components.classes["@mozilla.org/filepicker;1"].createInstance(nsIFilePicker);
	let url = this._takeScreenshot(isjpeg);

	if (!url)
	    return;

	fp.init(window, "保存ファイルを選んでください", nsIFilePicker.modeSave);
	if (defaultdir) {
	    let file = KanColleTimerUtils.file.initWithPath(defaultdir);
	    if (file.exists() && file.isDirectory())
		fp.displayDirectory = file;
	}
	fp.appendFilters(nsIFilePicker.filterImages);
	fp.defaultString = "screenshot-"+ this.getNowDateString() + (isjpeg?".jpg":".png");
	fp.defaultExtension = isjpeg ? "jpg" : "png";
	ret = fp.show();
	if ((ret != nsIFilePicker.returnOK && ret != nsIFilePicker.returnReplace) || !fp.file)
	    return null;

	KanColleTimerUtils.file.saveURI(fp.file, url);
    },

    /**
     * スクリーンショット連続撮影用フォルダ選択
     * @return nsIFile
     */
    takeScreenshotSeriographySelectFolder: function(){
	let defaultdir = KanColleTimerConfig.getUnichar("screenshot.path");
	let ret;
	let nsIFilePicker = Components.interfaces.nsIFilePicker;
	let fp = Components.classes["@mozilla.org/filepicker;1"].createInstance(nsIFilePicker);

	fp.init(window, "保存フォルダを選んでください", nsIFilePicker.modeGetFolder);
	if (defaultdir) {
	    let file = KanColleTimerUtils.file.initWithPath(defaultdir);
	    if (file.exists() && file.isDirectory())
		fp.displayDirectory = file;
	}
	ret = fp.show();
	if (ret != nsIFilePicker.returnOK || !fp.file)
	    return null;

	KanColleTimerConfig.setUnichar("screenshot.path", fp.file.path);

	return fp.file;
    },

    /**
     * スクリーンショット連続撮影
     */
    takeScreenshotSeriography: function(){
	let isjpeg = KanColleTimerConfig.getBool("screenshot.jpeg");
	let url = this._takeScreenshot(isjpeg);
	let file = null;
	let dir;

	if (!url)
	    return;

	dir = KanColleTimerConfig.getUnichar("screenshot.path");
	if (dir) {
	    file = KanColleTimerUtils.file.initWithPath(dir);
	}

	// フォルダのチェック。フォルダでなければ、(再)選択
	do {
	    if (file && file.exists() && file.isDirectory())
		break;
	    file = this.takeScreenshotSeriographySelectFolder();
	} while(file);

	// エラー
	if (!file)
	    return null;

	file.append("screenshot-" + this.getNowDateString() + (isjpeg ? '.jpg' : '.png'));

	KanColleTimerUtils.file.saveURI(file, url);
    },

    /**
     * 艦これタイマーを開く
     */
    open:function(){
	let feature="chrome,resizable=yes";

	let win = KanColleUtils.findMainWindow();
	if(win){
	    win.focus();
	}else{
	    let w = window.open("chrome://kancolletimer/content/mainwindow.xul","KanColleTimer",feature);
	    w.focus();
	}
    },

    startTimer: function() {
	if (this._timer)
	    return;
	this._timer = setInterval(this.update.bind(this), 1000);
    },

    stopTimer: function() {
	if (!this._timer)
	    return;
	clearInterval(this._timer);
	this._timer = null;
    },

    initPowerupDB: function() {
	let db = KanColleUtils.readObject('powerup.json', null);
	if(db != null){
	    KanColleDatabase.powerup.save(db, false);
	    return;
	}
	let uri = KanColleUtils.getUnicharPref('powerupdata.uri', 'https://docs.google.com/spreadsheets/d/1UOo5VpFZF-Ee_NUZ2T_ECMBajfIaIUkIs9xVYBkPcv4/export?format=tsv&id=1UOo5VpFZF-Ee_NUZ2T_ECMBajfIaIUkIs9xVYBkPcv4&gid=1529761687')
	let req = Components.classes["@mozilla.org/xmlextras/xmlhttprequest;1"]
		    .createInstance()
		    .QueryInterface(Components.interfaces.nsIDOMEventTarget);
	req.open('GET', uri, false);
	req.addEventListener('load', function() {
	    let lines = req.responseText
			.replace(/／/g, "/")
			.replace(/10cm高角砲＋高射装置/g,"10cm連装高角砲+高射装置")
			.split( /\r\n|\n|\r/ );
	    for( let i = 1, line; line = lines[i]; i++ ){
		let row = line.split( /\t/ );
		if( row.length < 9 ) continue;
		let equip_name = row[0];
		let ship_name = row[8];
		for( let j = 0; j < 7; j++ ){
		    if( row[j+1] == "1" ){
			if(!db) db = {};
			if(!db[j]) db[j] = {};
			if(!db[j][ship_name]) db[j][ship_name] = {};
			if(!db[j][ship_name][equip_name]) db[j][ship_name][equip_name] = [{},{},{}];
		    }
		}
	    }
	    KanColleDatabase.powerup.save(db, true);
	});
	req.send();
    },

    init: function(){
	KanColleDatabase.init();

	for (let i = 0; i < this.panels.length; i++) {
	    let panel = this.panels[i];
	    if (panel.init)
		panel.init();
	}

	this.startTimer();

	for (let i = 0; i < this.panels.length; i++) {
	    let panel = this.panels[i];
	    if (panel.restore)
		panel.restore();
	}

	this.setWindowOnTop();

	for (let i = 0; i < this.panels.length; i++) {
	    let panel = this.panels[i];
	    if (panel.start)
		panel.start();
	}

	this.initPowerupDB();
    },

    destroy: function(){
	for (let i = this.panels.length - 1; i >= 0; i--) {
	    let panel = this.panels[i];
	    if (panel.stop)
		panel.stop();
	}

	this.stopTimer();

	for (let i = this.panels.length - 1; i >= 0; i--) {
	    let panel = this.panels[i];
	    if (panel.exit)
		panel.exit();
	}

	KanColleDatabase.exit();
    }
};


window.addEventListener("load", function(e){ KanColleTimer.init(); }, false);
window.addEventListener("unload", function(e){ KanColleTimer.destroy(); }, false);
