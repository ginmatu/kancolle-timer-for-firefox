// vim: set ts=8 sw=4 sts=4 ff=dos :

Components.utils.import("resource://gre/modules/ctypes.jsm");
Components.utils.import("chrome://kancolletimer/content/httpobserve.jsm");
Components.utils.import("chrome://kancolletimer/content/utils.jsm");

/**
 * いろいろと便利関数などを.
 */

const Cc = Components.classes;
const Ci = Components.interfaces;

const MODE_SAVE = Ci.nsIFilePicker.modeSave;

const XUL_NS = "http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul";
const HTML_NS= "http://www.w3.org/1999/xhtml";

const ADDON_ID = "kancolletimer-kai@st-paulia.net";

var __KanColleTimerPanel = {
    update: null,
    _update_bound: null,

    _update_start: function() {
	for (let [k, f] in Iterator(this._update_bound))
	    KanColleDatabase[k].appendCallback(f);
    },
    _update_stop: function() {
	for (let [k, f] in Iterator(this._update_bound))
	    KanColleDatabase[k].removeCallback(f);
    },

    _update_init: function() {
	if (!this._update_bound) {
	    this._update_bound = {};
	    if (this.update) {
		for (let k in this.update) {
		    let f = this.update[k];
		    let visited = {};   // loop detection
		    while (typeof(f) == 'string' && !visited[f]) {
			visited[f] = true;
			f = this.update[f];
		    }
		    this._update_bound[k] = f.bind(this);
		}
	    }
	}
    },
    _update_exit: function() {
	this._update_bound = null;
    },

    start: function() {
	this._update_start();
    },
    stop: function() {
	this._update_stop();
    },

    init: function() {
	this._update_init();
    },
    exit: function() {
	this._update_exit();
    },
};

/*
 * 艦娘/装備数
 */
var KanColleTimerHeadQuarterInfo = {
    lonely_ships: [],
    repair_timer: {
	base: Number.NaN,
	_timer: null,
    },
    general_timer: {
	_timer: null,
    },

    update: {
	ship: function() {
	    let remodel_map = {};
	    let ship_bytype = KanColleDatabase.ship.list().map(function(id){
		let ship = KanColleDatabase.ship.get(id);
		let typeid = ship.api_ship_id;
		let name = FindShipNameByCatId(typeid);
		let locked = ship.api_locked;
		if (remodel_map[typeid]) {
		    typeid = remodel_map[typeid];
		} else {
		    let remodel_type = {};	// loop detection
		    let highest_id = -1;
		    while (true) {
			let shiptype = KanColleDatabase.masterShip.get(typeid);
			let aftertypeid = shiptype ? parseInt(shiptype.api_aftershipid, 10) : Number.NaN;

			if (highest_id < typeid)
			    highest_id = typeid;
			remodel_type[typeid] = true;

			if (isNaN(aftertypeid) || !aftertypeid)
			    break;
			if (remodel_type[aftertypeid]) {
			    /* Loop.  Use highest id. */
			    let types = Object.keys(remodel_type);
			    debugprint('Loop detected: ' + name + ' [' + types.map(function(_id) { return FindShipNameByCatId(_id); }) + '] => ' + FindShipNameByCatId(highest_id));
			    typeid = highest_id;
			    break;
			}
			typeid = aftertypeid;
		    }
		    remodel_map[ship.api_ship_id] = typeid;
		}
		return { id: id, name: name, lv: ship.api_lv, type: typeid, locked: locked, };
	    }).reduce(function(p,c) {
		if (!p[c.type])
		    p[c.type] = [];
		p[c.type].push(c);
		return p;
	    }, {});
	    this.lonely_ships = Object.keys(ship_bytype).filter(function(e) {
		ship_bytype[e].sort(function(a,b) {
		    return b.lv - a.lv;
		});
		return !ship_bytype[e][0].locked;
	    }).map(function(e) {
		return ship_bytype[e];
	    });
	    this.update.record.call(this);
	},
	record: function() {
	    let d;
	    let maxships;
	    let maxslotitems;
	    let ships;
	    let slotitems;
	    let shipnumfree = KanColleTimerConfig.getInt('display.ship-num-free');
	    let ship_color = null;
	    let slotitem_color = null;
	    let text;

	    function convnan(t) {
		return isNaN(t) ? '-' : t;
	    }

	    function numcolor(cur,mark,max) {
		let col = null;
		if (!isNaN(cur)) {
		    if (!isNaN(max) && cur >= max)
			col = 'red';
		    else if (!isNaN(mark) && cur >= mark)
			col = 'orange';
		    else
			col = 'black';
		}
		return col;
	    }

	    d = KanColleDatabase.record.get();

	    ships = convnan(d.api_ship[0]);
	    maxships = convnan(d.api_ship[1]);
	    ship_color = numcolor(ships, maxships - shipnumfree, maxships);

	    slotitems = convnan(d.api_slotitem[0]);
	    maxslotitems = convnan(d.api_slotitem[1]);
	    slotitem_color = numcolor(slotitems, maxslotitems - shipnumfree * 4, maxslotitems);

	    // 母港100、装備枠500から、母港拡張10ごとに装備枠40増加
	    //   装備枠 = 500 + (母港-100) * 4
	    //
	    // APIでから渡される装備数上限は、それ以上の数では新艦船/
	    // 新装備開発ができなくなる、という実際の制限値であり、
	    // 戦績表示の「最大保有可能装備アイテム数」から3減じた
	    // ものとなっている。
	    //
	    // このため、建造やドロップでの艦船取得によって装備数が
	    // 最大4つ増え、装備数がAPIによる最大装備数を上回ることは
	    // ある。
	    //
	    //if (!isNaN(maxslotitems) && !isNaN(maxships) &&
	    //	maxslotitems < 100 + maxships * 4) {
	    //	maxslotitems = 100 + maxships * 4;
	    //}
	    $('basic-information-shipcount').value = ships;
	    SetStyleProperty($('basic-information-shipcount'), 'color', ship_color);
	    text = ships + ' / ' + maxships;
	    if (this.lonely_ships.length) {
		text += '\n未ロック艦: ' +
			this.lonely_ships.map(function(e) {
			    return e[0].name + ' Lv' + e[0].lv;
			}).join(', ');
		SetStyleProperty($('basic-information-shipcount'), 'outline', 'red solid 2px');
	    } else {
		SetStyleProperty($('basic-information-shipcount'), 'outline', 'rgba(0,0,0,0) solid 2px');
	    }
	    $('basic-information-shipcount').setAttribute('tooltiptext', text);

	    $('basic-information-slotitemcount').value = slotitems;
	    SetStyleProperty($('basic-information-slotitemcount'), 'color', slotitem_color);
	    $('basic-information-slotitemcount').setAttribute('tooltiptext', slotitems + ' / ' + maxslotitems);
	},

	material: function() {
	    let burner = '-';
	    let bucket = '-';
	    let screw = '-';
	    if (KanColleDatabase.material.timestamp()) {
		let d;
		/*
		 * 1: 燃料
		 * 2: 弾薬
		 * 3: 鋼材
		 * 4: ボーキサイト
		 * 5: 高速建造材
		 * 6: 高速修復材
		 * 7: 開発資材
		 * 8: 改修資材
		 */
		burner = KanColleDatabase.material.get('burner');
		bucket = KanColleDatabase.material.get('bucket');
		screw = KanColleDatabase.material.get('screw');
	    }

	    $('basic-information-burnercount').value = burner;
	    $('basic-information-bucketcount').value = bucket;
	    $('basic-information-screwcount').value = screw;
	},

	port: function() {
	    // 泊地修理タイマー
	    let data = KanColleDatabase.port.get();
	    this.repair_timer.base = data.repair_timer;
	},
    },

    show: function() {
	let nodes, checked;

	checked = $('basicinfo-toggle').getAttribute('checked') == 'true';
	$('basicinfo').collapsed = !checked;

	checked = $('porttimer-toggle').getAttribute('checked') == 'true';
	nodes = document.getElementsByClassName("porttimer");
	for (let i = 0; nodes[i]; i++)
	    nodes[i].collapsed = !checked;

	checked = $('gentimer-toggle').getAttribute('checked') == 'true';
	nodes = document.getElementsByClassName("gentimer");
	for (let i = 0; nodes[i]; i++)
	    nodes[i].collapsed = !checked;

	checked = $('porttimer-toggle').getAttribute('checked') == 'true' ||
		  $('gentimer-toggle').getAttribute('checked') == 'true' ||
		  $('deckinfo-toggle').getAttribute('checked') == 'true' ||
		  $('ndockinfo-toggle').getAttribute('checked') == 'true' ||
		  $('kdockinfo-toggle').getAttribute('checked') == 'true';
	$('timer').collapsed = !checked;
    },

    restore: function() {
	let general_timer = KanColleTimerConfig.getUnichar('timer.general-timer') || null;
	if (!isNaN(general_timer)) {
	    this.start_general_timer(general_timer);
	}
	this.show();
    },

    _update_general_timer: function() {
	$('sound.default').play();
	if( KanColleTimerConfig.getBool('popup.general-timer') ){
	    let str = "時間になりました。";
	    KanColleTimerUtils.alert.show(KanColleUtils.IconURL,
					  KanColleUtils.Title,
					  str, false, str, null);
	}
    },
    start_general_timer: function(time) {
	this.stop_general_timer();
	if (time) {
	    this._start_general_timer(time);
	    $('general-timer').finishTime = time;
	    KanColleTimerConfig.setUnichar('timer.general-timer', '' + time);
	}
    },
    start_general_timer_by_delay: function(delay) {
	let _delay = parseInt(delay, 10);
	if (isNaN(_delay))
	    _delay = 0;
	if (_delay)
	    this.start_general_timer((new Date).getTime() + _delay * 1000);
	else
	    this.stop_general_timer();
    },
    stop_general_timer: function() {
	this._stop_general_timer();
	$('general-timer').finishTime = '';
	KanColleTimerConfig.setUnichar('timer.general-timer', '');
    },
    _start_general_timer: function(time) {
	this.general_timer._timer = KanColleTimerUtils.timer.startDelayedEventAt(this, time, { type: 'general_timer' });
    },
    _stop_general_timer: function() {
	KanColleTimerUtils.timer.stop(this.general_timer._timer);
	this.general_timer._timer = null;
    },

    _update_repair_timer: function() {
	if (isNaN(this.repair_timer.base)) {
	    $('port-timer').value = '-';
	} else {
	    let now = (new Date).getTime();
	    let diff = (now - this.repair_timer.base) / 1000;
	    let sec, min, hour;
	    let t;
	    sec = Math.floor(diff) % 60;
	    min = Math.floor(diff / 60) % 60;
	    hour = Math.floor(diff / 3600);
	    t = (hour < 10 ? '0' + hour : hour) + ':' +
		(min < 10 ? '0' + min : min) + ':' +
		(sec < 10 ? '0' + sec : sec);
	    $('port-timer').value = t;
	}
    },

    _start_repair_timer: function() {
	this.repair_timer._timer = KanColleTimerUtils.timer.startRepeatingEvent(this, 1000, { type: 'repair_timer' });
    },
    _stop_repair_timer: function() {
	KanColleTimerUtils.timer.stop(this.repair_timer._timer);
	this.repair_timer._timer = null;
    },

    observe: function(subject, topic, data) {
	switch (topic) {
	case 'timer-callback':
	    if (!data)
		break;
	    switch (data.type) {
	    case 'repair_timer':
		this._update_repair_timer();
		break;
	    case 'general_timer':
		this._update_general_timer(data.extradata);
		break;
	    default:;
	    }
	    break;
	default:;
	}
    },

    init: function() {
	this._update_init();
	this._start_repair_timer();
    },
    exit: function() {
	this._stop_general_timer()
	this._stop_repair_timer();
	this._update_exit();
    },
};
KanColleTimerHeadQuarterInfo.__proto__ = __KanColleTimerPanel;

/*
 * デッキ/遠征
 *  member/deck		: api_data
 *  member/deck_port	: api_data
 *  member/ship2	: api_data_deck
 */
var KanColleTimerDeckInfo = {
    update: {
	mission: function() {
	    let decks = KanColleDatabase.deck.list();
	    for (let i = 0; i < decks.length; i++){
		let d = KanColleDatabase.deck.get(decks[i]);
		let k = d.api_id;
		if (d.api_mission[0]) {
		    let mission_id = d.api_mission[1]; // 遠征ID
		    let mission = KanColleDatabase.mission.get(mission_id);
		    if (mission) {
			KanColleRemainInfo.mission[k] = { name: mission.api_name, id: mission_id };
			$('deck_mission'+k).value = mission.api_name;
			SetStyleProperty($('deck_mission'+k), 'font-style', '');
			SetStyleProperty($('deck_label'+k), 'color', '');
		    }
		}
	    }
	},

	deck: function(extradata) {
	    let decks = KanColleDatabase.deck.list();
	    let now = Math.floor(KanColleDatabase.deck.timestamp() / 1000);

	    if (extradata && extradata.state !== undefined) {
		debugprint(extradata.toSource());
		if (extradata.state > 1)
		    AddLog(extradata.message + '\n');
	    }

	    for( let i = 0; i < decks.length; i++ ){
		let d = KanColleDatabase.deck.get(decks[i]);
		let k = d.api_id;
		let labelid = 'deck_label'+k;
		let targetid = 'fleet'+k;
		let timeid = 'fleetremain'+k;
		$(labelid).setAttribute('tooltiptext', d.api_name); // 艦隊名
		if( d.api_mission[0] ){
		    let mission_id = d.api_mission[1]; // 遠征ID
		    let mission = KanColleDatabase.mission.get(mission_id);
		    // 遠征名を表示
		    let mission_name = mission ? mission.api_name : (KanColleData.mission_param[mission_id] || {}).name;
		    if (!mission_name)
			mission_name = 'UNKNOWN_' + mission_id;
		    KanColleRemainInfo.mission[k] = { name: mission_name, id: mission_id };
		    $('deck_mission'+k).value = mission_name;
		    SetStyleProperty($('deck_mission'+k), 'font-style', '');

		    $(targetid).finishTime = d.api_mission[2];
		    $(timeid).finishTime = d.api_mission[2];
		    SetStyleProperty($('deck_label'+k), 'color', '');
		}else{
		    $(targetid).finishTime = '';
		    $(timeid).finishTime = '';
		}
	    }
	},
	memberBasic: function() {
	    let d = KanColleDatabase.memberBasic.get();
	    let fleets;
	    if (!d)
		return;
	    fleets = document.getElementsByClassName("fleet");
	    for( let i=0; i<4; i++ )
		SetStyleProperty(fleets[i], 'display',
				 (i != 0 && i < d.api_count_deck) ? "" : "none");
	},
    },

    show: function() {
	let checked = $('deckinfo-toggle').getAttribute('checked') == 'true';
	let nodes = document.getElementsByClassName("fleet");
	for (let i = 0; nodes[i]; i++)
	    nodes[i].collapsed = !checked;

	checked = $('porttimer-toggle').getAttribute('checked') == 'true' ||
		  $('gentimer-toggle').getAttribute('checked') == 'true' ||
		  $('deckinfo-toggle').getAttribute('checked') == 'true' ||
		  $('ndockinfo-toggle').getAttribute('checked') == 'true' ||
		  $('kdockinfo-toggle').getAttribute('checked') == 'true';
	$('timer').collapsed = !checked;
    },

    restore: function() {
	this.update.memberBasic.call(this);
	try{
	    for( let i = 0; i < 4; i++ ){
		let k = i + 1;
		if( KanColleRemainInfo.mission[k].name ){
		    let mission_name = KanColleRemainInfo.mission[k].name;
		    $('deck_mission'+k).value=mission_name;
		}
	    }
	} catch(x) {
	}
	this.show();
    },
};
KanColleTimerDeckInfo.__proto__ = __KanColleTimerPanel;

/*
 * 入渠ドック
 *  member/ndock	: api_data
 */
var KanColleTimerNdockInfo = {
    update: {
	ndock: function(extradata) {
	    let docks = KanColleDatabase.ndock.list();
	    let now = Math.floor(KanColleDatabase.ndock.timestamp()/1000);

	    if (extradata && extradata.state !== undefined) {
		debugprint(extradata.toSource());
		if (extradata.state > 1)
		    AddLog(extradata.message + '\n');
	    }

	    // 入渠ドック
	    for( let i = 0; i < docks.length; i++ ){
		let d = KanColleDatabase.ndock.get(docks[i]);
		let k = d.api_id;
		var targetid = 'ndock'+k;
		var timeid = 'ndockremain'+k;
		if( d.api_state > 0 ){
		    let ship_id = d.api_ship_id;
		    let name = FindShipName( ship_id );
		    let complete_time = d.api_complete_time;
		    if (!complete_time)
			complete_time = cur;
		    $("ndock-name"+k).value = name;

		    $(targetid).finishTime = complete_time;
		    $(timeid).finishTime = complete_time;
		}else if(d.api_state == 0){
		    $("ndock-name"+(i+1)).value = "";
		    $(targetid).finishTime = '';
		    $(timeid).finishTime = '';
		}else{
		    $('ndock-box'+(i+1)).style.display = 'none';
		}
	    }
	},
	memberBasic: function() {
	    let d = KanColleDatabase.memberBasic.get();
	    let ndocks;
	    if (!d)
		return;
	    ndocks  = document.getElementsByClassName("ndock-box");
	    for( let i = 0; i < 4; i++ )
		SetStyleProperty(ndocks[i], 'display', i < d.api_count_ndock ? "":"none");
	},
	material: function() {
	    let d = KanColleDatabase.material.get('bucket');
	    if (d >= 0)
		$('repairkit-number').value = d;
	},
    },

    // 入渠ドックのメモ作成
    createRepairMemo: function(){
	let elem = $('popup-ndock-memo').triggerNode;
	let hbox = FindParentElement(elem,"row");
	let oldstr = hbox.getAttribute('tooltiptext') || "";
	let text = "入渠ドック"+hbox.firstChild.value+"のメモを入力してください。\nツールチップとして表示されるようになります。";
	let str = InputPrompt(text,"入渠ドックメモ", oldstr);
	if( str==null ) return;
	hbox.setAttribute('tooltiptext',str);

	let ndock_hbox = evaluateXPath(document,"//*[@class='ndock-box']");
	for(let k in ndock_hbox){
	    k = parseInt(k, 10);
	    let elem = ndock_hbox[k];
	    KanColleRemainInfo.ndock_memo[k] = ndock_hbox[k].getAttribute('tooltiptext');
	}
    },

    show: function() {
	let checked = $('ndockinfo-toggle').getAttribute('checked') == 'true';
	let nodes = document.getElementsByClassName("ndock-box");
	for (let i = 0; nodes[i]; i++)
	    nodes[i].collapsed = !checked;

	checked = $('porttimer-toggle').getAttribute('checked') == 'true' ||
		  $('gentimer-toggle').getAttribute('checked') == 'true' ||
		  $('deckinfo-toggle').getAttribute('checked') == 'true' ||
		  $('ndockinfo-toggle').getAttribute('checked') == 'true' ||
		  $('kdockinfo-toggle').getAttribute('checked') == 'true';
	$('timer').collapsed = !checked;
    },

    restore: function() {
	this.update.memberBasic.call(this);
	try{
	    for( let i=0; i < 4; i++ ){
		let k = i + 1;
		if( KanColleRemainInfo.ndock_memo[i] ){
		    $('ndock-box'+k).setAttribute('tooltiptext',
						  KanColleRemainInfo.ndock_memo[i] );
		}
	    }
	} catch(x) {
	}
	this.show();
    },
};
KanColleTimerNdockInfo.__proto__ = __KanColleTimerPanel;

/*
 * 建造
 *  member/kdock	: api_data
 */
var KanColleTimerKdockInfo = {
    update: {
	kdock: function(extradata) {
	    let docks = KanColleDatabase.kdock.list();
	    let cur = KanColleDatabase.kdock.timestamp();
	    let now = Math.floor(cur);

	    if (extradata && extradata.state !== undefined) {
		debugprint(extradata.toSource());
		if (extradata.state > 1)
		    AddLog(extradata.message + '\n');
	    }

	    // 建造ドック
	    for( let i = 0; i < docks.length; i++ ) {
		let d = KanColleDatabase.kdock.get(docks[i]);
		let k = d.api_id;
		let targetid = 'kdock'+k;
		let timeid = 'kdockremain'+k;

		if( d.api_state > 0 ){
		    // 建造艦の推定
		    // 建造艦艇の表示…はあらかじめ分かってしまうと面白みがないのでやらない
		    /*
		    let ship_id = parseInt( d.api_created_ship_id, 10 );
		    let ship_name = FindShipNameByCatId(d.api_created_ship_id);
		     */
		    let timestamp = KanColleDatabase.kdock.timestamp(k);
		    let complete_time = timestamp.complete;
		    let created_time = timestamp.start;
		    debugprint('complete_time='+complete_time+', created_time='+created_time);
		    let ship_name = this._guess_ship(Math.floor(created_time/1000),
						     Math.floor(complete_time/1000));
		    $('kdock-name'+k).setAttribute('tooltiptext', ship_name);
		    SetStyleProperty($('kdock-name'+k), 'color', 'rgba(0,0,0,0)');
		    $('kdock-name'+k).value = FindShipNameByCatId(d.api_created_ship_id);

		    if (d.api_state == 3)
			complete_time = cur;

		    $(targetid).finishTime = complete_time;
		    $(timeid).finishTime = complete_time;
		}else if (d.api_state == 0) {
		    // 建造していない
		    SetStyleProperty($('kdock-name'+k), 'color', 'rgba(0,0,0,0)');
		    $('kdock-name'+k).value = '-';
		    $('kdock-name'+k).setAttribute('tooltiptext', '');
		    $(targetid).finishTime = '';
		    $(timeid).finishTime = '';
		}else{
		    $('kdock-box'+k).style.display = 'none';
		}
	    }
	},

	memberBasic: function() {
	    let d = KanColleDatabase.memberBasic.get();
	    let ndocks;
	    if (!d)
		return;
	    ndocks = document.getElementsByClassName("kdock-box");
	    for( let i = 0; i < 4; i++ )
		SetStyleProperty(ndocks[i], 'display', i < d.api_count_kdock ? "":"none");
	},

	material: function() {
	    let d = KanColleDatabase.material.get('burner');
	    if (d >= 0)
		$('burner-number').value = d;
	},

    },

    show: function() {
	let checked = $('kdockinfo-toggle').getAttribute('checked') == 'true';
	let nodes = document.getElementsByClassName("kdock-box");
	for (let i = 0; nodes[i]; i++)
	    nodes[i].collapsed = !checked;

	checked = $('porttimer-toggle').getAttribute('checked') == 'true' ||
		  $('gentimer-toggle').getAttribute('checked') == 'true' ||
		  $('deckinfo-toggle').getAttribute('checked') == 'true' ||
		  $('ndockinfo-toggle').getAttribute('checked') == 'true' ||
		  $('kdockinfo-toggle').getAttribute('checked') == 'true';
	$('timer').collapsed = !checked;
    },

    restore: function() {
	this.update.memberBasic.call(this);
	this.show();
    },

    /*
     * 建造艦名表示（隠し機能）
     *
     * 建造ドックのNo.欄を素早く何度かダブルクリックすると
     * 艦名を tooltip として表示
     */
    timer: {},

    handleEvent: function(e) {
	let id = e.target.id;
	let now = (new Date).getTime();

	if (!this.timer[id] || this.timer[id] + 1000 < now) {
	    this.timer[id] = now;
	    return;
	}
	this.timer[id] += 1000;

	if (this.timer[id] - now <= 2000)
	    return;

	if (id.match(/^kdock-name(\d+)$/)) {
	    let fleet_id = parseInt(RegExp.$1, 10);
	    let fleet = KanColleDatabase.kdock.get(fleet_id);
	    if( fleet && fleet.api_complete_time ){
		let ship_id = parseInt( fleet.api_created_ship_id, 10 );
		let ship_name = FindShipNameByCatId(ship_id);
		e.target.setAttribute('tooltiptext',ship_name);
	    }
	}
    },

    _guess_ship: function(now, finishedtime) {
	let remain = finishedtime - now;
	for( let k in KanColleData.construction_shipname ){
	    let shipname = KanColleData.construction_shipname[k];
	    k = parseInt( k, 10 );
	    k *= 60;

	    let t1 = k - 30;
	    let t2 = k + 30;

	    if( t1 < remain && remain < t2 ){
		return shipname;
	    }
	}
	return "建造艦種不明";
    },

    init: function() {
	this._update_init();
	for( let i = 0; i < 4; i++ ){
	    let k = 'kdock-name' + (i + 1);
	    $(k).addEventListener('dblclick', this);
	}
    },

    exit: function(){
	for( let i = 0; i < 4; i++ ){
	    let k = 'kdock-name' + (i + 1);
	    $(k).removeEventListener('dblclick', this);
	}
	this._update_exit();
    },
};
KanColleTimerKdockInfo.__proto__ = __KanColleTimerPanel;

/*
 * 所有艦娘情報2
 *  member/ship2	: api_data
 */
function KanColleTimerShipInfoHandler(){
    KanColleCreateShipTree();
    KanColleShipInfoSetView();
}

var KanColleTimerFleetInfo = {
    //_get_shipname: function(deckid,pos) {
    //	let ships = KanColleDatabase.deck.get(deckid).api_ship;
    //	if (pos >= 1 && pos <= 6)
    //	    return FindShipName(ships[pos - 1]);
    //	else
    //	    return '?' + pos;
    //},
    _mission_plan: {},
    _escape: {},
    _slotitem: {},

    _ship_hp_color: function(hpratio) {
	let ship_color;
	let ship_damage_color;
	if (hpratio >= 1) {
	    ship_color = null;
	    ship_damage_color = '#ddffdd';
	} else if (hpratio > 0.75) {
	    ship_color = '#0000cc'; //かすり傷
	    ship_damage_color = '#00ff00';
	} else if (hpratio > 0.50) {
	    ship_color = '#ff44cc'; //小破
	    ship_damage_color = '#ffff00';
	} else if (hpratio > 0.25) {
	    ship_color = '#ff4400'; //中破
	    ship_damage_color = '#ffa500';
	} else if (hpratio > 0) {
	    ship_color = '#ff0000'; //大破
	    ship_damage_color = '#ff0000';
	} else {
	    ship_color = '#000000'; //撃沈
	    ship_damage_color = '#6495ed';
	}
	return [ship_color, ship_damage_color];
    },

    _show_battleresult_menu: function(enable) {
	$('shipstatus-context-battleresult').setAttribute('hidden', !enable);
	$('shipstatus-context-battleresult').addEventListener('command', function() { KanColleTimerFleetInfo._discloseBattleResult(true); }, true);
    },

    _clear_enemy_ships: function() {
	let [dummy,defaultcolor] = this._ship_hp_color(1);
	$('shipstatus-e1-0').value = '-';
	for (let i = 1; i <= 6; i++) {
	    let node = $('shipstatus-e1' + '-' + i);
	    node.value = '-';
	    SetStyleProperty(node, 'text-shadow', null);
	    SetStyleProperty(node, 'border-left-color', defaultcolor);
	    SetStyleProperty(node, 'text-decoration', null);
	    SetStyleProperty(node, 'background', null);
	    SetStyleProperty(node, '-moz-text-decoration-style', null);
	    SetStyleProperty(node, 'text-decoration-style', null);
	    SetStyleProperty(node, '-moz-text-decoration-color', null);
	    SetStyleProperty(node, 'text-decoration-color', null);
	}
    },

    _show_enemy_ships: function(show) {
	$('shipstatus-fleet-e1').setAttribute('hidden', !show);
    },

    _set_escape_ships: function() {
	let that = this;
	let decks = KanColleDatabase.deck.list();
	decks.forEach(function(deckid) {
	    let res = KanColleDatabase.battle.get(deckid) || {};
	    let escape = res.escape || [];
	    escape.forEach(function(e,i){
		let node = $('shipstatus-' + deckid + '-' + i);
		if (!i)
		    return;
		SetStyleProperty(node, 'border-right-color', e ? 'rgb(64,64,64)' : null);
		if (!that._escape[deckid])
		    that._escape[deckid] = [];
		that._escape[deckid][i] = e ? true : false;
	    });
	});
    },

    _set_enemy_ships: function() {
	let enemy_style = null;
	let enemy_weight = null;
	let enemy_decoration = null;
	let d = KanColleDatabase.battle.get('e1');

	if (!d) {
	    debugprint('no enemy information');
	    return;
	}

	if (d.practice)
	    enemy_style = 'italic';
	if (d.boss)
	    enemy_weight = 'bold';
	if (d.last)
	    enemy_decoration = 'underline';

	$('shipstatus-e1-0').value = d.name;
	if (d.fullname)
	    $('shipstatus-e1-0').setAttribute('tooltiptext', d.fullname);
	else
	    $('shipstatus-e1-0').removeAttribute('tooltiptext');

	if (d.ships) {
	    d.ships.forEach(function(e,idx) {
		if (idx) {
		    let node = $('shipstatus-e1-' + idx);
		    let eship = KanColleDatabase.masterShip.get(e);
		    let name = '-';
		    let ecolor = null;
		    if (eship) {
			name = eship.api_name;
			switch (eship.api_yomi) {
			case 'flagship':
			    ecolor = 'red';
			    break;
			case 'elite':
			    ecolor = 'orange';
			    break;
			}
		    }
		    node.value = name;
		    SetStyleProperty(node, 'background', name ? '#cccccc' : null);
		    if (e < 0)
			SetStyleProperty(node, 'border-left-color', null);
		    if (ecolor)
			SetStyleProperty(node, 'text-shadow', '0 0 2px ' + ecolor);
		}
	    });
	}

	SetStyleProperty($('shipstatus-e1-0'), 'font-style', enemy_style);
	SetStyleProperty($('shipstatus-e1-0'), 'font-weight', enemy_weight);
	SetStyleProperty($('shipstatus-e1-0'), 'text-decoration', enemy_decoration);
    },

    _discloseBattleResult: function(nowarn) {
	let that = this;
	KanColleDatabase.battle.list().forEach(function(deckid) {
	    let battle = KanColleDatabase.battle.get(deckid);
	    let warn = 0;
	    if (!battle.result)
		return;
	    Object.keys(battle.result).forEach(function(i) {
		let node = $('shipstatus-' + deckid + '-' + i);
		let res = battle.result[i];
		let ratio = res.cur / res.maxhp;
		let [hpcolor, damagecolor] = that._ship_hp_color(ratio);
		SetStyleProperty(node, 'border-left-color', damagecolor);
		// 大破警告
		//  敵艦,旗艦は除外
		//  出撃中なので入渠中ではない
		if (!nowarn && !isNaN(parseInt(deckid, 10)) && i > 1 &&
		    ratio <= 0.25) {
		    let shipslot = (that._slotitem[deckid] && that._slotitem[deckid][i]) || [];
		    let escape = (that._escape[deckid] && that._escape[deckid][i]) || false;
		    if (!shipslot[23] && !escape) {
			warn++;
		    }
		}
		// ダメージ時のみ更新
		if (res.damage) {
		    if (hpcolor) {
			SetStyleProperty(node, 'text-decoration', 'line-through');
			SetStyleProperty(node, '-moz-text-decoration-style', 'double');
			SetStyleProperty(node, 'text-decoration-style', 'double');
			SetStyleProperty(node, '-moz-text-decoration-color', hpcolor);
			SetStyleProperty(node, 'text-decoration-color', hpcolor);
		    } else {
			SetStyleProperty(node, 'text-decoration', null);
			SetStyleProperty(node, '-moz-text-decoration-style', null);
			SetStyleProperty(node, 'text-decoration-style', null);
			SetStyleProperty(node, '-moz-text-decoration-color', null);
			SetStyleProperty(node, 'text-decoration-color', null);
		    }
		}
	    });
	    if (!isNaN(parseInt(deckid, 10))) {
		SetStyleProperty($('shipstatus-fleet-' + deckid), 'background-color', warn ? 'red' : null);
	    }
	});
    },

    update: {
	deck: function() {
	    let l = KanColleDatabase.deck.list();

	    function timestr(t){
		let d = new Date;
		let h;
		let m;

		d.setTime(t);

		h = d.getHours();
		if (h < 10)
		    h = '0' + h;

		m = d.getMinutes();
		if (m < 10)
		    m = '0' + m;

		return h + ':' + m;
	    }

	    for ( let i = 0; i < 4; i++ ){
		$("shipstatus-fleet-"+(i+1)).style.display = typeof l[i] == "undefined" ? "none" : "";
	    }

	    // 艦隊/遠征情報
	    for ( let i = 0; i < l.length; i++ ){
		let fi = KanColleDatabase.deck.get(l[i]);
		let id = parseInt(fi.api_id, 10);
		let fleet_text = fi.api_name;
		let fleet_flagship_lv = 0;
		let fleet_lv = 0;
		let fleet_stypes = {};
		let fleet_slotitem_ship = {};
		let fleet_slotitem_num =  {};
		let fleet_ap = 0;
		let fleet_search = 0;
		let fleet_search_srv = 0;
		let fleet_search_rdr = 0;
		let fleet_warn = 0;
		let min_cond = 100;
		let mission_ok;

		if (fi.api_mission[1])
		    this._mission_plan[id] = fi.api_mission[1];
		mission_ok = this.checkMission(id);

		for ( let j = 0; j < fi.api_ship.length; j++ ){
		    let ship_id = fi.api_ship[j];
		    let ship_name = FindShipName(ship_id);
		    let ship_info = FindShipStatus(ship_id);
		    let ship_cond = FindShipCond(ship_id);
		    let ship = KanColleDatabase.ship.get(ship_id);
		    let ship_escape = (this._escape[id] && this._escape[id][j+1]) || false;
		    let ship_bgcolor;
		    let ship_res_color;
		    let ship_hpratio;
		    let ship_hp_color;
		    let ship_damage_color;
		    let ship_damage_style;
		    let ship_shadow;
		    let ship_text = ship_name + (ship ? ' Lv' + ship.api_lv : '');
		    let shipslot = {};

		    if (ship) {
			let shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
			fleet_lv += ship.api_lv;
			if (j == 0)
			    fleet_flagship_lv = ship.api_lv;
			if (!fleet_stypes[shiptype.api_stype])
			    fleet_stypes[shiptype.api_stype] = 1;
			else
			    fleet_stypes[shiptype.api_stype]++;
		    }

		    if (ship_cond === undefined) {
			ship_cond = '-';
			ship_bgcolor = null;
		    } else if (ship_cond >= 90) {
			ship_bgcolor = '#ffffff';   //キラキラ
		    } else if (ship_cond >= 80) {
			ship_bgcolor = '#eeffff';   //キラキラ
		    } else if (ship_cond >= 70) {
			ship_bgcolor = '#ddffee';   //キラキラ
		    } else if (ship_cond >= 60) {
			ship_bgcolor = '#ccffdd';   //キラキラ
		    } else if (ship_cond >= 50) {
			ship_bgcolor = '#bbffcc';   //キラキラ
		    } else if (ship_cond >= 40) {
			ship_bgcolor = '#88ff88';   //平常
		    } else if (ship_cond >= 30) {
			ship_bgcolor = '#ffdd88';   //間宮
		    } else if (ship_cond >= 20) {
			ship_bgcolor = '#ffaa44';   //オレンジ
		    } else if (ship_cond >= 0) {
			ship_bgcolor = '#ff8888';   //赤
		    } else {
			ship_bgcolor = '#666666';   //...
		    }

		    if (ship_cond < 49) {
			let t = KanColleDatabase.ship.timestamp();
			ship_text += ' ' + timestr(t + Math.ceil((49 - ship_cond) / 3) * 180000);

			if (ship_cond < min_cond)
			    min_cond = ship_cond;
		    }

		    if (ship_info === undefined) {
			ship_res_color = 'rgba(0,0,0,0)';
			ship_hp_color = null;
			ship_damage_style = 'solid';
			ship_hpratio = 1;   //XXX
		    } else {
			let in_ndock = KanColleDatabase.ndock.find(ship_id);
			ship_hpratio = ship_info.nowhp / ship_info.maxhp;

			ship_text += '\nHP  : ' + ship_info.nowhp + '/' + ship_info.maxhp + (in_ndock ? ' (入渠中)' : '')
				  +  '\n士気: ' + ship_cond
				  +  '\n燃料: ' + ship_info.bull  + '/' + ship_info.bull_max
				  +  '\n弾薬: ' + ship_info.fuel  + '/' + ship_info.fuel_max
			;

			if (ship_info.bull_max > ship_info.bull ||
			    ship_info.fuel_max > ship_info.fuel) {
			    ship_res_color = 'red';
			} else {
			    ship_res_color = 'rgba(0,0,0,0)';
			}

			if (ship_hpratio >= 1) {
			    ship_shadow = '1px 1px 0 rgba(0,0,0,0)';
			} else {
			    ship_shadow = '1px 1px 0 black';
			}
			[ship_hp_color, ship_damage_color] = this._ship_hp_color(ship_hpratio);
			ship_damage_style = in_ndock ? 'dotted' : 'solid';
		    }

		    if (ship) {
			let ship_search = ShipCalcSearch(ship_id);
			if (ship_search >= 0) {
			    if (fleet_search >= 0)
				fleet_search += ship_search;
			    fleet_search_srv += ShipCalcSearchSurveilanceAircraft(ship_id);
			    fleet_search_rdr += ShipCalcSearchRadar(ship_id);
			} else
			    fleet_search = -1;
		    }

		    if (KanColleTimerConfig.getBool('display.extra-info') && ship) {
			let ship_ap = ShipCalcAirPower(ship_id);
			if (ship_ap >= 0) {
			    //ship_text += '\n制空: ' + ship_ap;
			    if (fleet_ap >= 0)
				fleet_ap += ship_ap;
			} else
			    fleet_ap = -1;
		    }

		    // 装備
		    if (ship) {
			let ship_slot = JSON.parse(JSON.stringify(ship.api_slot));
			let ship_slot_string = [];
			if (ship.api_slot_ex)
			    ship_slot.push(ship.api_slot_ex);
			for (let k = 0; k < ship_slot.length; k++) {
			    let itemid = ship_slot[k];
			    let item;
			    let itemtype;
			    let s = '';

			    if (itemid < 0)
				continue;
			    item = KanColleDatabase.slotitem.get(itemid);
			    itemtype = item ? KanColleDatabase.masterSlotitem.get(item.api_slotitem_id) : null;

			    if (itemtype) {
				s = itemtype.api_name;
				if (item.api_level || item.api_alv) {
				    s += '[';
				    if (item.api_level)
					s += '★' + item.api_level;
				    if (item.api_alv)
					s += '+' + item.api_alv;
				    s += ']';
				}
			    } else {
				s = '-';
			    }
			    ship_slot_string.push(s);

			    if (!itemtype)
				continue;

			    shipslot[itemtype.api_type[2]] = shipslot[itemtype.api_type[2]] ?  shipslot[itemtype.api_type[2]] + 1 : 1;
			}
			if (!this._slotitem[id])
			    this._slotitem[id] = {};
			this._slotitem[id][j+1] = shipslot;
			for (let k in shipslot) {
			    // 装備数
			    fleet_slotitem_num[k] = fleet_slotitem_num[k] ? fleet_slotitem_num[k] + shipslot[k] : shipslot[k];
			    // 所持艦船数
			    fleet_slotitem_ship[k] = fleet_slotitem_ship[k] ? fleet_slotitem_ship[k] + 1 : 1;
			}
			ship_text += '\n装備:' + ship_slot_string.join(', ');
		    }

		    // 大破警告
		    //	旗艦でない、大破、ダメコンなし、待避/入渠していない
		    if (j && ship_hpratio <= 0.25 &&
			shipslot && !shipslot[23] && !ship_escape &&
			!KanColleDatabase.ndock.find(ship_id)) {
			fleet_warn++;
		    }

		    $('shipstatus-' + id + '-' + (j + 1)).value = ship_cond;
		    $('shipstatus-' + id + '-' + (j + 1)).setAttribute('tooltiptext', ship_text);
		    $('shipstatus-' + id + '-' + (j + 1)).setAttribute('data-name', ship_name);
		    $('shipstatus-' + id + '-' + (j + 1)).setAttribute('data-lv', '' + (ship ? ship.api_lv : ''));
		    SetStyleProperty($('shipstatus-' + id + '-' + (j + 1)), 'background-color', ship_bgcolor);
		    SetStyleProperty($('shipstatus-' + id + '-' + (j + 1)), 'outline-color', ship_res_color);
		    SetStyleProperty($('shipstatus-' + id + '-' + (j + 1)), 'color', ship_hp_color);
		    SetStyleProperty($('shipstatus-' + id + '-' + (j + 1)), 'border-left-color', ship_damage_color);
		    SetStyleProperty($('shipstatus-' + id + '-' + (j + 1)), 'border-left-style', ship_damage_style);
		    SetStyleProperty($('shipstatus-' + id + '-' + (j + 1)), 'text-shadow', ship_shadow);
		    SetStyleProperty($('shipstatus-' + id + '-' + (j + 1)), 'text-decoration', null);
		    SetStyleProperty($('shipstatus-' + id + '-' + (j + 1)), '-moz-text-decoration-style', null);
		    SetStyleProperty($('shipstatus-' + id + '-' + (j + 1)), 'text-decoration-style', null);
		    SetStyleProperty($('shipstatus-' + id + '-' + (j + 1)), '-moz-text-decoration-color', null);
		    SetStyleProperty($('shipstatus-' + id + '-' + (j + 1)), 'text-decoration-color', null);
		}

		if (fleet_flagship_lv > 0) {
		    let stypes;
		    let fleetinfo = [];
		    let timercmd = null;
		    let slotitem2show = [ 12, 13, 23, 24, 30 ];
		    let slotiteminfo = [];

		    let cur = (new Date).getTime();
		    let t = KanColleDatabase.ship.timestamp();
		    let time = t + Math.ceil((49 - min_cond) / 3) * 180000;
		    let str = timestr(time);

		    stypes = Object.keys(fleet_stypes).sort(function(a,b){
			return fleet_stypes[b] - fleet_stypes[a];
		    });

		    if (this._mission_plan[id]) {
			let mission = KanColleDatabase.masterMission.get(this._mission_plan[id]);
			fleet_text += '\n遠征: ' + (mission ? mission.api_name : this._mission_plan[id]) + ' ' +
				      (mission_ok !== undefined ? (mission_ok ? 'OK' : 'NG') : '?');
		    }

		    fleet_text += '\n編成: 旗艦Lv' + fleet_flagship_lv + '/' + fleet_lv;
		    for( let j = 0; j < stypes.length; j++ ){
			let stype = KanColleDatabase.masterStype.get(stypes[j]);
			let stypename = stype ? stype.api_name : null;
			if (!stypename)
			    stypename = 'UNKNOWN_' + stypes[j];
			fleetinfo.push(' ' + stypename + '(' + fleet_stypes[stypes[j]] + ')');
		    }
		    fleet_text += ';' + fleetinfo.join(',');

		    for ( let j = 0; j < slotitem2show.length; j++ ) {
			let k = slotitem2show[j];
			let eqtype = KanColleDatabase.masterSlotitemEquiptype.get(k);

			if (!fleet_slotitem_ship[k])
			    fleet_slotitem_ship[k] = 0;
			if (!fleet_slotitem_num[k])
			    fleet_slotitem_num[k] = 0;

			slotiteminfo.push(eqtype.api_name +
					  '(' + fleet_slotitem_num[k] +
					  '/' + fleet_slotitem_ship[k] +
					  ')');
		    }

		    fleet_text += '\n装備: ' + slotiteminfo.join(', ');

		    if (fleet_search >= 0) {
			// 2-5式
			let fleet_search_2 = fleet_search_srv * 2 +
					     fleet_search_rdr +
					     Math.floor(Math.sqrt(fleet_search -
								  fleet_search_srv -
								  fleet_search_rdr));
			fleet_text += '\n索敵: ' + fleet_search_2 + '/' + fleet_search;

			// 2-5(秋)
			fleet_search_2 = FleetCalcSearchV2(fi.api_ship);
			fleet_text += '\n索敵(2-5 2014秋): ' + fleet_search_2.f.toFixed(4) +
				      '±' + fleet_search_2.e.toFixed(4);
			// 新秋簡易式モデルA
			fleet_search_2 = FleetCalcSearchV3(fi.api_ship);
			fleet_text += '\n索敵(新秋簡易式モデルA): ' + fleet_search_2;
		    }
		    if (KanColleTimerConfig.getBool('display.extra-info') && fleet_ap >= 0)
			fleet_text += '\n制空: ' + fleet_ap;
		}

		$('shipstatus-'+ id +'-0').setAttribute('tooltiptext', fleet_text);

		if (fleet_warn)
		    debugprint('#' + id + ': ' + fleet_warn);
		SetStyleProperty($('shipstatus-fleet-' + id), 'background-color', fleet_warn ? 'red' : null);
	    }
	},
	ship: 'deck',
	slotitem: 'deck',

	ndock: function() {
	    let l = KanColleDatabase.ndock.list();
	    for (let i = 0; i < l.length; i++) {
		let d = KanColleDatabase.ndock.get(l[i]);
		if (d.api_state > 0) {
		    let fleet = KanColleDatabase.deck.lookup(d.api_ship_id);
		    if (fleet) {
			SetStyleProperty($('shipstatus-' + fleet.fleet + '-' + (fleet.pos + 1)), 'border-left-style', 'dotted');

			// 大破警告
			//  shipで既に警告済のものを入渠によって除外
			if (fleet.pos) {
			    let warn = 0;
			    try {
				let deck = KanColleDatabase.deck.get(fleet.fleet);
				// 旗艦は除く
				for (let j = 1; j < deck.api_ship.length; j++) {
				    let status, ratio;

				    if (j == fleet.pos ||
					KanColleDatabase.ndock.find(deck.api_ship[j]))
					continue;

				    status = FindShipStatus(deck.api_ship[j]);
				    ratio = status.nowhp / status.maxhp;

				    if (ratio <= 0.25) {
					let shipslot = (this._slotitem[fleet.fleet] && this._slotitem[fleet.fleet][j+1]) || {};
					// 待避中ということはない
					if (!shipslot[23])
					    warn++;
				    }
				}
			    } catch(e) {
			    }
			    if (warn)
				debugprint('#' + fleet.fleet + ': ' + warn);
			    SetStyleProperty($('shipstatus-fleet-' + fleet.fleet), 'background-color', warn ? 'red' : null);
			}
		    }
		}
	    }
	},

	battle: function(data) {
	    let list = KanColleDatabase.battle.list();
	    list.forEach(function(e) {
		let d = KanColleDatabase.battle.get(e);
		debugprint('battle(' + data + '), ' + e + ': ' + d.toSource());
	    });
	    switch(data) {
	    case -3:	// reset
		this._clear_enemy_ships();
		this._set_escape_ships();
		this._show_enemy_ships(false);
		break;
	    case -2:	// enemy information (partial)
		break;
	    case -1:	// enemy information
		this._set_enemy_ships();
		this._show_enemy_ships(true);
		break;
	    case 0:	// before battle
		this._show_battleresult_menu(false);
		this._set_escape_ships();
		this._discloseBattleResult(true);
		break;
	    case 1:	// after battle
		this._show_battleresult_menu(true);
		break;
	    case 2:	// battle finished
		break;
	    case 3:	// final
		this._show_battleresult_menu(false);
		this._discloseBattleResult(KanColleDatabase.battle.get('e1').practice);
		break;
	    default:
	    }
	},

	masterStype: function() {
	    this._updateMissionMenu();
	},

	masterSlotitemEquiptype: function() {
	    this._updateMissionMenu();
	},
	masterMission: function() {
	    this._updateMissionMenu();
	},
	masterMaparea: function() {
	    this._updateMissionMenu();
	},
    },

    _updateMissionMenu: function() {
	let list;
	let areamenu = $('shipstatus-context-check-maparea');
	let matmenu = $('shipstatus-context-check-material');
	let materials = [ '燃料', '弾薬', '鋼材', 'ボーキサイト' ];
	let popup;

	if (!KanColleDatabase.masterMission.timestamp() ||
	    !KanColleDatabase.masterMaparea.timestamp() ||
	    !KanColleDatabase.masterStype.timestamp() ||
	    !KanColleDatabase.masterSlotitemEquiptype.timestamp()) {
	    $('shipstatus-context-check').setAttribute('disabled', true);
	    return;
	}

	function createMenuItem(missionid) {
	    let d = KanColleDatabase.masterMission.get(missionid);
	    let item = document.createElementNS(XUL_NS, 'menuitem');
	    let param = KanColleData.mission_param[missionid] || {};
	    let tooltip;
	    item.setAttribute('value', '' + d.api_id);
	    item.setAttribute('label', '' + d.api_name);
	    if (param.balance) {
		let desc;
		let t = '?';
		if (d.api_time) {
		    t = d.api_time % 60;
		    if (t < 10)
			t = '0' + t;
		    t = Math.floor(d.api_time / 60) + ':' + t + ' ';
		}
		desc = '[' + param.balance.join(',') + ']';
		item.setAttribute('description', t + desc);
	    }
	    tooltip = d.api_details;
	    if (param.lv) {
		tooltip += '\nLv:';
		if (param.lv.flagship)
		    tooltip += ' ' + param.lv.flagship;
		if (param.lv.total)
		    tooltip += '/' + param.lv.total;
	    }
	    if (param.stype || param.num) {
		tooltip += '\n編成:';
		if (param.num)
		    tooltip += ' ' + param.num;
		if (param.stype) {
		    tooltip += '\n' +
			       param.stype.map(function(element) {
						return '\t' +
						       element.mask.map(function(e){
									    let stype = KanColleDatabase.masterStype.get(e);
									    return stype ? stype.api_name : ('UNKNOWN_' + e);
									}).join('|') +
						       ': ' + element.num +
						       (element.flagship ? '(旗艦)' : '');
					       }).join('\n');
		}
	    }
	    if (param.slotitem) {
		tooltip += '\n装備:\n' +
			   Object.keys(param.slotitem).map(function(element) {
							    let cond = param.slotitem[element];
							    let etype = KanColleDatabase.masterSlotitemEquiptype.get(element);
							    return '\t' + etype.api_name + ': ' +
								   (cond.ship ? cond.ship : 0) +
								   (cond.num ? '/' + cond.num : '');
							   }).join('\n');
	    }
	    item.setAttribute('tooltiptext', tooltip);
	    item.addEventListener('command', function() { KanColleTimerFleetInfo._missionMenuCommand(this.value); }, true);
	    return item;
	}

	list = KanColleDatabase.masterMission.list();
	RemoveChildren(areamenu);
	popup = document.createElementNS(XUL_NS, 'menupopup');
	areamenu.appendChild(popup);
	for (let i = 0; i < list.length; i++) {
	    let d = KanColleDatabase.masterMission.get(list[i]);
	    let submenu = $('shipstatus-context-check-maparea-' + d.api_maparea_id);
	    let item = createMenuItem(list[i]);
	    if (!submenu) {
		let area = KanColleDatabase.masterMaparea.get(d.api_maparea_id);
		let e = document.createElementNS(XUL_NS, 'menu');
		e.setAttribute('label', area.api_name);
		submenu = document.createElementNS(XUL_NS, 'menupopup');
		submenu.setAttribute('id', 'shipstatus-context-check-maparea-' + d.api_maparea_id);
		e.appendChild(submenu);
		popup.appendChild(e);
	    }
	    submenu.appendChild(item);
	}

	// sorted by balance
	RemoveChildren(matmenu);
	popup = document.createElementNS(XUL_NS, 'menupopup');
	matmenu.appendChild(popup);
	for (let i = 0; i < materials.length; i++) {
	    let matmenu = $('group-mission-fleet-context-material');
	    let submenu;
	    let e = document.createElementNS(XUL_NS, 'menu');
	    e.setAttribute('label', materials[i]);
	    submenu = document.createElementNS(XUL_NS, 'menupopup');
	    submenu.setAttribute('id', 'group-mission-fleet-context-material-' + i);
	    e.appendChild(submenu);
	    popup.appendChild(e);

	    list = JSON.parse(JSON.stringify(list));
	    list.sort(function(a,b) {
			let ma = (KanColleData.mission_param[a] || {}).balance;
			let mb = (KanColleData.mission_param[b] || {}).balance;
			let ret = !ma - !mb;
			if (!ret && ma && mb)
			    ret = mb[i] - ma[i];
			return ret;
		      });
	    for (let j = 0; j < list.length; j++)
		submenu.appendChild(createMenuItem(list[j]));
	}

	$('shipstatus-context-check').setAttribute('disabled', false);
    },

    _checkMission: function(deckid) {
	let flagship_lv = -1;
	let flagship_stype = -1;
	let res;
	let param;
	let pass = true;
	let d = KanColleDatabase.deck.get(deckid);
	let mission_id = d.api_mission[1] || this._mission_plan[deckid];

	if (!d || !mission_id ||
	    !KanColleDatabase.ship.timestamp() || !KanColleDatabase.masterShip.timestamp() ||
	    !KanColleDatabase.slotitem.timestamp() || !KanColleDatabase.masterSlotitem.timestamp())
	    return;
	res = d.api_ship.map(function(val, idx, array) {
				let ship = val >= 0 ? KanColleDatabase.ship.get(val) : null;
				let res = {};
				if (ship) {
				    let shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
				    let ship_slot = JSON.parse(JSON.stringify(ship.api_slot));
				    if (ship.api_slot_ex)
					ship_slot.push(ship.api_slot_ex);
				    res.num = 1;
				    res.lv = ship.api_lv;
				    if (idx == 0) {
					flagship_lv = ship.api_lv;
					flagship_stype = shiptype.api_stype;
				    }
				    res.stype = shiptype.api_stype;
				    res.slotitem = ship_slot.map(function(v, i, a) {
									let item = KanColleDatabase.slotitem.get(v);
									let itemtype = item ? KanColleDatabase.masterSlotitem.get(item.api_slotitem_id) : null;
									return itemtype ? itemtype.api_type[2] : -1;
								     });
				} else {
				    res.num = 0;
				    res.lv = 0;
				    res.stype = -1;
				    res.slotitem = [];
				}
				return res;
			     }).reduce(function(prev, cur) {
					let items;
					prev.num += cur.num;
					prev.lv += cur.lv;
					if (cur.stype >= 0)
					    prev.stype[cur.stype] = (prev.stype[cur.stype] ? prev.stype[cur.stype] : 0) + 1;
					items = cur.slotitem.reduce(function(p,c) {
									if (c >= 0)
									    p[c] = (p[c] ? p[c] : 0) + 1;
									return p;
								    }, {});
					Object.keys(items).forEach(function(v,i,a){
					    if (!prev.slotitem[v])
						prev.slotitem[v] = { ship: 0, num: 0, };
					    prev.slotitem[v].ship++;
					    prev.slotitem[v].num += items[v];
					});
					return prev;
				       }, { num: 0, stype: {}, lv: 0, slotitem: {}, });
	param = KanColleData.mission_param[mission_id];
	if (!param)
	    return;

	if (param.lv) {
	    if (param.lv.flagship)
		pass &= param.lv.flagship <= flagship_lv;
	    if (param.lv.total)
		pass &= param.lv.total <= res.lv;
	}
	if (param.num)
	    pass &= param.num <= res.num;
	if (param.stype)
	    pass &= param.stype.every(function(val,idx,array){
					let num = val.mask.map(function(v,i,a) {
								return res.stype[v] ? res.stype[v] : 0;
							       }).reduce(function(p,c) {
									    return p + c;
									 });
					return val.num <= num &&
					       (!val.flagship || val.mask.some(function(v,i,a) { return v == flagship_stype; }));
				      });
	if (param.slotitem)
	    pass &= Object.keys(param.slotitem).every(function(val,idx,array){
							return res.slotitem[val] &&
							       (!param.slotitem[val].ship || (res.slotitem[val].ship && param.slotitem[val].ship <= res.slotitem[val].ship)) &&
							       (!param.slotitem[val].num || (res.slotitem[val].num && param.slotitem[val].num <= res.slotitem[val].num));
						      });
	return pass;
    },

    _deckAvailable: function(deckid) {
	let basic = KanColleDatabase.memberBasic.get();
	let deck = KanColleDatabase.deck.get(deckid);
	return (basic && deck &&
	        deckid <= basic.api_count_deck &&
		!deck.api_mission[1]);
    },

    setMission: function(deckid, missionid) {
	if (!this._deckAvailable(deckid) || !missionid)
	    return;
	this._mission_plan[deckid] = missionid;
	this.update.deck.call(this);
    },

    checkMission: function(deckid) {
	let res = this._checkMission(deckid);
	SetStyleProperty($('shipstatus-' + deckid + '-0'), 'outline', (res === 0) ? 'solid red 2px' : '');
	return res;
    },

    _menuShowing: function(e) {
	let min_cond = 101;
	let deck;
	if (!this._deckid)
	    return e.preventDefault();
	deck = KanColleDatabase.deck.get(this._deckid);
	if (deck) {
	    for (let i = 0; i < deck.api_ship.length; i++) {
		if (!this._ship || this._ship - 1 == i) {
		    let cond = FindShipCond(deck.api_ship[i]);
		    if (isNaN(cond))
			continue;
		    if (cond < min_cond)
			min_cond = cond;
		}
	    }
	}
	if (min_cond < 49) {
	    let t = KanColleDatabase.ship.timestamp();
	    let time_offset = KanColleUtils.getIntPref('time-offset', 0);
	    let time = t - ((t - time_offset) % 180000) + Math.ceil((49 - min_cond) / 3) * 180000;
	    let d = new Date;
	    d.setTime(time);
	    $('shipstatus-context-settimer').label = 'タイマー設定(' + d.toLocaleTimeString() + ')';
	    $('shipstatus-context-settimer').setAttribute('disabled', 'false');
	    $('shipstatus-context-settimer').addEventListener('command', function() { KanColleTimer.setGeneralTimerByTime(time); }, true);
	} else {
	    $('shipstatus-context-settimer').label = 'タイマー設定';
	    $('shipstatus-context-settimer').setAttribute('disabled', 'true');
	}

	if (this._deckid != 1 && !this._ship) {
	    let list = [ 'maparea', 'material' ];
	    let isdisabled = !this._deckAvailable(this._deckid);
	    let mission;
	    let id;

	    for (let i = 0; i < list.length; i++) {
		let n = $('shipstatus-context-check-' + list[i]);
		n.setAttribute('disabled', isdisabled ? 'true' : 'false');
	    }
	    $('shipstatus-context-check').collapsed = false;
	} else
	    $('shipstatus-context-check').collapsed = true;
    },

    _missionMenuCommand: function(v) {
	let missionid = parseInt(v, 10);
	this.setMission(this._deckid, missionid);
    },

    handleEvent: function(e) {
	let curTarget = e.currentTarget;
	//debugprint("e.type=" + e.type + "; e.target.id=" + e.target.id + "; e.currentTarget.id=" + curTarget.id);
	switch (e.type) {
	case 'contextmenu':
	    this._deckid = parseInt(e.target.getAttribute('data-deckid'), 10);
	    this._ship = parseInt(e.target.getAttribute('data-ship'), 10);
	    break;
	default:;
	}
    },

    show: function() {
	let checked = $('fleetinfo-toggle').getAttribute('checked') == 'true';
	$('fleetinfo').collapsed = !checked;
    },

    restore: function() {
	this.show();
    },

    init: function() {
	debugprint("init");
	this._update_init();
	$('shipstatus-grid').addEventListener('contextmenu', this, true);
    },

    exit: function() {
	$('shipstatus-grid').removeEventListener('contextmenu', this);
	this._update_exit();
    },
};
KanColleTimerFleetInfo.__proto__ = __KanColleTimerPanel;

var KanColleTimerQuestInfo = {
    _state: null,
    _localstate: null,

    update: {
	quest: function() {
	    let questbox = $('quest-list-box');
	    let ids = KanColleDatabase.quest.list();
	    let list = $('quest-list-rows');
	    let listitem;
	    let staletime = KanColleDatabase.ship.timestamp();
	    let mode = parseInt($('quest-information-mode-popup-filter').selectedItem.value, 10);
	    let tooltips = {};
	    let now = (new Date).getTime();

	    function deadline(t,type){
		const fuzz = 60000;
		let elapsed;	// 期間開始からの経過時間
		let t2;
		let cur;
		let cur_day;
		let cur_date;
		let cur_month;
		let cur_year;
		let start_time;
		let next_time;

		if (!t || t < 0 || type == 0)
		    return -1;	// 期限なし、または エラー

		t2 = t + 14400000;	// 5:00JST を 0時UTCとみなすため
		cur = new Date(t2);
		cur_day = (cur.getUTCDay() + 6) % 7;	// 0: Monday
		cur_date = cur.getUTCDate();
		cur_month = cur.getUTCMonth();		// 0: January
		cur_year = cur.getUTCFullYear();

		switch (type) {
		case 0:	// none
		    return 0;
		case 1:	// daily
		    start_time = Date.UTC(cur_year, cur_month, cur_date, 5, 0, 0) - 32400000;
		    next_time = start_time + 86400000;
		    break;
		case 2:	// weekly
		    start_time = Date.UTC(cur_year, cur_month, cur_date, 5, 0, 0) - 32400000;
		    start_time -= cur_day * 86400000;
		    next_time = start_time + 86400000 * 7;
		    break;
		case 3: // monthly
		    start_time = Date.UTC(cur_year, cur_month, 1, 0, 0, 0) - 14400000;
		    // Javascript accepts overflow
		    next_time = Date.UTC(cur_year, cur_month + 1, 1, 0, 0, 0) - 14400000;
		    break;
		default:
		    return -1;
		}

		elapsed = t - start_time;
		if (elapsed < fuzz)
		    return 0;	// 時計ずれを考慮

		return next_time;
	    }

	    // clear
	    RemoveChildren(list);

	    for (let i = 0; i < ids.length; i++) {
		let no = ids[i];
		let qdata = KanColleData.quest[no];
		let listitem = CreateElement('row');
		let cell;
		let t;
		let q = KanColleDatabase.quest.get(ids[i]);
		let qt = KanColleDatabase.quest.timestamp(ids[i]);
		let type = 0;
		let tid = 'quest-information-deadline-tooltip-' + i;
		let in_progress;
		let was_in_progress;
		let extradata;
		let _extradata;
		let state = '';
		let prog;

		function update_quest_count(ptype, param, d, _d)
		{
		    let ret = null;
		    let v = null;
		    switch (ptype) {
		    case 'map':
		      {
			/* 戦闘結果 */
			//debugprint('param='+param.toSource());
			let stat = KanColleDatabase.battle.stat();
			if (!d.v)
			    d.v = 0;
			v = 0;
			if (stat) {
			    let re = param.regexp ? new RegExp('^' + param.regexp) : null;
			    v = Object.keys(stat.map)
					.filter(function(v) {
					    /* 戦闘セル */
					    //debugprint('1 filter v='+v.toSource());
					    return (!re || re.exec(v) != null);
					})
					.map(function(v) {
					    /* セル毎統計 */
					    //debugprint('2 map v='+v.toSource());
					    return stat.map[v];
					})
					.filter(function(v) {
					    /* 演習/ボス判定 */
					    //debugprint('3 filter v='+v.toSource());
					    return (!param.practice == !v.practice) &&
						   (!param.boss || v.boss);
					})
					.map(function(v) {
					    /* 戦闘結果ランク集計 */
					    //debugprint('4 map v='+v.toSource());
					    return (param.rank || Object.keys(v.rank))
						    .map(function(_v) {
							return v.rank[_v] || 0;
						    })
						    .reduce(function(p,c) {
							return p + c;
						    }, 0);
					})
					.reduce(function(p,c) {
					    return p + c;
					}, 0);
			}
			break;
		      }
		    case 'stype':
		      {
			/* 敵撃沈艦 */
			let stypes = param.mask;
			let stat = KanColleDatabase.battle.stat();
			if (!d.v)
			    d.v = 0;
			v = 0;
			if (stat) {
			    for (let i = 0; i < stypes.length; i++) {
				let stype = stypes[i];
				v += stat.type[stype] || 0;
			    }
			}
			break;
		      }
		    case 'first':
		      {
			/* 出撃 */
			let stat = KanColleDatabase.battle.stat();
			if (!d.v)
			    d.v = 0;
			v = 0;
			if (stat)
			    v = stat.first;
			break;
		      }
		    case 'mission':
		      {
			/* 遠征 */
			let stat = KanColleDatabase.mission.stat();
			if (!d.v)
			    d.v = 0;
			v = 0;
			if (stat) {
			    v = (param.mask || Object.keys(stat))
				.map(function(v) {
				    return stat[v] || {};
				}).map(function(v) {
				    return (param.rank || Object.keys(v))
					    .map(function(_v) {
						    return v[_v] || 0;
					    })
					    .reduce(function(p,c) {
						return p + c;
					    }, 0);
				})
				.reduce(function(p,c) {
					    return p + c;
				}, 0);
			}
		      }
		    }
		    if (v !== null) {
			if (!was_in_progress && in_progress) {
			    // off => on
			    debugprint('off=>on');
			    _d.b = v;
			} else if (was_in_progress) {
			    // on
			    if (isNaN(_d.b)) {
				debugprint('on(reset)');
				_d.b = v;
			    } else {
				debugprint('on');
			    }
			    d.v += v - (_d.b || 0);
			    _d.b = v;
			}
			ret = d.v;
		    }
		    return ret;
		}

		// title
		cell = CreateElement('label');
		cell.className = 'quest-title';
		cell.setAttribute('value', q.api_title);
		cell.setAttribute('crop', 'end');
		cell.setAttribute('tooltiptext', q.api_detail);
		listitem.appendChild(cell);

		// category
		listitem.setAttribute('data-category', '' + q.api_category);

		// type
		type = q.api_type;

		switch (q.api_type) {
		case 1: t = '';	    type = 0; break;
		case 2: t = '[日]'; type = 1; break;
		case 3: t = '[週]'; type = 2; break;
		case 4: t = '[日]'; type = 1; break;  //3,7,0の日
		case 5: t = '[日]'; type = 1; break;  //2,8の日
		case 6: t = '[月]'; type = 3; break;
		default:
			t = '[' + q.api_type + ']';
		}
		cell = CreateElement('label');
		cell.className = 'quest-deadline';
		cell.setAttribute('value', t);
		listitem.appendChild(cell);

		if (qdata && qdata.type) {
		    // 例えば「艦隊精鋭演習」はtype 6 (マンスリー)
		    // だがカウンタを日単位でリセットするため、
		    // 期限はデイリー扱い。
		    type = qdata.type;
		}
		t = deadline(qt, type);

		/* Save state if (and only if) state has beed loaded */
		if (this._state) {
		    if (!this._localstate) {
			this._localstate = {};
		    }
		    if (!this._state[no] || this._state[no].t < now ||
			this._state[no].t < t) {
			this._state[no] = { d: {}, };
			this._localstate[no] = {};
		    }
		    if (!this._localstate)
			this._localstate = {};
		    if (!this._localstate[no])
			this._localstate[no] = {};
		    in_progress = q.api_state == 2 ? 1 : 0;
		    was_in_progress = this._state[no].p;

		    this._state[no].p = in_progress;
		    this._state[no].t = t;

		    extradata = this._state[no].d;
		    _extradata = this._localstate[no];

		    if (qdata && qdata.counter) {
			debugprint(qdata.toSource());
			let qres = [];
			let qp = 0;
			let qc = 0;
			for (let qi = 0; qi < qdata.counter.length; qi++) {
			    let q = qdata.counter[qi];
			    let qr;
			    if (!extradata[qi])
				extradata[qi] = {};
			    if (!_extradata[qi])
				_extradata[qi] = {};
			    qr = update_quest_count(q.type, q.param, extradata[qi], _extradata[qi]);
			    if (qr !== null) {
				qp += Math.min(qr, q.num) / q.num;
				qc++;
				qres.push((q.label ? q.label + ': ' : '') + qr + '/' + q.num);
			    }
			}
			if (qc) {
			    qp /= qc;
			    state = Math.floor(qp * 10000) / 100;
			    if (state >= 100)
				prog = 100;
			    else if (state >= 80)
				prog = 80;
			    else if (state >= 50)
				prog = 50;
			    else if (state >= 0)
				prog = 0;
			    else
				prog = -1;
			    state += '%\n';
			}
			listitem.setAttribute('data-counter', '' + prog);
			state += '\t' + qres.join('\n\t');
		    }

		    debugprint(no + "(" + q.api_title + "): " + this._state[no].toSource());
		}

		if (t > 0) {
		    let tooltip = CreateElement('tooltip');
		    let timer;
		    tooltip.setAttribute('id', tid);

		    timer = CreateElement('timer');
		    timer.mode = 'time';
		    timer.finishTime = '' + t;
		    tooltip.appendChild(timer);

		    tooltips[tid] = tooltip;

		    cell.setAttribute('tooltip', tid);
		}

		// progress
		t = '?';
		prog = -1;
		if (q.api_state == 1 ||
		    q.api_state == 2) {
		    switch (q.api_progress_flag) {
		    case 0:
			    t = '  ';
			    prog = 0;
			    break;
		    case 1: //50%
			    t = '50';
			    prog = 50;
			    break;
		    case 2: //80%
			    t = '80';
			    prog = 80;
			    break;
		    }
		} else if (q.api_state == 3) {
		    //t = '\u2713';	//check mark
		    t = 'OK';
		    prog = 100
		}

		cell = CreateElement('label');
		cell.className = 'quest-progress';
		cell.setAttribute('value', t);
		if (state.length)
		    cell.setAttribute('tooltiptext', state);
		listitem.appendChild(cell);

		// 進行中
		listitem.setAttribute('data-inprogress', q.api_state > 1 ? 'true' : 'false');
		// 0,50,80,100
		listitem.setAttribute('data-progress', '' + prog);

		// 古いときは灰色に。
		listitem.setAttribute('data-stale',
				      (!staletime || qt < staletime) ? 'true' : 'false');

		//debugprint('no: ' + no +
		//	   '[state: ' + q.api_state +
		//	   ', flag:' + q.api_progress_flag +
		//	   '] title: ' + q.api_title +
		//	   '; detail: ' + q.api_detail);

		if (mode == 1) {
		    // 遂行していないかつ進捗なし(-50%)
		    if (q.api_state == 1 &&
			q.api_progress_flag == 0)
			continue;
		} else if (mode == 2) {
		    // 遂行中でも達成済でもない
		    if (q.api_state != 2 && q.api_state != 3)
			continue;
		} else if (mode == 3) {
		    // 遂行中でない
		    if (q.api_state != 2)
			continue;
		}

		list.appendChild(listitem);
	    }

	    if (questbox) {
		for (let tid in tooltips) {
		    let node = $(tid);
		    if (node)
			node.parentNode.replaceChild(tooltips[tid], node);
		    else
			questbox.appendChild(tooltips[tid]);
		}
	    }

	    this._save_state();
	},
	ship: 'quest',
	battle: function(stage) {
	    // battle result
	    if (stage == 3)
		this.update.quest.call(this);
	},
	masterMission: function() {
	    this._load_state();
	    this.update.quest.call(this);
	},
    },

    _load_state: function() {
	let data = KanColleUtils.readObject('mission', null);
	this._localstate = null;
	if (!data) {
	    this._state = {};
	    return;
	}
	if (data.ver == '0.2') {
	    this._state = data.data;
	    //debugprint('mission state loaded: ' + this._state.toSource());
	} else {
	    this._state = {};
	}
    },

    _save_state: function() {
	if (this._state) {
	    let data = {
		ver: '0.2',
		data: this._state,
	    };
	    KanColleUtils.writeObject('mission', data);
	    //debugprint('mission state saved: ' + data.toSource());
	}
    },

    show: function() {
	let checked = $('questinfo-toggle').getAttribute('checked') == 'true';
	$('quest-list-box').collapsed = !checked;
    },

    restore: function() {
	let val = KanColleTimerConfig.getInt('quest-info.mode');
	if (!val) {
	    KanColleTimerConfig.setInt('quest-info.mode', 0);
	    val = 0;
	}
	$('quest-information-mode-popup-filter').selectedIndex = val;
	this.update.quest.call(this);
	this.show();
    },

    changeMode: function(node) {
	KanColleTimerConfig.setInt('quest-info.mode', node.selectedIndex);
	this.update.quest.call(this);
    },
};
KanColleTimerQuestInfo.__proto__ = __KanColleTimerPanel;

var KanColleTimerMapInfo = {
    update: {
	memberMapinfo: function() {
	    let l = KanColleDatabase.memberMapinfo.list();
	    let rows = $('mapinfo-grid-rows');
	    if (!KanColleDatabase.masterMapinfo.timestamp() || !rows)
		return;
	    RemoveChildren(rows);
	    for (i = 0; i < l.length; i++) {
		let label_id, label_name, label_progress;
		let d = KanColleDatabase.memberMapinfo.get(l[i]);
		let md;
		let row;

		row = CreateElement('row');

		md = KanColleDatabase.masterMapinfo.get(d.api_id);
		label_id = CreateElement('label');
		label_id.className = 'mapinfo-id';
		label_id.setAttribute('value', md.api_maparea_id + '-' + md.api_no);
		label_name = CreateElement('label');
		label_name.className = 'mapinfo-name';
		label_name.setAttribute('value', md.api_name);
		label_name.setAttribute('tooltiptext', md.api_opetext);
		label_progress = CreateElement('label');
		label_progress.className = 'mapinfo-progress';

		if (d.api_eventmap) {
		    if (d.api_eventmap.api_state == 2) {
			label_progress.setAttribute('value', '済');
			label_progress.setAttribute('tooltiptext', '突破済');
		    } else if (d.api_eventmap.api_max_maphp) {
			if (d.api_eventmap.api_selected_rank) {
			    label_progress.setAttribute('value', (d.api_eventmap.api_now_maphp * 100 / d.api_eventmap.api_max_maphp).toFixed(0));
			    label_progress.setAttribute('tooltiptext', d.api_eventmap.api_now_maphp + ' / ' + d.api_eventmap.api_max_maphp);
			} else {
			    label_progress.setAttribute('value', '未');
			    label_progress.setAttribute('tooltiptext', '未選択');
			}
		    }
		    row.setAttribute('data-selected-rank', '' + (d.api_eventmap.api_selected_rank || 0));
		    row.setAttribute('data-cleared', d.api_eventmap.api_state == 2 ? 'true' : 'false');
		    row.setAttribute('data-gauge-type', '' + (d.api_eventmap.api_gauge_type || 0));
		} else if (md.api_required_defeat_count) {
		    label_progress.setAttribute('value', ((md.api_required_defeat_count - d.api_defeat_count) * 100 / md.api_required_defeat_count).toFixed(0));
		    label_progress.setAttribute('tooltiptext', (md.api_required_defeat_count - d.api_defeat_count) + ' / ' + md.api_required_defeat_count);
		}

		row.appendChild(label_id);
		row.appendChild(label_name);
		row.appendChild(label_progress);

		if (!d.api_cleared || d.api_eventmap)
		    rows.appendChild(row);
	    }
	},
    },
};
KanColleTimerMapInfo.__proto__ = __KanColleTimerPanel;

var KanColleTimerMissionBalanceInfo = {
    _id: 'hourly_balance',
    _popupMissionId: null,
    _missions: null,
    _calctime: 0,
    _calcrep: false,

    contextMenuPopupShowing: function() {
	for (let i = 2; i <= 4; i++) {
	    let isdisabled = !KanColleTimerFleetInfo._deckAvailable(i);
	    let item = $('hourly_balance_context_' + i);
	    item.setAttribute('disabled', isdisabled ? "true" : "false");
	}
	this._popupMissionId = document.popupNode.getAttribute('data-missionid');
    },

    contextMenuCommand: function(node) {
	let fleet = parseInt(node.getAttribute('value'), 10);
	let mission_id = parseInt(this._popupMissionId, 10);
	KanColleTimerFleetInfo.setMission(fleet, mission_id);
    },

    _calc: function(id, key) {
	let val = KanColleData.mission_param[id].balance[key];
	let time = KanColleData.mission_param[id].time;
	let calctime = this._calctime;
	let count;
	if (!this._calctime) {
	    time = 60;
	    count = 1;
	} else {
	    count = Math.floor(this._calctime / time);
	    if (!this._calcrep && count > 1)
		count = 1;
	}
	return val * time * count / 60;
    },

    _fillTable: function(key){
	let rows = $('hourly_balance');
	let that = this;

	if (!key)
	    key = 0;
	key--;

	if (!this._missions) {
	    this._missions = Object.keys(KanColleData.mission_param).filter(function(e) {
		return !!KanColleData.mission_param[e].balance;
	    });
	}

	this._missions = (function() {
	    let mission_values = that._missions.map(function(e, i) {
		return { id: e, idx: i, };
	    });
	    mission_values.sort(function(a, b) {
		let va = key >= 0 ? that._calc(a.id, key) : -a.id;
		let vb = key >= 0 ? that._calc(b.id, key) : -b.id;
		return res = vb - va || a.idx - b.idx;
	    });
	    return mission_values.map(function(e) {
		return e.id;
	    });
	})();

	for (let i = -1; i < 4; i++)
	    SetStyleProperty($('hourly_balance_label-' + (i + 1)), 'font-weight', i == key ? 'bold' : null);

	this._missions.forEach(function(i) {
	    let l;
	    let row = CreateElement('row');
	    let name = KanColleData.mission_param[i].name;
	    if (!KanColleData.mission_param[i].balance)
		return;
	    row.setAttribute('data-missionid', '' + i);
	    l = CreateLabel( name );
	    l.setAttribute('data-missionid', '' + i);
	    l.setAttribute('context', 'hourly_balance_context');
	    row.appendChild(l);
	    for( let j=0; j<4; j++ ){
		row.appendChild( CreateLabel(that._calc(i,j).toFixed(2)) );
	    }
	    row.setAttribute("style","border-bottom: 2px solid gray;");
	    row.setAttribute("tooltiptext", KanColleData.mission_param[i].help );
	    rows.appendChild( row );
	});
    },

    _clearTable: function() {
	let parent = $(this._id);
	let node, next;
	for (node = parent.firstChild; node; node = next) {
	    next = node.nextSibling;
	    if (node.getAttribute('data-missionid'))
		RemoveElement(node);
	}
    },

    change: function() {
	let time = parseInt($('hourly_balance_time').value, 10);
	if (isNaN(time) || time < 0)
	    time = 0;
	this._calctime = time;
	if (!time) {
	    $('hourly_balance_repeat').setAttribute('checked', 'true');
	    $('hourly_balance_repeat').setAttribute('disabled', 'true');
	} else {
	    $('hourly_balance_repeat').setAttribute('disabled', 'false');
	}
	this._calcrep = $('hourly_balance_repeat').getAttribute('checked') == 'true';
	this._clearTable();
	this._fillTable(this._resid);
    },

    changesort: function(node) {
	let resid = parseInt(node.getAttribute('data-resid'), 10);
        this._resid = resid;
	this._clearTable();
	this._fillTable(resid);
    },

    init: function() {
	this._update_init();
        this._resid = 0;
	this._fillTable();
    },

    exit: function() {
	this._clearTable();
	this._update_exit();
    },
};
KanColleTimerMissionBalanceInfo.__proto__ = __KanColleTimerPanel;


var KanColleTimerPracticeInfo = {
    _calc_baseexp: function(ships) {
	let exp = -500;
	if (ships[0].api_level >= 1)
	    exp += KanColleData.level_accumexp[ships[0].api_level - 1] / 100;
	if (ships[1].api_level >= 1)
	    exp += KanColleData.level_accumexp[ships[1].api_level - 1] / 300;
	return Math.floor(500 + (exp > 0 ? Math.sqrt(exp) : exp));
    },

    update: {
	practice: function() {
	    let s = '';
	    let l = KanColleDatabase.practice.list();
	    for (let i = 0; i < l.length; i++) {
		//const label = [ '-', 'E', 'D', 'C', 'B', 'A', 'S' ];
		let p = KanColleDatabase.practice.get(l[i]);
		let ship;
		let info;
		s += p.api_enemy_name + ' Lv' + p.api_enemy_level + '[' + p.api_enemy_rank + '](' + p.api_state + ')';
		// ship = KanColleDatabase.masterShip.get(p.api_enemy_flag_ship);
		// s += ship ? ship.api_name : ('UNKNOWN_' + p.api_enemy_flag_ship);
		s += FindShipNameByCatId(p.api_enemy_flag_ship) + '(' + p.api_enemy_flag_ship + ')';
		info = KanColleDatabase.practice.find(p.api_enemy_id);
		if (info) {
		    s += ': ' + info.api_deckname;
		    if (info.api_deck && info.api_deck.api_ships) {
			let ships = [];
			for (let j = 0; j < info.api_deck.api_ships.length; j++) {
			    let shipinfo = info.api_deck.api_ships[j];
			    let t = '';
			    if (shipinfo.api_id < 0)
				continue;
			    t += FindShipNameByCatId(shipinfo.api_ship_id) + ' Lv' + shipinfo.api_level + '(';
			    //for (let k = 0; k <= shipinfo.api_star; k++)
			    //	t += '*';
			    t += shipinfo.api_star;
			    t += ')';
			    ships.push(t);
			}
			s += '[' + ships.join(',') + ']; baseexp=' + this._calc_baseexp(info.api_deck.api_ships);
		    }
		}
		s += '\n';
	    }
	    debugprint(s);
	},
    },
};
KanColleTimerPracticeInfo.__proto__ = __KanColleTimerPanel;

function AddLog(str){
    $('log').value = str + $('log').value;
}

var ShipListView = null;
function SaveShipList(){
    if (!ShipListView)
	return;
    ShipListView.saveShipList();
}

function OpenShipList(){
    let feature="chrome,resizable=yes";
    let w = window.open("chrome://kancolletimer/content/shiplist.xul","KanColleTimerShipList",feature);
    w.focus();
}

function OpenNewShipList(){
    if( KanColleTimerConfig.getBool( 'tab-open.shiplist2' ) )
	OpenDefaultBrowser( 'chrome://kancolletimer/content/shiplist/shiplist.xul', true);
    else {
	let feature="chrome,resizable=yes";
	let w = window.open("chrome://kancolletimer/content/shiplist/shiplist.xul","KanColleTimerNewShipList",feature);
	w.focus();
    }
}

function OpenResourceGraph(){
    if( KanColleTimerConfig.getBool( 'tab-open.resourcegraph' ) )
	OpenDefaultBrowser( 'chrome://kancolletimer/content/resourcegraph.xul', true);
    else
	window.open('chrome://kancolletimer/content/resourcegraph.xul','KanColleTimerResourceGraph','chrome,resizable=yes').focus();
}

function OpenAboutDialog(){
    var f='chrome,toolbar,modal=no,resizable=no,centerscreen';
    var w = window.openDialog('chrome://kancolletimer/content/about.xul','KanColleTimerAbout',f);
    w.focus();
}

function OpenSettingsDialog(){
    var f='chrome,toolbar,modal=no,resizable=yes,centerscreen';
    var w = window.openDialog('chrome://kancolletimer/content/preferences.xul','KanColleTimerPreference',f);
    w.focus();
}

function OpenTweetDialog(param){
    var f = 'chrome,toolbar,modal=no,resizable=no,centerscreen';
    var w = window.openDialog('chrome://kancolletimer/content/sstweet.xul','KanColleTimerTweet',f,param);
    w.focus();
}

function OpenPowerup() {
    window.open('chrome://kancolletimer/content/powerup.xul','KanColleTimerPowerUp','chrome,resizable=yes').focus();
}

function OpenDropShipList() {
    window.open('chrome://kancolletimer/content/droplist.xul','KanColleTimerDropShipList','chrome,resizable=yes').focus();
}

function OpenVideoRecorder(){
    let feature = "chrome,resizable=yes";
    let w = window.open( "chrome://kancolletimer/content/videorecorder.xul", "KanColleTimerVideoRecorder", feature );
    w.focus();
}

function OpenEquipmentList() {
    if( KanColleTimerConfig.getBool( 'tab-open.equipmentlist' ) )
	OpenDefaultBrowser( 'chrome://kancolletimer/content/equipment/equipmentlist.xul', true);
    else
	window.open('chrome://kancolletimer/content/equipment/equipmentlist.xul','KanColleTimerEquipmentList','chrome,resizable=yes').focus();
}

/**
 * @return スクリーンショットのdataスキーマのnsIURIを返す。艦これのタブがなければnullを返す
 */
function TakeKanColleScreenshot(isjpeg){
    var canvas = document.createElementNS( "http://www.w3.org/1999/xhtml", "canvas" );
    let region = KanColleUtils.getRegion();
    let masks = [];
    let url;

    if (!canvas || !region)
	return null;

    if (KanColleTimerConfig.getBool("screenshot.mask-name")) {
	masks.push({
		    r: 0, g: 0, b: 0,
		    x: 110, y: 5, w: 145, h: 20,
		   });
    }

    if (!KanColleTimerUtils.screenshot.drawCanvas(canvas, null, region, masks))
	return null;

    url = canvas.toDataURL(isjpeg ? 'image/jpeg' : 'image/png');

    KanColleTimerUtils.screenshot.clearCanvas(canvas);

    return Components.classes['@mozilla.org/network/io-service;1']
	    .getService(Components.interfaces.nsIIOService)
	    .newURI(url, null, null);
}

function TakeKanColleScreenshot_canvas(isjpeg){
    var canvas = document.createElementNS( "http://www.w3.org/1999/xhtml", "canvas" );
    let region = KanColleUtils.getRegion();
    let masks = [];
    let url;

    if (!canvas || !region)
	return null;

    if (!KanColleTimerUtils.screenshot.drawCanvas(canvas, null, region, masks))
	return null;

    return canvas;
}

/*

 */
function FindSlotItemNameById( api_id ){
    let item = KanColleDatabase.slotitem.get(api_id);
    let itemname = '[Unknown]';
    if (item) {
	let itemtype = KanColleDatabase.masterSlotitem.get(item.api_slotitem_id);
	if (itemtype)
	    itemname = itemtype.api_name;
    }
    return itemname;
}

function FindShipNameByCatId( id ){
    try{
	// 全艦データから艦艇型IDをキーに艦名を取得
	return KanColleDatabase.masterShip.get(id).api_name;
    } catch (x) {
    }
    return "";
}

/**
 * 自分の保有している艦のデータを返す.
 */
function FindOwnShipData( ship_id ){
    return KanColleDatabase.ship.get(ship_id);
}

/**
 * 艦のデータを返す
 */
function FindShipData( ship_id ){
    let ship = KanColleDatabase.ship.get(ship_id);
    if (ship)
	return KanColleDatabase.masterShip.get(ship.api_ship_id);
    return undefined;
}

/**
 * 艦艇の名前を返す
 */
function FindShipName( ship_id ){
    try{
	// member/ship2 には艦名がない。艦艇型から取得
	let ship = KanColleDatabase.ship.get(ship_id);
	return FindShipNameByCatId( ship.api_ship_id );
    } catch (x) {
    }
    return "";
}

function FindShipCond( ship_id ){
    try{
	let ship = KanColleDatabase.ship.get(ship_id);
	return parseInt(ship.api_cond, 10);
    } catch (x) {
    }
    return undefined;
}

function FindShipStatus( ship_id ){
    try{
	let info = {
	    fuel: undefined,
	    fuel_max: undefined,
	    bull: undefined,
	    bull_max: undefined,
	    nowhp: undefined,
	    maxhp: undefined,
	};

	// member/ship には fuel, bull, nowhp, maxhp
	let ship = KanColleDatabase.ship.get(ship_id);

	info.fuel = parseInt(ship.api_fuel, 10);
	info.bull = parseInt(ship.api_bull, 10);
	info.nowhp = parseInt(ship.api_nowhp, 10);
	info.maxhp = parseInt(ship.api_maxhp, 10);

	// fuel_max と bull_max は master/shipから
	ship = KanColleDatabase.masterShip.get(ship.api_ship_id);

	info.fuel_max = parseInt(ship.api_fuel_max, 10);
	info.bull_max = parseInt(ship.api_bull_max, 10);

	return info;
    } catch (x) {
    }
    return undefined;
}

function ShipCalcSearchBase(shipid) {
    let ship = KanColleDatabase.ship.get(shipid);
    let search = 0;

    if (!ship || !ship.api_sakuteki)
	return -1;

    search = ship.api_sakuteki[0];

    return search -
	   ship.api_slot.map(function(e) {
	let item = KanColleDatabase.slotitem.get(e);
	let itemtype = item ? KanColleDatabase.masterSlotitem.get(item.api_slotitem_id) : null;
	return itemtype ? itemtype.api_saku : 0;
    }).reduce(function(prev, cur) {
	return prev + cur;
    }, 0);
}

function ShipCalcSearch(shipid) {
    let ship = KanColleDatabase.ship.get(shipid);
    let search = 0;

    if (!ship || !ship.api_sakuteki)
	return -1;

    return ship.api_sakuteki[0];
}

function ShipCalcSearchEquip(shipid,equip) {
    let ship = KanColleDatabase.ship.get(shipid);
    let val = 0;

    if (!ship)
	return -1;

    for (let j = 0; j < ship.api_slot.length && j < ship.api_onslot.length; j++) {
	let item = KanColleDatabase.slotitem.get(ship.api_slot[j]);
	let itemtype;

	if (!item)
	    continue;

	itemtype = KanColleDatabase.masterSlotitem.get(item.api_slotitem_id);
	if (itemtype && itemtype.api_type[1] == equip)
	    val += itemtype.api_saku;
    }
    return val;
}

function __ShipCalcSearchV3(factors, shipid) {
    let nomials;
    let base = ShipCalcSearchBase(shipid);
    let ship = KanColleDatabase.ship.get(shipid);

    if (base < 0 || !ship)
	return null;
    base = Math.sqrt(base);

    nomials = ship.api_slot
		.map(function(e) {
		    let item = KanColleDatabase.slotitem.get(e);
		    let itemtype;
		    if (!item)
			return { v: 0, f: { f2: 0, }, };
		    itemtype = KanColleDatabase.masterSlotitem.get(item.api_slotitem_id);
		    if (!itemtype)
			return { v: 0, f: { f2: 0, }, };
		    let obj = { v: itemtype.api_saku,
			     f: factors.slotitem[itemtype.api_type[1]] || factors.slotitem["*"],
			   };
		    return obj;
		});
    nomials.unshift({ v: base, f: factors.base, });

    return nomials.reduce(function(p,c) { return p + c.v * c.f.f2; }, 0);
}

function FleetCalcSearchV3(ships)
{
    let factors = KanColleData.search_factors;
    let nomials;

    let lv = KanColleDatabase.record.get().api_level;
    if (isNaN(lv))
	return null;
    // 切り上げ
    lv += 4;
    lv -= lv % 5;

    nomials = ships.map(function(e) { return __ShipCalcSearchV3(factors, e); });
    nomials.push(lv * factors.lv.f2);

    //debugprint(nomials.toSource());

    return nomials.reduce(function(p,c) {
	return p + Math.floor(c);
    }, 0);
}

function __ShipCalcSearchV2(factors, shipid) {
    let nomials;
    let base = ShipCalcSearchBase(shipid);

    if (base < 0)
	return null;
    base = Math.sqrt(base);

    nomials = Object.keys(factors.slotitem).map(function(e) {
	return { v: ShipCalcSearchEquip(shipid, e), f: factors.slotitem[e], };
    });
    nomials.unshift({ v: base, f: factors.base, });

    return nomials.reduce(function(p,c) {
	return { f: p.f + c.v * c.f.f, e: p.e + c.v * c.f.e, };
    }, { f: 0, e: 0, });
}

function FleetCalcSearchV2(ships)
{
    let factors = KanColleData.search_factors;
    let nomials;

    let lv = KanColleDatabase.record.get().api_level;
    if (isNaN(lv))
	return null;
    // 切り上げ
    lv += 4;
    lv -= lv % 5;

    nomials = ships.map(function(e) { return __ShipCalcSearchV2(factors, e); });
    nomials.push({ f: lv * factors.lv.f, e: lv * factors.lv.e, });

    //debugprint(nomials.toSource());

    return nomials.reduce(function(p,c) {
	return { f: p.f + (c ? c.f : 0),
		 e: p.e + (c ? c.e : 0), };
    }, { f: 0, e: 0, });
}

function ShipCalcSearchSurveilanceAircraft(shipid) {
    return ShipCalcSearchEquip(shipid, 7 /* 偵察機 */);
}

function ShipCalcSearchRadar(shipid) {
    return ShipCalcSearchEquip(shipid, 8 /* 電探 */);
}

function ShipCalcAirPower(shipid) {
    let ship = KanColleDatabase.ship.get(shipid);
    let ap = 0;

    if (!ship)
	return -1;

    for (let j = 0; j < ship.api_slot.length && j < ship.api_onslot.length; j++) {
	let item = KanColleDatabase.slotitem.get(ship.api_slot[j]);
	let itemtype;

	if (!item)
	    continue;

	itemtype = KanColleDatabase.masterSlotitem.get(item.api_slotitem_id);
	if (!itemtype || itemtype.api_tyku == 0)
	    continue;
	if (itemtype.api_type[1] == 5 ||
	    itemtype.api_type[1] == 7) {
	    ap += Math.floor(itemtype.api_tyku * Math.sqrt(ship.api_onslot[j]));
	}
    }
    return ap;
}

/*
 * Tree
 */
var ShipInfoTree = {
    /* Columns*/
    COLLIST: [
	{ label: '艦隊', id: 'fleet', flex: 1, },
	{ label: 'ID', id: 'id', flex: 1, },
	//{ label: '艦種', id: 'type', flex: 2, },
	{ label: '艦種', id: 'stype', flex: 1,
	  sortspecs: [
	    {
		sortspec: '_stype',
		label: '艦種',
		skipdump: true,
	    },
	  ],
	},
	{ label: '艦名', id: 'name', flex: 3, always: true,
	  sortspecs: [
	    {
		sortspec: 'id',
		label: 'ID',
		skipdump: true,
	    },
	    {
		sortspec: '_stype',
		label: '艦種',
		skipdump: true,
	    },
	    {
		sortspec: '_yomi',
		label: 'ヨミ',
	    },
	  ],
	},
	{ label: 'Lv', id: 'lv', flex: 1,
	  subdump: true,
	  sortspecs: [
	    {
		sortspec: '_lv',
		label: 'Lv',
	    },
	    {
		sortspec: '_lvupg',
		label: '次改装Lv',
	    },
	    {
		sortspec: '_lvupgremain',
		label: '次改装Lv残',
	    },
	  ],
	},
	{ label: '経験値', id: 'exp', flex: 2,
	  subdump: true,
	  sortspecs: [
	    {
		sortspec: '_exp',
		label: '経験値',
	    },
	    {
		sortspec: '_expnext',
		label: '次Lv経験値',
	    },
	    {
		sortspec: '_expnextremain',
		label: '次Lv経験値残',
	    },
	    {
		sortspec: '_expupg',
		label: '次改装Lv経験値',
	    },
	    {
		sortspec: '_expupgremain',
		label: '次改装Lv経験値残',
	    },
	  ],
	},
	{ label: 'HP', id: 'hp', flex: 1,
	  subdump: true,
	  sortspecs: [
	    {
		sortspec: '_hp',
		label: 'HP',
	    },
	//    {
	//	sortspec: 'hpratio',
	//	label: 'HP%',
	//    },
	    {
		sortspec: '_maxhp',
		label: 'MaxHP',
	    },
	  ],
	},
	{ label: '火力', id: 'karyoku', flex: 1,
	  subdump: true,
	  sortspecs: [
	    {	sortspec: '_karyoku',	    label: '火力',	    },
	    {	sortspec: '_karyokumax',    label: '最大火力',	    },
	    {	sortspec: '_karyokuremain', label: '火力強化余地',  },
	  ],
	},
	{ label: '雷装', id: 'raisou', flex: 1,
	  subdump: true,
	  sortspecs: [
	    {	sortspec: '_raisou',	    label: '雷装',	    },
	    {	sortspec: '_raisoumax',    label: '最大雷装',	    },
	    {	sortspec: '_raisouremain',  label: '雷装強化余地',  },
	  ],
	},
	{ label: '対空', id: 'taiku', flex: 1,
	  subdump: true,
	  sortspecs: [
	    {	sortspec: '_taiku',	    label: '対空',	    },
	    {	sortspec: '_taikumax',	    label: '最大対空',	    },
	    {	sortspec: '_taikuremain',   label: '対空強化余地',  },
	  ],
	},
	{ label: '装甲', id: 'soukou', flex: 1,
	  subdump: true,
	  sortspecs: [
	    {	sortspec: '_soukou',	    label: '装甲',	    },
	    {	sortspec: '_soukoumax',	    label: '最大装甲',	    },
	    {	sortspec: '_soukouremain',  label: '装甲強化余地',  },
	  ],
	},
	{ label: '回避', id: 'kaihi', flex: 1, },
	{ label: '対潜', id: 'taisen', flex: 1, },
	{ label: '索敵', id: 'sakuteki', flex: 1, },
	{ label: '速力', id: 'soku', flex: 1, },
	{ label: '射程', id: 'length', flex: 1, },
	{ label: '運', id: 'lucky', flex: 1,
	  subdump: true,
	  sortspecs: [
	    {	sortspec: '_lucky',	    label: '運',	    },
	    {	sortspec: '_luckymax',	    label: '最大運',	    },
	    {	sortspec: '_luckyremain',   label: '運強化余地',    },
	  ],
	},
	{ label: '士気', id: 'cond', flex: 1, },
	{ label: '入渠', id: 'ndock', flex: 1,
	  subdump: true,
	  sortspecs: [
	    {
		sortspec: '_ndock',
		label: '入渠時間',
	    },
	  ],
	},
	{ label: '燃料', id: 'fuel', flex: 1,
	  subdump: true,
	  sortspecs: [
	    {	sortspec: '_fuel',	    label: '燃料',	    },
	    {	sortspec: '_fuelmax',	    label: '最大燃料',	    },
	    {	sortspec: '_fuelremain',    label: '不足燃料',      },
	  ],
	},
	{ label: '弾薬', id: 'bull', flex: 1,
	  subdump: true,
	  sortspecs: [
	    {	sortspec: '_bull',	    label: '弾薬',	    },
	    {	sortspec: '_bullmax',	    label: '最大弾薬',	    },
	    {	sortspec: '_bullremain',    label: '不足弾薬',      },
	  ],
	},
	{ label: '装備1', id: 'slotitem1', flex: 1, },
	{ label: '装備2', id: 'slotitem2', flex: 1, },
	{ label: '装備3', id: 'slotitem3', flex: 1, },
	{ label: '装備4', id: 'slotitem4', flex: 1, },
    ],
    collisthash: {},
    /* Filter */
    filterspec: null,
    /* Sorting */
    sortkey: null,
    sortspec: null,
    sortorder: null,
    _sortold: {},
    sortold: {},
};

// Filter by Stype (Ship Class)
function __KanColleStypeFilterTemplate(type2spec){
    let stypes = KanColleDatabase.masterStype.list().reduce(function(p, c) {
	p[c] = KanColleDatabase.masterStype.get(c).api_name;
	return p;
    }, {});
    let shiptypes = KanColleDatabase.ship.list().map(function(e) {
	let ship = KanColleDatabase.ship.get(e);
	if (ship) {
	    let shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
	    if (shiptype)
		return shiptype.api_stype;
	}
	return -1;
    }).reduce(function(p, c) {
	p[c] = !p[c] || p[c] + 1;
	return p;
    }, {});
    let stypegroup = [
	{ label: '駆逐系',
	  types: [
	    { id: 2, }	    //駆逐
	  ],
	},
	{ label: '軽巡系',
	  types: [
	    { id: 3, },	    //軽巡
	    { id: 4, },	    //雷巡
	  ],
	},
	{ label: '重巡系',
	  types: [
	    { id: 5, },	    //重巡
	    { id: 6, },	    //航巡
	  ]
	},
	{ label: '戦艦系',
	  types: [
	    { id: 8, label: '高速戦艦' },
	    { id: 9, },	    //戦艦
	    { id: 10, },    //航空戦艦
	  ]
	},
	{ label: '空母系',
	  types: [
	    { id: 7, },	    //水母
	    { id: 11, },    //軽水母
	    { id: 16, },    //空母
	    { id: 18, },    //装甲空母
	  ],
	},
	{ label: '潜水艦系',
	  types: [
	    { id: 13, },    //潜水艦
	    { id: 14, },    //潜水空母
	  ],
	},
	{ label: '特務艦',
	  types: [
	    { id: 17, },    //揚陸艦
	    { id: 19, },    //工作艦
	    { id: 20, },    //潜水母艦
	    { id: 21, },    //練習巡洋艦
	    { id: 22, },    //補給艦
	  ],
	},
    ];
    let menu = [];

    if (Object.keys(stypes).length && Object.keys(shiptypes).length) {
	stypegroup.push({
	    label: '未分類',
	    types: Object.keys(stypes).filter(function(e,i,a) {
		    return stypegroup.every(function(f,j,b) {
			return f.types.every(function(g,k,c) {
			    return g.id != e;
			});
		    });
		   }).map(function(e) { return { id: e, log: true, }; }),
	});
	for( let i = 0; i < stypegroup.length; i++ ){
	    let count = 0;
	    let submenu = {
		label: stypegroup[i].label,
		menu: [],
	    };
	    for( let j = 0; j < stypegroup[i].types.length; j++ ){
		let label = stypegroup[i].types[j].label;
		let subsubmenu, _subsubmenu;
		if (!label)
		    label = stypes[stypegroup[i].types[j].id];
		if (!label)
		    label = 'UNKNOWN_' + stypegroup[i].types[j].id;
		// 未所持の艦種はスキップ
		if (!shiptypes[stypegroup[i].types[j].id])
		    continue;

		if (stypegroup[i].types[j].log) {
		    debugprint('未分類' + stypegroup[i].types[j].id +
			       ': ' + label);
		}

		_subsubmenu = type2spec([ stypegroup[i].types[j].id ]);
		subsubmenu = {
		    label: label,
		};
		if (typeof(_subsubmenu) == 'object')
		    subsubmenu.menu = _subsubmenu;
		else
		    subsubmenu.spec = _subsubmenu;
		submenu.menu.push(subsubmenu);
		count++;
	    }
	    if (count > 1) {
		let _subsubmenu = type2spec(stypegroup[i].types.map(function(e) { return e.id; }));
		let subsubmenu = {
		    label: stypegroup[i].label + 'すべて',
		};
		if (typeof(_subsubmenu) == 'object')
		    subsubmenu.menu = _subsubmenu;
		else
		    subsubmenu.spec = _subsubmenu;
		submenu.menu.unshift(subsubmenu);
	    }
	    menu.push(submenu);
	}
    }

    return {
	label: '艦種',
	menu: menu,
    };
}

// Filter by Slotitems
function __KanColleSlotitemFilterTemplate(itemok){
    let menu = [];
    let submenu = null;
    let slotitemowners = KanColleDatabase.slotitem.get(null,'owner');
    let levelkeys = KanColleDatabase.slotitem._levelkeys;

    let itemlist = Object.keys(slotitemowners).sort(function(a,b){
	let type_a = slotitemowners[a].type[2];
	let type_b = slotitemowners[b].type[2];
	let id_a = slotitemowners[a].id;
	let id_b = slotitemowners[b].id;
	let diff = type_a - type_b;
	if (!diff)
	    diff = id_a - id_b;
	return diff;
    });

    for (let i = 0; i < itemlist.length; i++) {
	let k = itemlist[i];
	let itemname = slotitemowners[k].name;
	let itemtype = slotitemowners[k].type[2];
	let itemeqtype = KanColleDatabase.masterSlotitemEquiptype.get(itemtype);
	let itemtypename = itemeqtype.api_name;
	let itemnum = slotitemowners[k].num;
	let itemtotalnum = slotitemowners[k].totalnum;
	let itemmenutitle;
	let itemval = 'slotitem' + k;
	let menu_added = false;

	if (itemok && !itemok[itemtype])
	    continue;

	if (!itemtypename)
	    itemtypename = 'UNKNOWN_' + itemtype;

	/*
	if (!itemname)
	    itemname = KanColleDatabase.masterSlotitem.get(k).api_name;
	*/
	//debugprint(itemname + ': slotitem' + k);

	if (!submenu || lastitemtype != itemtypename) {
	    submenu = {
		label: itemtypename,
		menu: [],
	    };
	    menu.push(submenu);
	    lastitemtype = itemtypename;
	}

	for (let ki = 0; ki < levelkeys.length; ki++) {
	    let levelkey = levelkeys[ki].key;
	    let levellabel = levelkeys[ki].label;
	    let subsubmenu;
	    if (!slotitemowners[k].lv[levelkey] ||
		slotitemowners[k].lv[levelkey].length <= 1) {
		continue;
	    }
	    subsubmenu = [{
				label: itemname + '(' + itemnum + '/' + itemtotalnum + ')',
				spec: itemval,
			      }];
	    for (let j = 0; j < slotitemowners[k].lv[levelkey].length; j++) {
		let itemlvnum;
		let itemlvtotalnum;
		if (!slotitemowners[k].lv[levelkey][j])
		    continue;
		itemlvnum  = slotitemowners[k].lv[levelkey][j].num;
		itemlvtotalnum = slotitemowners[k].lv[levelkey][j].totalnum;
		subsubmenu.push({
				    label: itemname +
					   '[' + levellabel + j + ']' +
					   '(' + itemlvnum + '/' + itemlvtotalnum + ')',
				    spec: itemval + ':' + levelkey + '=' + j,
				});
	    }
	    submenu.menu.push({
				label: itemname + '(' + itemnum + '/' + itemtotalnum + ')',
				menu: subsubmenu,
			      });
	    menu_added = true;
	}
	if (!menu_added) {
	    submenu.menu.push({
				label: itemname + '(' + itemnum + '/' + itemtotalnum + ')',
				spec: itemval,
			      });
	}
    }

    return menu;
}

var KanColleShipFilter = [
    {
	name: 'stype',
	get_menu_template: function() {
	    return __KanColleStypeFilterTemplate(function(types) {
						    return 'stype' + types.join('-');
						 });
	},
	regexp: /^stype((\d+-)*\d+)$/,
	get_filter: function(match) {
	    let stypesearch = '-' + match[1] + '-';
	    return function(e) {
		let shiptype;
		let ship = KanColleDatabase.ship.get(e);
		if (!ship)
		    return false;
		shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
		if (!shiptype)
		    return false;
		if (stypesearch.indexOf('-' + shiptype.api_stype + '-') != -1)
		    return true;
		return false;
	    };
	},
    },{
	name: 'slotitem',
	get_menu_template: function() {
	    let submenu = [];
	    submenu.push(__KanColleStypeFilterTemplate(function(types) {
		let hash = types.map(function(e) {
		    let stype = KanColleDatabase.masterStype.get(e);
		    return stype ? stype.api_equip_type : {};
		}).reduce(function(p,c) {
		    for (let k in c) {
			p[k] = p[k] || c[k];
		    }
		    return p;
		}, {});
		//debugprint(hash.toSource());
		return __KanColleSlotitemFilterTemplate(hash);
	    }));
	    submenu.unshift({
		label: '全て',
		menu: __KanColleSlotitemFilterTemplate(null),
	    });
	    return {
		label: '装備',
		menu: submenu,
	    };
	},
	regexp: /^slotitem(\d+)(\:([^=]+)=(\d+))?$/,
	get_filter: function(match) {
	    let slotitemid = match[1];
	    let slotitemkey = match[3];
	    let slotitemlv = match[4];
	    let slotitemowners = KanColleDatabase.slotitem.get(null,'owner');
	    let owners = slotitemowners[slotitemid];
	    let shiphash = Object.keys(owners ? ((slotitemlv === undefined || slotitemlv == '') ? owners.list : (owners.lv[slotitemkey][slotitemlv] ? owners.lv[slotitemkey][slotitemlv].list : {})) : {}).reduce(function(p,c) {
		p[c] = true;
		return p;
	    }, {});
	    return function(e) {
		return shiphash[e];
	    };
	},
    },{
	name: 'upgrade',
	_menu_template: {
	    label: '近代化',
	    menu: [
		{
		    label: '改修可能',
		    spec: 'upgrade0',
		},{
		    label: '最高改造段階で改修可能',
		    spec: 'upgrade1',
		},
		{
		    label: '運改修可能',
		    spec: 'upgrade2',
		},{
		    label: '最高改造段階で運改修可能',
		    spec: 'upgrade3',
		},
	    ],
	},
	get_menu_template: function() {
	    return this._menu_template;
	},
	regexp: /^upgrade(\d+)$/,
	get_filter: function(match) {
	    const proplists = [ [ 'karyoku', 'raisou', 'taiku', 'soukou' ],
				[ 'lucky' ] ];
	    let param = parseInt(match[1], 10);
	    let upgrade = (param & 1) ? 1 : 0;
	    let proplist = proplists[(param & 2) ? 1 : 0];
	    return function(e) {
		let ship = KanColleDatabase.ship.get(e);
		let shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
		if (upgrade && shiptype.api_afterlv)
		    return false;
		for (j = 0; j < proplist.length; j++) {
		    k = proplist[j];
		    let p = getShipProperties(ship,k);
		    if (p && p.remain) {
			return true;
		    }
		}
		return false;
	    };
	},
    },{
	name: 'evolution',
	_menu_template: {
	    label: '改造',
	    menu: [
		{
		    label: '非最高改造段階',
		    spec: 'evolution0',
		},{
		    label: '保護下で非最高改造段階',
		    spec: 'evolution1',
		},
	    ],
	},
	get_menu_template: function() {
	    return this._menu_template;
	},
	regexp: /^evolution(\d+)$/,
	get_filter: function(match) {
	    let locked = parseInt(match[1], 10);
	    return function(e) {
		let ship = KanColleDatabase.ship.get(e);
		let shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
		if (!shiptype.api_afterlv)
		    return false;
		if (locked && !ship.api_locked)
		    return false;
		return true;
	    };
	},
    },{
	name: 'misc',
	_menu_template: {
	    label: '状態',
	    menu: [
		{
		    label: '非保護艦',
		    spec: 'locked0',
		},{
		    label: '保護艦',
		    spec: 'locked1',
		},{
		    label: 'キラ無',
		    spec: 'cond0',
		},{
		    label: 'キラ有',
		    spec: 'cond1',
		},
	    ],
	},
	get_menu_template: function() {
	    return this._menu_template;
	},
	regexp: /^(locked|cond)(\d+)$/,
	get_filter: function(match) {
	    if (match[1] == 'locked') {
		let locked = parseInt(match[2], 10);
		//debugprint('locked:' + locked);
		return function(e) {
		    let ship = KanColleDatabase.ship.get(e);
		    //debugprint('ship.api_locked:' + ship.api_locked);
		    if (locked != ship.api_locked)
			return false;
		    return true;
		};
	    } else if (match[1] == 'cond') {
		let cond = parseInt(match[2], 10);
		//debugprint('cond:' + cond);
		return function(e) {
		    let ship = KanColleDatabase.ship.get(e);
		    if (cond == 1 && ship.api_cond < 50)
			return false;
		    if (cond == 0 && ship.api_cond >= 50)
			return false;
		    return true;
		};
	    }
	},
    },{
	name: 'sallyarea',
	get_menu_template: function() {
	    let menu = {
		label: '出撃',
		menu: []
	    };

	    for (let i = 1; i < (1 << (4 + 1)) - 1; i++) {
		let b = i.toString(2);
		let label, spec;
		let bits = b.split("").map(function(val, idx) {
		    return {
			idx: idx,
			val: val,
		    }
		}).filter(function(val) {
		    return val.val != 0;
		}).map(function(val) {
		    return b.length - val.idx - 1;
		});
		menu.menu.push({
		    label: '海域' + bits.join('+'),
		    spec: 'sallyarea' + bits.join('-'),
		});
	    }
	    return menu;
	},
	regexp: /^sallyarea((\d+-)*\d+)$/,
	get_filter: function(match) {
	    let sareasearch = '-' + match[1] + '-';
	    return function(e) {
		let sarea;
		let ship = KanColleDatabase.ship.get(e);
		if (!ship)
		    return false;
		sarea = ship.api_sally_area;
		if (!sarea)
		    sarea = 0;
		if (sareasearch.indexOf('-' + sarea + '-') != -1)
		    return true;
		return false;
	    };
	},
    },
];

function KanColleBuildFilterMenuList(id){
    let menulist;
    let menupopup;
    let menu;
    let menuitems = [];
    var defaultmenu = null;

    function buildmenuitem(label, value){
	let item = document.createElementNS(XUL_NS, 'menuitem');
	item.setAttribute('label', label);
	if (value)
	    item.setAttribute('value', value);
	item.addEventListener('command', function() { ShipListFilter(this); }, true);
	return item;
    }

    function createmenu(templ) {
	let popup;
	let menu;
	let mlist;

	if (!templ)
	    return;

	//debugprint(templ.toSource());

	if (templ.spec) {
	    if (ShipInfoTree.shipfilterspec == templ.spec)
		defaultmenu = buildmenuitem(templ.label, templ.spec);
	    return buildmenuitem(templ.label, templ.spec);
	}

	mlist = [];
	popup = document.createElementNS(XUL_NS, 'menupopup');
	menu = document.createElementNS(XUL_NS, 'menu');

	menu.setAttribute('label', templ.label);
	menu.appendChild(popup);

	if (templ.menu) {
	    for (let i = 0; i < templ.menu.length; i++)
		mlist.push(createmenu(templ.menu[i]));
	}
	for( let i = 0; i < mlist.length; i++ )
	    popup.appendChild(mlist[i]);

	if (!mlist.length)
	    menu.setAttribute('disabled', 'true');

	return menu;
    }

    menulist = document.createElementNS(XUL_NS, 'menulist');
    menulist.setAttribute('label', 'XXX');
    menulist.setAttribute('id', id);

    menupopup = document.createElementNS(XUL_NS, 'menupopup');
    menupopup.setAttribute('id', id + '-popup');

    // Default
    menu = buildmenuitem('すべて', null);
    menuitems.push(menu);

    // Build menus
    KanColleShipFilter.forEach(function(e) {
	menu = createmenu(e.get_menu_template());
	if (menu)
	    menuitems.push(menu);
    });

    // Finally build menu
    if (defaultmenu)
	menupopup.appendChild(defaultmenu);
    for (let i = 0; i < menuitems.length; i++)
	menupopup.appendChild(menuitems[i]);

    menulist.appendChild(menupopup);

    return menulist;
}

function ShipListFilter(item){
    let itemval = item ? item.value : null;

    debugprint('ShipListFilter(' + itemval + ')');

    if (itemval)
	ShipInfoTree.shipfilterspec = itemval;
    else
	ShipInfoTree.shipfilterspec = null;

    $('shipinfo-filtermenu').setAttribute('label', item.getAttribute('label'));

    KanColleShipInfoSetView();
}

function KanColleCreateFilterMenuList(box,id)
{
    let oldmenulist = $(id);
    let menulist = KanColleBuildFilterMenuList(id);
    let hbox;

    // Replace existing one or add new one.
    if (oldmenulist) {
	hbox = oldmenulist.parentNode;
	hbox.replaceChild(menulist, oldmenulist);
    }else {
	hbox = CreateElement('hbox');
	hbox.appendChild(menulist);
	box.appendChild(hbox);
    }
}

function KanColleSortMenuPopup(that){
    let value = that.value;
    debugprint('KanColleSortMenuPopup(' + value + ')');

    if (value.match(/:/)) {
	let key = RegExp.leftContext;
	let order = RegExp.rightContext;
	let spec = null;

	if (key.match(/@/)) {
	    spec = RegExp.leftContext;
	    key = RegExp.rightContext;
	} else
	    spec = key;

	if (ShipInfoTree.sortkey != key ||
	    ShipInfoTree.sortspec != spec ||
	    ShipInfoTree.sortorder != order) {
	    ShipInfoTree.sortold = ShipInfoTree._sortold;
	} else
	    ShipInfoTree.sortold = {};

	ShipInfoTree.sortkey = key;
	ShipInfoTree.sortspec = spec;
	ShipInfoTree.sortorder = order;

	ShipInfoTreeSort();
    }
}

function KanColleBuildSortMenuPopup(id,key){
    let menupopup;
    let idx;
    let colinfo;
    let sortspecs;

    menupopup  = document.createElementNS(XUL_NS, 'menupopup');
    menupopup.setAttribute('id', id);
    menupopup.setAttribute('position', 'overlap');

    idx = ShipInfoTree.collisthash[key];
    colinfo = ShipInfoTree.COLLIST[idx];
    sortspecs = colinfo.sortspecs;
    if (!sortspecs)
	sortspecs = [{ sortspec: colinfo.id, label: colinfo.label, }];

    for (let i = 0; i < sortspecs.length; i++) {
	let ad = [ { val:  1, label: '昇順', },
		   { val: -1, label: '降順', },
	];
	for (let j = 0; j < ad.length; j++) {
	    let menuitem = document.createElementNS(XUL_NS, 'menuitem');

	    //debugprint('key=' + key + ', spec = ' + sortspecs[i].sortspec);

	    menuitem.setAttribute('type', 'radio');
	    menuitem.setAttribute('name', id);
	    menuitem.setAttribute('label', sortspecs[i].label + ad[j].label);
	    menuitem.setAttribute('value', sortspecs[i].sortspec + '@' + key + ':' + ad[j].val);
	    menuitem.addEventListener('command', function() { KanColleSortMenuPopup(this); }, true);
	    menupopup.appendChild(menuitem);
	}
    }

    return menupopup;
}

function KanColleCreateSortMenuPopup(box,id,key)
{
    let oldmenupopup = $(id);
    let menupopup = KanColleBuildSortMenuPopup(id,key);

    // Replace existing one or add new one.
    if (oldmenupopup)
	box.replaceChild(menupopup, oldmenupopup);
    else
	box.appendChild(menupopup);
}

function KanColleCreateShipTree(){
    let tree;
    let oldtree;
    let treecols;
    let treechildren;
    let box;

    //debugprint('KanColleCreateShipTree()');

    // outer box
    box = $('shipinfo-box');

    // Setup hash
    ShipInfoTree.collisthash = {};
    for (let i = 0; i < ShipInfoTree.COLLIST.length; i++)
	ShipInfoTree.collisthash[ShipInfoTree.COLLIST[i].id] = i;

    // Build filter menu popup
    KanColleCreateFilterMenuList(box,'shipinfo-filtermenu');

    // Build sort menu
    for (let i = 0; i < ShipInfoTree.COLLIST.length; i++) {
	let key = ShipInfoTree.COLLIST[i].id;
	KanColleCreateSortMenuPopup(box, 'shipinfo-sortmenu-' + key, key);
    }

    // Treecols
    treecols = document.createElementNS(XUL_NS, 'treecols');
    treecols.setAttribute('context', 'shipinfo-colmenu');
    treecols.setAttribute('id', 'shipinfo-tree-columns');

    // Check selected items and build menu
    for (let i = 0; i < ShipInfoTree.COLLIST.length; i++) {
	let treecol;
	let colinfo = ShipInfoTree.COLLIST[i];
	let node = $('shipinfo-colmenu-' + colinfo.id);
	let ischecked = node &&
			node.hasAttribute('checked') &&
			node.getAttribute('checked') == 'true';

	treecol = document.createElementNS(XUL_NS, 'treecol');
	treecol.setAttribute('id', 'shipinfo-tree-column-' + colinfo.id);
	treecol.setAttribute('label', colinfo.label);
	if (colinfo.flex)
	    treecol.setAttribute('flex', colinfo.flex);
	treecol.setAttribute('popup', 'shipinfo-sortmenu-' + colinfo.id);
	treecol.setAttribute('class', 'sortDirectionIndicator');
	if (ShipInfoTree.sortkey && colinfo.id == ShipInfoTree.sortkey) {
	    treecol.setAttribute('sortDirection',
				 ShipInfoTree.sortorder > 0 ? 'ascending' : 'descending');
	}
	treecol.setAttribute('hidden', ischecked ? 'false' : 'true');

	if (i) {
	    let splitter = document.createElementNS(XUL_NS, 'splitter');
	    splitter.setAttribute('class', 'tree-splitter');
	    treecols.appendChild(splitter);
	}
	treecols.appendChild(treecol);
    }

    // Treechildren
    treechildren = document.createElementNS(XUL_NS, 'treechildren');
    treechildren.setAttribute('id', 'shipinfo-tree-children');

    // Build tree
    tree = document.createElementNS(XUL_NS, 'tree');
    tree.setAttribute('flex', '1');
    tree.setAttribute('hidecolumnpicker', 'true');
    tree.setAttribute('id', 'shipinfo-tree');

    tree.appendChild(treecols);
    tree.appendChild(treechildren);

    // Replace existing tree, or append one.
    oldtree = $('shipinfo-tree');
    if (oldtree)
	box.replaceChild(tree, oldtree);
    else
	box.appendChild(tree);
}

function ShipInfoTreeSort(){
    let order;
    let id;
    let key;
    let dir;

    debugprint('ShipInfoSort()');

    dir = ShipInfoTree.sortorder > 0 ? 'ascending' : 'descending';

    for (i = 0; i < ShipInfoTree.COLLIST.length; i++) {
	let colid = ShipInfoTree.COLLIST[i].id;
	if (colid == ShipInfoTree.sortkey)
	    $('shipinfo-tree-column-' + colid).setAttribute('sortDirection', dir);
	else
	    $('shipinfo-tree-column-' + colid).removeAttribute('sortDirection');
    }

    //debugprint('key=' + ShipInfoTree.sortkey + ', order=' + ShipInfoTree.sortorder);

    KanColleShipInfoSetView();
}

function getShipProperties(ship,name)
{
    const propmap = {
	karyoku: { kidx: 0, master: 'houg', },
	raisou:  { kidx: 1, master: 'raig', },
	taiku:   { kidx: 2, master: 'tyku', },
	soukou:  { kidx: 3, master: 'souk', },
	lucky:   { kidx: 4, master: 'luck', },
    };
    let prop = {
	cur: null,
	max: null,
	str: null,
	remain: null,
    };
    let shiptype;
    shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
    prop.cur = ship['api_' + name][0];
    prop.max = prop.cur - ship.api_kyouka[propmap[name].kidx] +
	       shiptype['api_' + propmap[name].master][1] -
	       shiptype['api_' + propmap[name].master][0];
    prop.str = prop.cur + '/' + prop.max;
    prop.remain = prop.max - prop.cur;
    return prop;
}

function getShipSlotitem(ship,slot){
    let idx = slot - 1;
    let name;

    if (idx >= ship.api_slotnum)
	return '';

    if (ship.api_slot[idx] < 0)
	return '-';

    item = KanColleDatabase.slotitem.get(ship.api_slot[idx]);
    if (!item)
	return '!';

    if (item.api_name)
	name = item.api_name;
    else {
	let itemtype = KanColleDatabase.masterSlotitem.get(item.api_slotitem_id);
	if (!itemtype)
	    return '?';
	name = itemtype.api_name;
    }
    return name;
}

function DefaultSortFunc(ship_a,ship_b,order){
    let shiptype_a = KanColleDatabase.masterShip.get(ship_a.api_ship_id);
    let shiptype_b = KanColleDatabase.masterShip.get(ship_b.api_ship_id);
    let ret;
    if (shiptype_a === undefined || shiptype_b === undefined)
	return ((shiptype_a !== undefined ? 1 : 0) - (shiptype_b !== undefined ? 1 : 0)) * order;
    ret = shiptype_a.api_stype - shiptype_b.api_stype;
    if (ret)
	return ret;
    //ゲーム内ソートは艦種と艦船の順位づけが逆
    return ship_b.api_sortno - ship_a.api_sortno;
}

function ShipExp(ship){
    let ship_exp;
    if (typeof(ship.api_exp) == 'object') {
	// 2013/12/11よりAPI変更
	// 0: 現在経験値
	// 1: 次Lvまでの必要経験値
	// 2: 現在Lvでの獲得経験値(%)
	return ship.api_exp[0];
    } else
	return ship.api_exp;
}

function ShipNextLvExp(ship){
    if (typeof(ship.api_exp) == 'object') {
	// Lv99: [1000000,0,0]
	return ship.api_exp[1] > 0 ? ship.api_exp[0] + ship.api_exp[1] : Number.POSITIVE_INFINITY;
    } else {
	let nextexp = KanColleData.level_accumexp[ship.api_lv];
	if (nextexp === undefined)
	    return undefined;
	else if (nextexp < 0)
	    return Number.POSITIVE_INFINITY;
	return nextexp;
    }
}

function ShipUpgradeableExp(ship){
    let shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
    let nextlv = shiptype ? shiptype.api_afterlv : 0;
    let nextexp;
    if (nextlv > 0) {
	nextexp = KanColleData.level_accumexp[nextlv - 1];
	if (nextexp === undefined || nextexp < 0)
	    return undefined;
    } else
	nextexp = Number.POSITIVE_INFINITY;
    return nextexp;
}

function TreeView(){
    var that = this;
    var shiplist;

    key = null;
    spec = null;
    order = null;
    let id;

    var key = ShipInfoTree.sortkey;
    var spec = ShipInfoTree.sortspec;
    var order = ShipInfoTree.sortorder;

    // getCellText function table by column ID
    var shipcellfunc = {
	fleet: function(ship) {
	    let fleet = KanColleDatabase.deck.lookup(ship.api_id);
	    if (fleet)
		return fleet.fleet;
	    return '';
	},
	id: function(ship) {
	    return ship.api_id;
	},
	stype: function(ship) {
	    let shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
	    if (!shiptype)
		return -1;
	    return KanColleDatabase.masterStype.get(shiptype.api_stype).api_name;
	},
	name: function(ship) {
	    return FindShipNameByCatId(ship.api_ship_id);
	},
	//_sortno: function(ship) {
	//    return ship.api_sortno;
	//},
	_yomi: function(ship) {
	    let shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id)
	    if (!shiptype)
		return 0;
	    return shiptype.api_yomi;
	},
	lv: function(ship) {
	    let shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
	    let nextlv = shiptype ? shiptype.api_afterlv : 0;
	    if (!nextlv)
		nextlv = '-';
	    return ship.api_lv + '/' + nextlv;
	},
	_lv: function(ship) {
	    return ship.api_lv;
	},
	_lvupg: function(ship) {
	    let shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
	    let nextlv = shiptype ? shiptype.api_afterlv : 0;
	    if (!nextlv)
		nextlv = '';
	    return nextlv;
	},
	_lvupgremain: function(ship) {
	    let shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
	    let nextlv = shiptype ? shiptype.api_afterlv : 0;
	    if (!nextlv)
		return '';
	    return nextlv - ship.api_lv;
	},
	exp: function(ship) {
	    let nextlvexp = ShipNextLvExp(ship);
	    let nextupgexp = ShipUpgradeableExp(ship);
	    let ship_exp = ShipExp(ship);

	    if (nextlvexp === undefined)
		nextlvexp = '?';
	    else if (nextlvexp == Number.POSITIVE_INFINITY)
		nextlvexp = '-';

	    if (nextupgexp === undefined)
		nextupgexp = '?';
	    else if (nextupgexp === Number.POSITIVE_INFINITY)
		nextupgexp = '-';

	    return ship_exp + '/' + nextlvexp + '/' + nextupgexp;
	},
	_exp: function(ship) {
	    return ShipExp(ship);
	},
	_expnext: function(ship) {
	    let expnext = ShipNextLvExp(ship);
	    if (expnext != Number.POSITIVE_INFINITY)
		return expnext;
	    return '';
	},
	_expnextremain: function(ship) {
	    let expnextremain = ShipNextLvExp(ship) - ShipExp(ship);
	    if (expnextremain != Number.POSITIVE_INFINITY)
		return expnextremain;
	    return '';
	},
	_expupg: function(ship) {
	    let expnext = ShipUpgradeableExp(ship);
	    if (expnext != Number.POSITIVE_INFINITY)
		return expnext;
	    return '';
	},
	_expupgremain: function(ship) {
	    let expnextremain = ShipUpgradeableExp(ship) - ShipExp(ship);
	    if (expnextremain != Number.POSITIVE_INFINITY)
		return expnextremain;
	    return '';
	},
	hp: function(ship) {
	    let info = FindShipStatus(ship.api_id);
	    return info ? info.nowhp + '/' + info.maxhp : '';
	},
	_hp: function(ship) {
	    let info = FindShipStatus(ship.api_id);
	    return info ? info.nowhp : '';
	},
	_maxhp: function(ship) {
	    let info = FindShipStatus(ship.api_id);
	    return info ? info.maxhp : '';
	},
	karyoku:	function(ship) { return getShipProperties(ship,'karyoku').str; },
	_karyoku:	function(ship) { return getShipProperties(ship,'karyoku').cur; },
	_karyokumax:	function(ship) { return getShipProperties(ship,'karyoku').max; },
	_karyokuremain:	function(ship) { return getShipProperties(ship,'karyoku').remain; },
	raisou:		function(ship) { return getShipProperties(ship,'raisou').str; },
	_raisou:	function(ship) { return getShipProperties(ship,'raisou').cur; },
	_raisoumax:	function(ship) { return getShipProperties(ship,'raisou').max; },
	_raisouremain:	function(ship) { return getShipProperties(ship,'raisou').remain; },
	taiku:		function(ship) { return getShipProperties(ship,'taiku').str; },
	_taiku:		function(ship) { return getShipProperties(ship,'taiku').cur; },
	_taikumax:	function(ship) { return getShipProperties(ship,'taiku').max; },
	_taikuremain:	function(ship) { return getShipProperties(ship,'taiku').remain; },
	soukou:		function(ship) { return getShipProperties(ship,'soukou').str; },
	_soukou:	function(ship) { return getShipProperties(ship,'soukou').cur; },
	_soukoumax:	function(ship) { return getShipProperties(ship,'soukou').max; },
	_soukouremain:	function(ship) { return getShipProperties(ship,'soukou').remain; },
	kaihi: function(ship) { return ship.api_kaihi[0]; },
	taisen: function(ship) { return ship.api_taisen[0]; },
	sakuteki: function(ship) { return ship.api_sakuteki[0]; },
	soku: function(ship) { return KanColleDatabase.masterShip.get(ship.api_ship_id).api_soku; },
	length: function(ship) { return ship.api_leng; },
	lucky:		function(ship) { return getShipProperties(ship,'lucky').str; },
	_lucky:		function(ship) { return getShipProperties(ship,'lucky').cur; },
	_luckymax:	function(ship) { return getShipProperties(ship,'lucky').max; },
	_luckyremain:	function(ship) { return getShipProperties(ship,'lucky').remain; },
	ndock: function(ship) {
	    let ndocktime = ship.api_ndock_time;
	    let hour;
	    let min;
	    if (!ndocktime)
		return '-';
	    min = Math.floor(ndocktime / 60000);
	    hour = Math.floor(min / 60);
	    min -= hour * 60;
	    if (min < 10)
		min = '0' + min;
	    return hour + ':' + min;
	},
	_ndock: function(ship) {
	    return ship.api_ndock_time;
	},
	cond: function(ship) {
	    return FindShipCond(ship.api_id);
	},
	fuel: function(ship) {
	    let shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
	    let maxfuel = shiptype ? shiptype.api_fuel_max : 0;
	    if (!maxfuel)
		maxfuel = '-';
	    return ship.api_fuel + '/' + maxfuel;
	},
	_fuel: function(ship) {
	    return ship.api_fuel;
	},
	_fuelmax: function(ship) {
	    let shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
	    let maxfuel = shiptype ? shiptype.api_fuel_max : 0;
	    if (!maxfuel)
		maxfuel = '-';
	    return maxfuel;
	},
	_fuelremain: function(ship) {
	    let shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
	    let maxfuel = shiptype ? shiptype.api_fuel_max : 0;
	    if (!maxfuel)
		maxfuel = '-';
	    return maxfuel - ship.api_fuel;
	},
	bull: function(ship) {
	    let shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
	    let maxbull = shiptype ? shiptype.api_bull_max : 0;
	    if (!maxbull)
		maxbull = '-';
	    return ship.api_bull + '/' + maxbull;
	},
	_bull: function(ship) {
	    return ship.api_bull;
	},
	_bullmax: function(ship) {
	    let shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
	    let maxbull = shiptype ? shiptype.api_bull_max : 0;
	    if (!maxbull)
		maxbull = '-';
	    return maxbull;
	},
	_bullremain: function(ship) {
	    let shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
	    let maxbull = shiptype ? shiptype.api_bull_max : 0;
	    if (!maxbull)
		maxbull = '-';
	    return maxbull - ship.api_bull;
	},
	slotitem1: function(ship) {
	    return getShipSlotitem(ship,1);
	},
	slotitem2: function(ship) {
	    return getShipSlotitem(ship,2);
	},
	slotitem3: function(ship) {
	    return getShipSlotitem(ship,3);
	},
	slotitem4: function(ship) {
	    return getShipSlotitem(ship,4);
	},
    };

    var shippropfunc = {
	'karyoku': function(ship) {
	    let props = [];
	    if (getShipProperties(ship,'karyoku').remain == 0)
		props.push('shipinfo-tree-cell-karyoku-full');
	    return props.join(' ');
	},
	'raisou': function(ship) {
	    let props = [];
	    if (getShipProperties(ship,'raisou').remain == 0)
		props.push('shipinfo-tree-cell-raisou-full');
	    return props.join(' ');
	},
	'taiku': function(ship) {
	    let props = [];
	    if (getShipProperties(ship,'taiku').remain == 0)
		props.push('shipinfo-tree-cell-taiku-full');
	    return props.join(' ');
	},
	'soukou': function(ship) {
	    let props = [];
	    if (getShipProperties(ship,'soukou').remain == 0)
		props.push('shipinfo-tree-cell-soukou-full');
	    return props.join(' ');
	},
	'*': function(ship) {
	    let ship_info;
	    let props = [];

	    ship_info = FindShipStatus(ship.api_id);
	    if (ship_info && ship_info.nowhp >= ship_info.maxhp) {
		props.push('shipinfo-tree-row-hp-ok');
	    }
	    if (ship_info &&
		ship_info.bull >= ship_info.bull_max &&
		ship_info.fuel >= ship_info.fuel_max) {
		props.push('shipinfo-tree-row-fill-ok');
	    }

	    ship_cond_str = 'none';
	    if (ship.api_cond >= 50) {
		ship_cond_str = 'good';
	    } else if (ship.api_cond == 49) {
		ship_cond_str = 'normal';
	    } else if (ship.api_cond >= 40) {
		ship_cond_str = 'hidden';
	    } else if (ship.api_cond >= 30) {
		ship_cond_str = 'tired';
	    } else if (ship.api_cond >= 20) {
		ship_cond_str = 'orange';
	    } else if (ship.api_cond >= 0) {
		ship_cond_str = 'red';
	    }
	    props.push('shipinfo-tree-row-cond-' + ship_cond_str);

	    if (ship.api_sally_area && parseInt(ship.api_sally_area, 10) != Number.NaN && ship.api_sally_area > 0)
		props.push('shipinfo-tree-row-sallyarea-' + ship.api_sally_area);

	    return props.join(' ');
	},
	'+': function(ship) {
	    let props = [];

	    if (KanColleDatabase.ndock.find(ship.api_id))
		props.push('shipinfo-tree-row-ndock-repairing');

	    return props.join(' ');
	},
    };

    // Ship list
    shiplist = KanColleDatabase.ship.list().slice();

    //
    // Sort ship list
    //
    // default comparison function
    var objcmp = function(a,b) {
	if (a > b)
	    return 1;
	else if (a < b)
	    return -1;
	return 0;
    };

    // special comparision function: each function takes two 'ship's
    var shipcmpfunc = {
	fleet: function(ship_a,ship_b,defres){
	    let fleet_a = KanColleDatabase.deck.lookup(ship_a.api_id);
	    let fleet_b = KanColleDatabase.deck.lookup(ship_b.api_id);
	    let ret;
	    if (!fleet_a || !fleet_b)
		return ((fleet_b ? 1 : 0) - (fleet_a ? 1 : 0)) * order;
	    if (!fleet_a.fleet || !fleet_b.fleet)
		return ((fleet_b.fleet ? 1 : 0) - (fleet_a.fleet ? 1 : 0)) * order;
	    ret = fleet_a.fleet - fleet_b.fleet;
	    if (ret)
		return ret;
	    ret = (fleet_a.pos - fleet_b.pos) * order;
	    if (ret)
		return ret;
	    if (defres)
		return defres;
	    return DefaultSortFunc(ship_b,ship_a,order);
	},
	_stype: function(ship_a,ship_b,defres){
	    let shiptype_a = KanColleDatabase.masterShip.get(ship_a.api_ship_id);
	    let shiptype_b = KanColleDatabase.masterShip.get(ship_b.api_ship_id);
	    let ret;
	    if (shiptype_a === undefined || shiptype_b === undefined)
		return ((shiptype_a !== undefined ? 1 : 0) - (shiptype_b !== undefined ? 1 : 0)) * order;
	    ret = shiptype_a.api_stype - shiptype_b.api_stype;
	    if (ret)
		return ret;
	    if (defres)
		return defres;
	    return DefaultSortFunc(ship_a,ship_b,order);
	},
	_lv: function(ship_a,ship_b,defres){
	    let ret = ship_a.api_lv - ship_b.api_lv;
	    if (!ret)
		ret = defres;
	    if (!ret)
		ret = ship_b.api_sortno - ship_a.api_sortno;
	    return ret;
	},
	_lvupg: function(ship_a,ship_b,defres){
	    let shiptype_a = KanColleDatabase.masterShip.get(ship_a.api_ship_id);
	    let lv_a = shiptype_a ? shiptype_a.api_afterlv : 0;
	    let shiptype_b = KanColleDatabase.masterShip.get(ship_b.api_ship_id);
	    let lv_b = shiptype_b ? shiptype_b.api_afterlv : 0;
	    let ret;
	    if (!lv_a)
		lv_a = Number.POSITIVE_INFINITY;
	    if (!lv_b)
		lv_b = Number.POSITIVE_INFINITY;
	    if (lv_a == Number.POSITIVE_INFINITY &&
		lv_b == Number.POSITIVE_INFINITY)
		ret = 0;
	    else
		ret = lv_a - lv_b;
	    if (!ret)
		ret = defres;
	    return ret;
	},
	_lvupgremain: function(ship_a,ship_b,defres){
	    let shiptype_a = KanColleDatabase.masterShip.get(ship_a.api_ship_id);
	    let lv_a = shiptype_a ? shiptype_a.api_afterlv : 0;
	    let shiptype_b = KanColleDatabase.masterShip.get(ship_b.api_ship_id);
	    let lv_b = shiptype_b ? shiptype_b.api_afterlv : 0;
	    let ret;
	    if (!lv_a)
		lv_a = Number.POSITIVE_INFINITY;
	    if (!lv_b)
		lv_b = Number.POSITIVE_INFINITY;
	    if (lv_a == Number.POSITIVE_INFINITY &&
		lv_b == Number.POSITIVE_INFINITY)
		ret = 0;
	    else
		ret = (lv_a - ship_a.api_lv) - (lv_b - ship_b.api_lv);
	    if (!ret)
		ret = defres;
	    return ret;
	},
	_expnext: function(ship_a,ship_b,defres) {
	    let nextexp_a = ShipNextLvExp(ship_a);
	    let nextexp_b = ShipNextLvExp(ship_b);
	    let ret;
	    if (nextexp_a === undefined)
		nextexp_a = Number.POSITIVE_INFINITY;
	    if (nextexp_b === undefined)
		nextexp_b = Number.POSITIVE_INFINITY;
	    if (nextexp_a == Number.POSITIVE_INFINITY &&
	        nextexp_b == Number.POSITIVE_INFINITY)
		ret = 0;
	    else
		ret = nextexp_a - nextexp_b;
	    if (!ret)
		ret = defres;
	    return ret;
	},
	_expnextremain: function(ship_a,ship_b,defres) {
	    let nextexp_a = ShipNextLvExp(ship_a);
	    let nextexp_b = ShipNextLvExp(ship_b);
	    let ret;
	    if (nextexp_a === undefined)
		nextexp_a = Number.POSITIVE_INFINITY;
	    if (nextexp_b === undefined)
		nextexp_b = Number.POSITIVE_INFINITY;
	    if (nextexp_a == Number.POSITIVE_INFINITY &&
	        nextexp_b == Number.POSITIVE_INFINITY)
		ret = 0;
	    else
		ret = (nextexp_a - ShipExp(ship_a)) - (nextexp_b - ShipExp(ship_b));
	    if (!ret)
		ret = defres;
	    return ret;
	},
	_expupg: function(ship_a,ship_b,defres) {
	    let nextexp_a = ShipUpgradeableExp(ship_a);
	    let nextexp_b = ShipUpgradeableExp(ship_b);
	    let ret;
	    if (nextexp_a === undefined)
		nextexp_a = Number.POSITIVE_INFINITY;
	    if (nextexp_b === undefined)
		nextexp_b = Number.POSITIVE_INFINITY;
	    if (nextexp_a == Number.POSITIVE_INFINITY &&
	        nextexp_b == Number.POSITIVE_INFINITY)
		ret = 0;
	    else
		ret = nextexp_a - nextexp_b;
	    if (!ret)
		ret = defres;
	    return ret;
	},
	_expupgremain: function(ship_a,ship_b,defres) {
	    let nextexp_a = ShipUpgradeableExp(ship_a);
	    let nextexp_b = ShipUpgradeableExp(ship_b);
	    let ret;
	    if (nextexp_a === undefined)
		nextexp_a = Number.POSITIVE_INFINITY;
	    if (nextexp_b === undefined)
		nextexp_b = Number.POSITIVE_INFINITY;
	    if (nextexp_a == Number.POSITIVE_INFINITY &&
	        nextexp_b == Number.POSITIVE_INFINITY)
		ret = 0;
	    else
		ret = (nextexp_a -= ShipExp(ship_a)) - (nextexp_b -= ShipExp(ship_b));
	    if (!ret)
		ret = defres;
	    return ret;
	},
    };

    // default
    if (key === undefined)
	key = 'id';
    if (spec === undefined)
	spec = key;
    if (order === undefined)
	order = 1;

    shiplist = shiplist.sort(function(a, b) {
	let res = 0;

	let ship_a = KanColleDatabase.ship.get(a);
	let ship_b = KanColleDatabase.ship.get(b);
	let old_a = ShipInfoTree.sortold[a];
	let old_b = ShipInfoTree.sortold[b];
	let oldres;

	if (old_a === undefined || old_b === undefined)
	    oldres = (old_a !== undefined ? 1 : 0) - (old_b !== undefined ? 1 : 0);
	else
	    oldres = objcmp(old_a, old_b);
	oldres *= order;

	if (!ship_a || !ship_b)
	    res = (ship_a ? 1 : 0) - (ship_b ? 1 : 0);
	else if (shipcmpfunc[spec] !== undefined)
	    res = shipcmpfunc[spec](ship_a,ship_b,oldres);
	else if (shipcellfunc[spec] !== undefined) {
	    let va = shipcellfunc[spec](ship_a);
	    let vb = shipcellfunc[spec](ship_b);
	    res = objcmp(va,vb);
	}

	if (!res)
	    res = oldres;

	return res * order;
    });

    ShipInfoTree._sortold = shiplist.map(function(e,i) {
	return { idx: i, key: e, };
    }).reduce(function(p, c) {
	p[c.key] = c.idx;
	return p;
    }, {});

    if (ShipInfoTree.shipfilterspec) {
	let filterspec = ShipInfoTree.shipfilterspec;
	let filter = null;
	KanColleShipFilter.forEach(function(e) {
	    let m;
	    if (filter)
		return;
	    m = e.regexp.exec(filterspec);
	    if (!m)
		return;
	    filter = e.get_filter(m);
	});

	if (filter) {
	    shiplist = shiplist.filter(filter);
	} else {
	    debugprint('invalid filterspec "' + filterspec + '"; ignored');
	}
    }

    // our local interface
    this.saveShipList = function(){
	const nsIFilePicker = Components.interfaces.nsIFilePicker;
	let fp = Components.classes["@mozilla.org/filepicker;1"].createInstance(nsIFilePicker);
	let rv;
	let cos;

	fp.init(window, "艦船リストの保存...", MODE_SAVE);
	fp.defaultExtension = 'txt';
	fp.appendFilter("テキストCSV","*.csv; *.txt");
	fp.appendFilters(nsIFilePicker.filterText);
	fp.appendFilters(nsIFilePicker.filterAll);
	rv = fp.show();

	if (rv == nsIFilePicker.returnOK || rv == nsIFilePicker.returnReplace)
	    cos = KanColleTimerUtils.file.openWriter(fp.file, 0, 0);
	else
	    return;

	// ZERO-WIDTH NO-BREAK SPACE (used as BOM)
	// とある表計算ソフトでは、UTF-8な.csvファイルにはこれがないと
	// "文字化け"する。一方、.txtなら問題ない。
	//cos.writeString('\ufeff');
	for (let i = -1; i < shiplist.length; i++) {
	    let a = [];
	    let ship = KanColleDatabase.ship.get(shiplist[i]);
	    for (let j = 0; j < ShipInfoTree.COLLIST.length; j++){
		if (!ShipInfoTree.COLLIST[j].subdump) {
		    let val;
		    if (i < 0)
			val = ShipInfoTree.COLLIST[j].label;
		    else
			val = '' + shipcellfunc[ShipInfoTree.COLLIST[j].id](ship);
		    a.push(val.replace(/,/g,'_').replace(/"/g,'_'));
		}
		if (!ShipInfoTree.COLLIST[j].sortspecs)
		    continue;
		for (let k = 0; k < ShipInfoTree.COLLIST[j].sortspecs.length; k++) {
		    let val;
		    if (ShipInfoTree.COLLIST[j].sortspecs[k].skipdump)
			continue;
		    if (!shipcellfunc[ShipInfoTree.COLLIST[j].sortspecs[k].sortspec]) {
			a.push('<'+ShipInfoTree.COLLIST[j].sortspecs[k].sortspec+'>');
			continue;
		    }
		    if (i < 0)
			val = ShipInfoTree.COLLIST[j].sortspecs[k].label;
		    else
			val = '' + shipcellfunc[ShipInfoTree.COLLIST[j].sortspecs[k].sortspec](ship);
		    val = val.replace(/[\r\n]+/, ' ');
		    a.push(val.match(/[",]/) ? '"' + val.replace(/"/g,'""') + '"' : val);
		}
	    }
	    cos.writeString(a.join(',')+'\n');
	}
	cos.close();
    };

    //
    // the nsITreeView object interface
    //
    this.rowCount = shiplist.length;
    this.getCellText = function(row,column){
	let colid = column.id.replace(/^shipinfo-tree-column-/, '');
	let ship;
	let func;

	if (row >= this.rowCount)
	    return 'N/A';

	ship = KanColleDatabase.ship.get(shiplist[row]);
	if (!ship)
	    return 'N/A';

	func = shipcellfunc[colid];
	if (func)
	    ret = func(ship);
	else
	    ret = colid + '_' + row;
	return ret;
    };
    this.getColumnProperties = function(col) {};
    this.setTree = function(treebox){ this.treebox = treebox; };
    this.isContainer = function(row){ return false; };
    this.isSeparator = function(row){ return false; };
    this.isSorted = function(){ return false; };
    this.getLevel = function(row){ return 0; };
    this.getImageSrc = function(row,col){
	if( col.index != 3 ) return;
	let ship = KanColleDatabase.ship.get(shiplist[row]);
	if(!ship)
	    return;
	return KanColleDatabase.ndock.find(ship.api_id) ? "chrome://kancolletimer/content/data/cross.png" : "";
    };
    this.getRowProperties = function(row,props){
	let ship;
	let func;
	if (row >= this.rowCount)
	    return '';
	ship = KanColleDatabase.ship.get(shiplist[row]);
	if (!ship)
	    return;
	func = shippropfunc['*'];
	return func(ship);
    };
    this.getCellProperties = function(row,column,props){
	let colid = column.id.replace(/^shipinfo-tree-column-/, '');
	let ship;
	let func;
	let prop;

	if (row >= this.rowCount)
	    return prop;
	ship = KanColleDatabase.ship.get(shiplist[row]);
	if (!ship)
	    return column.id;

	func = shippropfunc['+'];
	prop = func(ship);

	func = shippropfunc[colid];
	if (func)
	    return column.id + ' ' + prop + ' ' + func(ship);
	return column.id + ' ' + prop;
    };
    this.getColumnProperties = function(col,props){};
    this.cycleHeader = function(col,elem){};
};

function KanColleShipInfoSetView(){
    let menu = $('saveshiplist-menu');
    //debugprint('KanColleShipInfoSetView()');
    ShipListView = new TreeView();
    if (menu)
	menu.setAttribute('disabled', 'false');
    $('shipinfo-tree').view = ShipListView;
}

function ShipInfoTreeMenuPopup(){
    //debugprint('ShipInfoTreeMenuPopup()');
    KanColleCreateShipTree();
    KanColleShipInfoSetView();
}

function KanColleTimerShipTableShow() {
    let toggle = $('shipinfo-toggle');
    if (toggle) {
	let checked = toggle.getAttribute('checked') == 'true';
	$('shipinfo-box').collapsed = !checked;
    }
}

var KanColleTimerShipTable = {
    update: {
	masterSlotitem: function() {
	    KanColleTimerShipInfoHandler();
	},
	ship: function() {
	    KanColleTimerShipInfoHandler();
	},
	slotitem: function() {
	    KanColleTimerShipInfoHandler();
	},
    },
    restore: function() {
	KanColleCreateShipTree();
	KanColleShipInfoSetView();
	KanColleTimerShipTableShow();
    },
};
KanColleTimerShipTable.__proto__ = __KanColleTimerPanel;

/*
 * とうらぶ
 */
var TouRabuTimerDeckInfo = {
    _update_party: function(party, summary, _now, t) {
	let now = TouRabuParseTime(_now);
	for (let i = 1; i <= 4; i++) {
	    let box = $('tourabu-mission' + i);
	    let slot = party[i];
	    if (box) {
		let finish = slot ? TouRabuParseTime(slot.finished_at) : Number.NaN;
		let diff = isNaN(finish) ? 0 : finish - now;
		debugprint('t = ' + t + ', now = ' + now + ', finish = ' + finish + '; diff = ' + diff);
		if (!isNaN(finish)) {
		    let goal = t + (finish - now);
		    KanColleRemainInfo.tourabu_fleet[i] = { finishedtime: goal };
		    $('tourabu-mission' + i + '-name').value = summary ? ('#' + summary[i].field_id) : '?';
		    $('tourabu-mission' + i + '-time').finishTime = goal;
		    $('tourabu-mission' + i + '-remain').finishTime = goal;
		} else {
		    KanColleRemainInfo.tourabu_fleet[i] = { finishedtime: Number.NaN };
		    $('tourabu-mission' + i + '-name').value = '';
		    $('tourabu-mission' + i + '-time').finishTime = '';
		    $('tourabu-mission' + i + '-remain').finishTime = '';
		}
	    }
	}
    },
    update: {
	tourabuConquest: function(_now) {
	    let t = KanColleDatabase.tourabuConquest.timestamp();
	    let data = KanColleDatabase.tourabuConquest.get();
	    this._update_party(data.party, data.summary, _now, t);
	},
	tourabuLoginStart: function(_now) {
	    let t = KanColleDatabase.tourabuLoginStart.timestamp();
	    let data = KanColleDatabase.tourabuLoginStart.get();
	    this.restore();
	    this._update_party(data.party, null, _now, t);
	},
    },
    restore: function() {
	let t = KanColleDatabase.tourabuLoginStart.timestamp();
	let d = KanColleDatabase.tourabuLoginStart.get();
	for (let i = 1; i <= 4; i++) {
	    let box = $('tourabu-mission' + i);
	    if (!box)
		continue;
	    if (t && i <= d.max_party)
		$('tourabu-mission' + i).collapsed = false;
	    else
		$('tourabu-mission' + i).collapsed = true;
	}
    },
};
TouRabuTimerDeckInfo.__proto__ = __KanColleTimerPanel;

var TouRabuTimerNdockInfo = {
    update: {
	tourabuRepair: function(_now) {
	    let swords = KanColleDatabase.tourabuSword.get();
	    let t = KanColleDatabase.tourabuRepair.timestamp();
	    let d = KanColleDatabase.tourabuRepair.get();
	    let now = TouRabuParseTime(_now);
	    for (let i = 1; i <= 4; i++) {
		let slot = d[i];
		let id = slot ? slot.sword_serial_id : null;
		if (true) {
		    let finish = slot ? TouRabuParseTime(slot.finished_at) : Number.NaN;
		    let diff = finish - now;
		    let sword = swords ? swords[id] : null;
		    let sword_name = TouRabuSwordName(id);
		    debugprint('t = ' + t + ', now = ' + now + ', finish = ' + finish + '; diff = ' + diff);
		    if (diff > 0) {
			let goal = t + (finish - now);
			KanColleRemainInfo.tourabu_ndock[i] = { finishedtime: goal };
			$('tourabu-ndock' + i + '-name').value = sword_name;
			$('tourabu-ndock' + i + '-time').finishTime = goal;
			$('tourabu-ndock' + i + '-remain').finishTime = goal;
		    } else {
			KanColleRemainInfo.tourabu_ndock[i] = { finishedtime: Number.NaN };
			$('tourabu-ndock' + i + '-name').value = '';
			$('tourabu-ndock' + i + '-time').finishTime = '';
			$('tourabu-ndock' + i + '-remain').finishTime = '';
		    }
		}
	    }
	},
	tourabuLoginStart: function() {
	    this.restore();
	},
    },
    restore: function() {
	let t = KanColleDatabase.tourabuLoginStart.timestamp();
	let d = KanColleDatabase.tourabuLoginStart.get();
	for (let i = 1; i <= 4; i++) {
	    if (t && i <= d.repair_slot)
		$('tourabu-ndock' + i).collapsed = false;
	    else
		$('tourabu-ndock' + i).collapsed = true;
	}
    },
};
TouRabuTimerNdockInfo.__proto__ = __KanColleTimerPanel;

var TouRabuTimerKdockInfo = {
    update: {
	tourabuForge: function(_now) {
	    let t = KanColleDatabase.tourabuForge.timestamp();
	    let d = KanColleDatabase.tourabuForge.get();
	    let now = TouRabuParseTime(_now);
	    for (let i = 1; i <= 4; i++) {
		let slot = d[i];
		if (true) {
		    let finish = slot ? TouRabuParseTime(slot.finished_at) : Number.NaN;
		    let diff = isNaN(finish) ? 0 : finish - now;
		    debugprint('t = ' + t + ', now = ' + now + ', finish = ' + finish + '; diff = ' + diff);
		    if (!isNaN(finish)) {
			let goal = t + diff;
			KanColleRemainInfo.tourabu_kdock[i] = { finishedtime: goal };
			$('tourabu-kdock' + i + '-name').value = '?';   // slot.sword_id;
			$('tourabu-kdock' + i + '-time').finishTime = goal;
			$('tourabu-kdock' + i + '-remain').finishTime = goal;
		    } else {
			KanColleRemainInfo.tourabu_kdock[i] = { finishedtime: Number.NaN };
			$('tourabu-kdock' + i + '-name').value = '';
			$('tourabu-kdock' + i + '-time').finishTime = '';
			$('tourabu-kdock' + i + '-remain').finishTime = '';
		    }
		}
	    }
	},
	tourabuLoginStart: function() {
	    this.restore();
	},
    },
    restore: function() {
	let t = KanColleDatabase.tourabuLoginStart.timestamp();
	let d = KanColleDatabase.tourabuLoginStart.get();
	for (let i = 1; i <= 4; i++) {
	    if (t && i <= d.forge_slot)
		$('tourabu-kdock' + i).collapsed = false;
	    else
		$('tourabu-kdock' + i).collapsed = true;
	}
    },
};
TouRabuTimerKdockInfo.__proto__ = __KanColleTimerPanel;

function TouRabuParseTime(s) {
    if (s && s.match(/^(\d+)-(\d+)-(\d+) (\d+):(\d+):(\d+)$/)) {
	let year = parseInt(RegExp.$1, 10),
	    month = parseInt(RegExp.$2, 10),
	    day = parseInt(RegExp.$3, 10),
	    hour = parseInt(RegExp.$4, 10),
	    min = parseInt(RegExp.$5, 10),
	    sec = parseInt(RegExp.$6, 10);
	let d = new Date;
	d.setUTCFullYear(year, month - 1, day);
	d.setUTCHours(hour);
	d.setUTCMinutes(min);
	d.setUTCSeconds(sec);
	return d.getTime() - 9 * 3600000;
    } else {
	return Number.NaN;
    }
}

function TouRabuSwordName(id) {
    let swords = KanColleDatabase.tourabuSword.get();
    let sword = swords ? swords[id] : null;
    let sword_name = null;
    if (sword) {
	let sword_id = sword.sword_id;
	let sword_data = sword_id ? TouRabuData.sword[sword_id] : null;
	sword_name = sword_data ? sword_data.name : null;
	if (!sword_name)
	    sword_name = '#' + sword_id;
    }
    if (!sword_name)
	sword_name = '?(' + id + ')';
    return sword_name;
}

function GetFleetNo(ship_id) {
    let fleet = KanColleDatabase.deck.lookup( ship_id );
    return fleet ? fleet.fleet : 0;
}

function SaveCheckPreference( elem ){
    let name = elem.getAttribute("prefname");
    let checked = elem.hasAttribute('checked');
    KanColleTimerConfig.setBool( name, checked );
}

/**
 * Style
 */
function SetStyleProperty(node, prop, value, prio){
    if (value === undefined || value === null)
	node.style.removeProperty(prop);
    else {
	if (prio === undefined || prio === null)
	    prio = '';
	node.style.setProperty(prop,value,prio);
    }
}

/**
 * 指定のURLを開く.
 * @param url URL
 * @param hasfocus 開いたタブがフォーカスを得るか
 */
function OpenDefaultBrowser(url, hasfocus){
    return KanColleTimerUtils.window.openTab(url, hasfocus);
}

/**
 * Windowの最前面表示設定をする.
 * @note Windows/Firefox 17以降でのみ有効
 * @param win Window
 * @param istop 最前面にするならtrue
 */
function WindowOnTop(win, istop){
    try{
	let baseWin = win.QueryInterface(Ci.nsIInterfaceRequestor)
	    .getInterface(Ci.nsIWebNavigation)
	    .QueryInterface(Ci.nsIDocShellTreeItem)
	    .treeOwner
	    .QueryInterface(Ci.nsIInterfaceRequestor)
	    .nsIBaseWindow;
	let nativeHandle = baseWin.nativeHandle;

	let lib = ctypes.open('user32.dll');

	let HWND_TOPMOST = -1;
	let HWND_NOTOPMOST = -2;
	let SWP_NOMOVE = 2;
	let SWP_NOSIZE = 1;

	/*
	 WINUSERAPI BOOL WINAPI SetWindowPos(
	 __in HWND hWnd,
	 __in_opt HWND hWndInsertAfter,
	 __in int X,
	 __in int Y,
	 __in int cx,
	 __in int cy,
	 __in UINT uFlags );
	 */
	let SetWindowPos = lib.declare("SetWindowPos",
				       ctypes.winapi_abi, // abi
				       ctypes.int32_t,     // return type
				       ctypes.int32_t,     // hWnd arg 1 HWNDはint32_tでOK
				       ctypes.int32_t,     // hWndInsertAfter
				       ctypes.int32_t,     // X
				       ctypes.int32_t,     // Y
				       ctypes.int32_t,     // cx
				       ctypes.int32_t,     // cy
				       ctypes.uint32_t);   // uFlags
	SetWindowPos( parseInt(nativeHandle), istop?HWND_TOPMOST:HWND_NOTOPMOST,
		      0, 0, 0, 0,
		      SWP_NOMOVE | SWP_NOSIZE);
	lib.close();
    } catch (x) {
    }
}

/**
 * サウンド再生をする.
 * @param path ファイルのパス
 */
function PlaySound( path ){
    try{
	//debugprint(path);
	let IOService = Cc['@mozilla.org/network/io-service;1'].getService(Ci.nsIIOService);
	let localFile = Cc['@mozilla.org/file/local;1'].createInstance(Ci.nsILocalFile);
	let sound = Cc["@mozilla.org/sound;1"].createInstance(Ci.nsISound);
	localFile.initWithPath( path );
	sound.play(IOService.newFileURI(localFile));
	//sound.playEventSound(0);
    } catch (x) {
    }
}


function $(tag){
    return document.getElementById(tag);
}

function $$(tag){
    return document.getElementsByTagName(tag);
}

/**
 * オブジェクトをマージする.
 * @param a オブジェクト1
 * @param b オブジェクト2
 * @param aにbをマージしたオブジェクトを返す
 */
function MergeSimpleObject(a,b)
{
    for(let k in b){
	a[k] = b[k];
    }
    return a;
}

/**
 * 配列をシャッフルする.
 * @param list 配列
 */
function ShuffleArray( list ){
    let i = list.length;
    while(i){
	let j = Math.floor(Math.random()*i);
	let t = list[--i];
	list[i] = list[j];
	list[j] = t;
    }
}

/**
 * ユーザーのProfileディレクトリを返す
 */
function GetProfileDir(){
    return KanColleTimerUtils.file.profileDir();
}

function GetAddonVersion()
{
    let version;
    try{
	let em = Components.classes["@mozilla.org/extensions/manager;1"].getService(Components.interfaces.nsIExtensionManager);
	let addon = em.getItemForID(ADDON_ID);
	version = addon.version;
    } catch (x) {
	// Fx4
	let addon = KanColleTimerUtils.addon.get(ADDON_ID);
	version = addon.version;
    }
    return version;
}

function GetXmlText(xml,path){
    try{
	let tmp = evaluateXPath(xml,path);
	if( tmp.length<=0 ) return null;
	return tmp[0].textContent;
    } catch (x) {
	debugprint(x);
	return null;
    }
}

// 特定の DOM ノードもしくは Document オブジェクト (aNode) に対して
// XPath 式 aExpression を評価し、その結果を配列として返す。
// 最初の作業を行った wanderingstan at morethanwarm dot mail dot com に感謝します。
function evaluateXPath(aNode, aExpr) {
    let xpe = new XPathEvaluator();
    let nsResolver = xpe.createNSResolver(aNode.ownerDocument == null ?
					  aNode.documentElement : aNode.ownerDocument.documentElement);
    let result = xpe.evaluate(aExpr, aNode, nsResolver, 0, null);
    let found = [];
    let res;
    while (res = result.iterateNext())
	found.push(res);
    return found;
}
function evaluateXPath2(aNode, aExpr) {
    let xpe = new XPathEvaluator();
    let nsResolver = function(){ return XUL_NS; };
    let result = xpe.evaluate(aExpr, aNode, nsResolver, 0, null);
    let found = [];
    let res;
    while (res = result.iterateNext())
	found.push(res);
    return found;
}

function CreateElement(part){
    let elem;
    elem = document.createElementNS(XUL_NS,part);
    return elem;
}
function CreateHTMLElement(part){
    let elem;
    elem = document.createElementNS(HTML_NS,part);
    return elem;
}

/**
 * 指定の要素を削除する.
 * @param elem 削除したい要素
 */
function RemoveElement(elem){
    if (elem) {
	elem.parentNode.removeChild(elem);
	return true;
    }
    return false;
}

/**
 * 指定の要素の子要素を全削除する.
 * @param elem 対象の要素
 */
function RemoveChildren(elem){
    while(elem.hasChildNodes()) {
	elem.removeChild(elem.childNodes[0]);
    }
}

function ClearListBox( list ){
    while( list.getRowCount() ){
	list.removeItemAt( 0 );
    }
}

function CreateMenuItem(label,value){
    let elem;
    elem = document.createElementNS(XUL_NS,'menuitem');
    elem.setAttribute('label',label);
    elem.setAttribute('value',value);
    return elem;
};

function CreateButton(label){
    let elem;
    elem = document.createElementNS(XUL_NS,'button');
    elem.setAttribute('label',label);
    return elem;
}

function ToggleCheckbox(event,func) {
    let target = event.originalTarget;
    let checked = target.getAttribute('checked') == 'true';
    target.checked = !checked;
    return func();
}

function CreateLabel(label){
    let elem;
    elem = document.createElementNS(XUL_NS,'label');
    elem.setAttribute('value',label);
    return elem;
}

function CreateListCell(label){
    let elem;
    elem = document.createElementNS(XUL_NS,'listcell');
    elem.setAttribute('label',label);
    return elem;
}

function GetInputStream( file ){
    return KanColleTimerUtils.file.openRawReader(file);
}

/** ディレクトリを作成する.
 * ディレクトリ掘ったらtrue、掘らなかったらfalseを返す.
 */
function CreateFolder(path){
    let file = KanColleTimerUtils.file.initWithPath(path);
    return KanColleTimerUtils.file.createDirectory(file, 0);
}

/**
 * ファイルを開く
 */
function OpenFile(path){
    return KanColleTimerUtils.file.initWithPath(path);
}

function CreateFile( file ){
    return KanColleTimerUtils.file.openRawWriter(file, 0, 0);
}

// NicoLiveHelperのインストールパスを返す.
function GetExtensionPath(){
    let ext;
    try{
	ext = Components.classes["@mozilla.org/extensions/manager;1"]
            .getService(Components.interfaces.nsIExtensionManager)
            .getInstallLocation(ADDON_ID)
            .getItemLocation(ADDON_ID);
    } catch (x) {
	let _addon = KanColleTimerUtils.addon.get(ADDON_ID);
	ext = _addon.getResourceURI('/').QueryInterface(Components.interfaces.nsIFileURL).file.clone();
    }
    return ext;
}

function PlayAlertSound(){
    let sound = Components.classes["@mozilla.org/sound;1"].createInstance(Components.interfaces.nsISound);
    sound.playSystemSound("_moz_alertdialog");
}

function AlertPrompt(text,caption){
    let prompts = Components.classes["@mozilla.org/embedcomp/prompt-service;1"].getService(Components.interfaces.nsIPromptService);
    let result = prompts.alert(window, caption, text);
    return result;
}

function ConfirmPrompt(text,caption){
    let prompts = Components.classes["@mozilla.org/embedcomp/prompt-service;1"].getService(Components.interfaces.nsIPromptService);
    let result = prompts.confirm(window, caption, text);
    return result;
}

function InputPrompt(text,caption,input){
    let check = {value: false};
    let input_ = {value: input};

    let prompts = Components.classes["@mozilla.org/embedcomp/prompt-service;1"].getService(Components.interfaces.nsIPromptService);
    let result = prompts.prompt(window, caption, text, input_, null, check);
    if( result ){
	return input_.value;
    }else{
	return null;
    }
}

function InputPromptWithCheck(text,caption,input,checktext){
    let check = {value: false};
    let input_ = {value: input};

    let prompts = Components.classes["@mozilla.org/embedcomp/prompt-service;1"].getService(Components.interfaces.nsIPromptService);
    let result = prompts.prompt(window, caption, text, input_, checktext, check);
    if( result ){
	return input_.value;
    }else{
	return null;
    }
}

/**
 * @return nsIFileを返す
 */
function OpenFileDialog( caption, mode )
{
    const nsIFilePicker = Components.interfaces.nsIFilePicker;
    let fp = Components.classes["@mozilla.org/filepicker;1"].createInstance(nsIFilePicker);
    fp.init( window, caption, mode );
    fp.appendFilters(nsIFilePicker.filterAll);
    let rv = fp.show();
    if (rv == nsIFilePicker.returnOK || rv == nsIFilePicker.returnReplace) {
	let file = fp.file;
	return file;
    }
    return null;
}

function SaveUrlToFile(url, file)
{
    return KanColleTimerUtils.file.saveURI(file, url);
}

/**
 * HTML canvasを nsIURL に変換して返す
 * @param canvas
 * @param format
 * @returns {nsIURL}
 */
function CanvasToURI( canvas, format ){
    format = format || "image/png";
    let url = canvas.toDataURL( format );
    const IO_SERVICE = Components.classes['@mozilla.org/network/io-service;1']
       .getService( Components.interfaces.nsIIOService );
    let newurl = IO_SERVICE.newURI( url, null, null );
    return newurl;
}

function DrawSVGToCanvas( svg ){
    var canvas = document.createElementNS( "http://www.w3.org/1999/xhtml", "canvas" );
    let rect = svg.getBoundingClientRect();
    let region = {
	win: window,
	x: rect.left,
	y: rect.top,
	w: rect.width,
	h: rect.height,
    };

    return KanColleTimerUtils.screenshot.drawCanvas(canvas, null, region, null);
}

/**
 *  Javascriptオブジェクトをファイルに保存する.
 * @param obj Javascriptオブジェクト
 * @param caption ファイル保存ダイアログに表示するキャプション
 */
function SaveObjectToFile(obj,caption)
{
    const nsIFilePicker = Components.interfaces.nsIFilePicker;
    let fp = Components.classes["@mozilla.org/filepicker;1"].createInstance(nsIFilePicker);
    fp.init(window, caption, MODE_SAVE);
    fp.appendFilters(nsIFilePicker.filterAll);
    let rv = fp.show();
    if (rv == nsIFilePicker.returnOK || rv == nsIFilePicker.returnReplace) {
	KanColleTimerUtils.file.writeObject(fp.file, obj);
    }
}

/**
 *  ファイルからJavascriptオブジェクトを読み込む.
 * @param caption ファイル読み込みダイアログに表示するキャプション
 */
function LoadObjectFromFile(caption)
{
    const nsIFilePicker = Components.interfaces.nsIFilePicker;
    let fp = Components.classes["@mozilla.org/filepicker;1"].createInstance(nsIFilePicker);
    fp.init(window, caption, nsIFilePicker.modeOpen);
    fp.appendFilters(nsIFilePicker.filterAll);
    let rv = fp.show();
    if (rv == nsIFilePicker.returnOK || rv == nsIFilePicker.returnReplace) {
	let obj = KanColleTimerUtils.file.readObject(fp.file);
	if (obj === undefined)
	    obj = null;
	return obj;
    }
    return null;
}


/**
 * 指定タグを持つ親要素を探す.
 * @param elem 検索の起点となる要素
 * @param tag 親要素で探したいタグ名
 */
function FindParentElement(elem,tag){
    //debugprint("Element:"+elem+" Tag:"+tag);
    while(elem.parentNode &&
	  (!elem.tagName || (elem.tagName.toUpperCase()!=tag.toUpperCase()))){
	elem = elem.parentNode;
    }
    return elem;
}

function GetSignedValue(v)
{
    return (!isNaN(v) && v > 0) ? '+' + v : v;
}

/**
 * クリップボードにテキストをコピーする.
 * @param str コピーする文字列
 */
function CopyToClipboard(str){
    if(str.length<=0) return;
    let gClipboardHelper = Components.classes["@mozilla.org/widget/clipboardhelper;1"].
	getService(Components.interfaces.nsIClipboardHelper);
    gClipboardHelper.copyString(str);
}

function htmlspecialchars(ch){
    ch = ch.replace(/&/g,"&amp;");
    ch = ch.replace(/"/g,"&quot;");
    //ch = ch.replace(/'/g,"&#039;");
    ch = ch.replace(/</g,"&lt;");
    ch = ch.replace(/>/g,"&gt;");
    return ch ;
}

function restorehtmlspecialchars(ch){
    ch = ch.replace(/&quot;/g,"\"");
    ch = ch.replace(/&amp;/g,"&");
    ch = ch.replace(/&lt;/g,"<");
    ch = ch.replace(/&gt;/g,">");
    ch = ch.replace(/&nbsp;/g," ");
    ch = ch.replace(/&apos;/g,"'");
    return ch;
}

function syslog(txt){
    let tmp = GetDateString( GetCurrentTime()*1000 );
    txt = tmp + " " +txt;
    if( $('syslog-textbox') )
	$('syslog-textbox').value += txt + "\n";
}

function debugprint(txt){
    /*
    if( $('debug-textbox') )
	$('debug-textbox').value += txt + "\n";
     */
    KanColleTimerUtils.console.log(txt);
}

function debugconsole(txt){
    KanColleTimerUtils.console.log(txt);
}

function debugalert(txt){
    AlertPrompt(txt,'');
}

function ShowPopupNotification(imageURL,title,text,cookie){
    KanColleTimerUtils.alert.show(imageURL, title, text, false, cookie, null);
}


function GetUTF8ConverterInputStream(istream)
{
    let cis = Components.classes["@mozilla.org/intl/converter-input-stream;1"].createInstance(Components.interfaces.nsIConverterInputStream);
    cis.init(istream,"UTF-8",0,Components.interfaces.nsIConverterInputStream.DEFAULT_REPLACEMENT_CHARACTER);
    return cis;
}

function GetUTF8ConverterOutputStream(os)
{
    let cos = Components.classes["@mozilla.org/intl/converter-output-stream;1"].createInstance(Components.interfaces.nsIConverterOutputStream);
    cos.init(os,"UTF-8",0,Components.interfaces.nsIConverterOutputStream.DEFAULT_REPLACEMENT_CHARACTER);
    return cos;
}


/**
 *  現在時刻を秒で返す(UNIX時間).
 */
function GetCurrentTime(){
    let d = new Date();
    return Math.floor(d.getTime()/1000);
}

function GetDateString(ms){
    let d = new Date(ms);
    return d.toLocaleFormat("%m-%d %H:%M:%S");
}

function GetFormattedDateString(format,ms){
    let d = new Date(ms);
    return d.toLocaleFormat(format);
}

// string bundleから文字列を読みこむ.
function LoadString(name){
    return $('string-bundle').getString(name);
}
function LoadFormattedString(name,array){
    return $('string-bundle').getFormattedString(name,array);
}

// hour:min:sec の文字列を返す.
function GetTimeString(sec){
    sec = Math.abs(sec);

    let hour = Math.floor( sec / 60 / 60 );
    let min = Math.floor( sec / 60 ) - hour*60;
    let s = sec % 60;

    let str = hour<10?"0"+hour:hour;
    str += ":";
    str += min<10?"0"+min:min;
    str += ":";
    str += s<10?"0"+s:s;
    return str;
}

// min以上、max以下の範囲で乱数を返す.
function GetRandomInt(min, max){
    return Math.floor(Math.random() * (max - min + 1)) + min;
}


// LCGの疑似乱数はランダム再生専用のため、他の用途では使用禁止.
var g_randomseed = GetCurrentTime();
function srand(seed)
{
    g_randomseed = seed;
}
function rand()
{
    g_randomseed = (g_randomseed * 214013 + 2531011) & 0x7fffffff;
    return g_randomseed;
}
// min以上、max以下の範囲で乱数を返す.
function GetRandomIntLCG(min,max)
{
    let tmp = rand() >> 4;
    return (tmp % (max-min+1)) + min;
}

function ZenToHan(str){
    return str.replace(/[ａ-ｚＡ-Ｚ０-９－（）＠]/g,
		       function(s){ return String.fromCharCode(s.charCodeAt(0)-65248); });
}

function HiraToKana(str){
    return str.replace(/[\u3041-\u3094]/g,
		      function(s){ return String.fromCharCode(s.charCodeAt(0)+0x60); });
}

/*
 *  convertKana JavaScript Library beta4
 *
 *  MIT-style license.
 *
 *  2007 Kazuma Nishihata [to-R]
 *  http://www.webcreativepark.net
 *
 * よりアルゴリズムを拝借.
 */
function HanToZenKana(str){
    let fullKana = new Array("ヴ","ガ","ギ","グ","ゲ","ゴ","ザ","ジ","ズ","ゼ","ゾ","ダ","ヂ","ヅ","デ","ド","バ","ビ","ブ","ベ","ボ","パ","ピ","プ","ペ","ポ","゛","。","「","」","、","・","ヲ","ァ","ィ","ゥ","ェ","ォ","ャ","ュ","ョ","ッ","ー","ア","イ","ウ","エ","オ","カ","キ","ク","ケ","コ","サ","シ","ス","セ","ソ","タ","チ","ツ","テ","ト","ナ","ニ","ヌ","ネ","ノ","ハ","ヒ","フ","ヘ","ホ","マ","ミ","ム","メ","モ","ヤ","ユ","ヨ","ラ","リ","ル","レ","ロ","ワ","ン","゜");
    let halfKana = new Array("ｳﾞ","ｶﾞ","ｷﾞ","ｸﾞ","ｹﾞ","ｺﾞ","ｻﾞ","ｼﾞ","ｽﾞ","ｾﾞ","ｿﾞ","ﾀﾞ","ﾁﾞ","ﾂﾞ","ﾃﾞ","ﾄﾞ","ﾊﾞ","ﾋﾞ","ﾌﾞ","ﾍﾞ","ﾎﾞ","ﾊﾟ","ﾋﾟ","ﾌﾟ","ﾍﾟ","ﾎﾟ","ﾞ","｡","｢","｣","､","･","ｦ","ｧ","ｨ","ｩ","ｪ","ｫ","ｬ","ｭ","ｮ","ｯ","ｰ","ｱ","ｲ","ｳ","ｴ","ｵ","ｶ","ｷ","ｸ","ｹ","ｺ","ｻ","ｼ","ｽ","ｾ","ｿ","ﾀ","ﾁ","ﾂ","ﾃ","ﾄ","ﾅ","ﾆ","ﾇ","ﾈ","ﾉ","ﾊ","ﾋ","ﾌ","ﾍ","ﾎ","ﾏ","ﾐ","ﾑ","ﾒ","ﾓ","ﾔ","ﾕ","ﾖ","ﾗ","ﾘ","ﾙ","ﾚ","ﾛ","ﾜ","ﾝ","ﾟ");
    for(let i = 0; i < 89; i++){
	let re = new RegExp(halfKana[i],"g");
	str=str.replace(re, fullKana[i]);
    }
    return str;
}

function FormatCommas(str){
    try{
	return str.toString().replace(/(\d)(?=(?:\d{3})+$)/g,"$1,");
    } catch (x) {
	return str;
    }
}

function clearTable(tbody)
{
   while(tbody.rows.length>0){
      tbody.deleteRow(0);
   }
}

function IsWINNT()
{
    let osString = Components.classes["@mozilla.org/xre/app-info;1"]
        .getService(Components.interfaces.nsIXULRuntime).OS;
    if(osString=="WINNT"){
	return true;
    }
    return false;
}

function IsDarwin()
{
    let osString = Components.classes["@mozilla.org/xre/app-info;1"]
        .getService(Components.interfaces.nsIXULRuntime).OS;
    if(osString=="Darwin"){
	return true;
    }
    return false;
}

function IsLinux()
{
    let osString = Components.classes["@mozilla.org/xre/app-info;1"]
        .getService(Components.interfaces.nsIXULRuntime).OS;
    if(osString=="Linux"){
	return true;
    }
    return false;
}
