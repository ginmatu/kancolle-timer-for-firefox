// vim: set ts=8 sw=4 sts=4 ff=dos :

Components.utils.import("chrome://kancolletimer/content/utils.jsm");

var Powerup = {
    _file: "chrome://kancolletimer/content/data/powerup.tsv",
    _url: 'https://docs.google.com/spreadsheets/d/1UOo5VpFZF-Ee_NUZ2T_ECMBajfIaIUkIs9xVYBkPcv4/export?format=tsv&id=1UOo5VpFZF-Ee_NUZ2T_ECMBajfIaIUkIs9xVYBkPcv4&gid=1529761687',

    _data: null,

    parse: function( text ){
	this._data = new Array();
	let rows = text.split( /\r\n|\n|\r/ );

	let flg = false;
	for( let row of rows ){
	    if( !flg ){
		// 1行目にはヘッダがあるだけなのでスキップ
		flg = true;
		continue;
	    }
	    let tmp = row.split( /\t/ );
	    if (tmp.length < 2)
		continue;
	    this._data.push( tmp );
	}
	return (flg ? 0 : -1) + this._data.length;
    },

    expand: function(){
	let elems = document.getElementsByClassName( 'on-off' );
	for( let elem of elems ){
	    elem.checked = true;
	}
    },
    collapse: function(){
	let elems = document.getElementsByClassName( 'on-off' );
	for( let elem of elems ){
	    elem.checked = false;
	}
    },

    today: function(){
	let now = new Date();
	$( 'calendar' ).value = now.getFullYear() + '-' + (now.getMonth()+1) + '-' + now.getDate();
	this.createView();
    },

    createView: function(){
	let date = $( 'calendar' ).dateValue;
	let dayofweek = date.getDay();

	let db = KanColleUtils.readObject('powerup.json', {});
	let pickup = new Object();
	let obj = {};
	for( let row of this._data ){
	    if( row[dayofweek + 1] ){
		let equip_name = row[0];
		let ship_name = row[8];

		if( db[dayofweek] && db[dayofweek][ship_name] &&
		    !db[dayofweek][ship_name][equip_name] )
		    continue;

		if( !pickup[equip_name] ){
		    pickup[equip_name] = new Array();
		}
		pickup[equip_name].push( ship_name );

		if(!obj[ship_name]) obj[ship_name]={};
		obj[ship_name][equip_name]=true;
	    }
	}

	if(db[dayofweek]){
	    for(let ship_name in db[dayofweek]){
		for(let equip_name in db[dayofweek][ship_name]){
		    if(obj[ship_name]&&obj[ship_name][equip_name])
			continue;
		    if( !pickup[equip_name] ){
			pickup[equip_name] = new Array();
		    }
		    pickup[equip_name].push( ship_name );
		    pickup[equip_name].sort();
		}
	    }
	}

	let body = $( 'body' );
	RemoveChildren( body );

	let cnt = 0;
	for( let k in pickup ){
	    let row = pickup[k];

	    let label = CreateHTMLElement( 'label' );
	    label.setAttribute( 'class', 'equip-name' );
	    label.setAttribute( 'for', '_' + cnt );
	    label.appendChild( document.createTextNode( k ) );

	    let checkbox = CreateHTMLElement( 'input' );
	    checkbox.setAttribute( 'type', 'checkbox' );
	    checkbox.setAttribute( 'id', '_' + cnt );
	    checkbox.setAttribute( 'class', 'on-off' );

	    let description = CreateElement( 'description' );

	    for( let shipname of row ){
		let name = CreateHTMLElement( 'li' );
		name.appendChild( document.createTextNode( shipname || "---" ) );

		if(db[dayofweek]&&db[dayofweek][shipname]&&db[dayofweek][shipname][k]){
		    for( let lv = 0; lv < 3; lv++ ){
			if(typeof(db[dayofweek][shipname][k][lv].api_req_fuel)==='number'){
			    let s = (lv==0?"★0～5":lv==1?"★6～9":"★max") + ": ";
			    if( db[dayofweek][shipname][k][lv].api_req_buildkit == 0 &&
				db[dayofweek][shipname][k][lv].api_certain_buildkit == 0 &&
				db[dayofweek][shipname][k][lv].api_req_remodelkit == 0 &&
				db[dayofweek][shipname][k][lv].api_certain_remodelkit == 0 &&
				db[dayofweek][shipname][k][lv].api_change_flag == 0 &&
				lv == 2 ){
				s += "---";
			    } else {
				s += db[dayofweek][shipname][k][lv].api_req_fuel + "/"
				   + db[dayofweek][shipname][k][lv].api_req_bull + "/"
				   + db[dayofweek][shipname][k][lv].api_req_steel + "/"
				   + db[dayofweek][shipname][k][lv].api_req_bauxite + " "
				   + (typeof(db[dayofweek][shipname][k][lv].api_req_buildkit)==='number' ? db[dayofweek][shipname][k][lv].api_req_buildkit : "-")
				   + (db[dayofweek][shipname][k][lv].api_certain_buildkit ? "("+db[dayofweek][shipname][k][lv].api_certain_buildkit+")" : "")
				   + "/"
				   + (typeof(db[dayofweek][shipname][k][lv].api_req_remodelkit)==='number' ? db[dayofweek][shipname][k][lv].api_req_remodelkit : "-")
				   + (db[dayofweek][shipname][k][lv].api_certain_remodelkit ? "("+db[dayofweek][shipname][k][lv].api_certain_remodelkit+")" : "");
				if( db[dayofweek][shipname][k][lv].api_req_slot_name &&
				db[dayofweek][shipname][k][lv].api_req_slot_num ){
				s += " "+db[dayofweek][shipname][k][lv].api_req_slot_name + "が"
				   + db[dayofweek][shipname][k][lv].api_req_slot_num + "個必要。";
				}
			    }
			    name.appendChild( CreateHTMLElement('br') );
			    name.appendChild( document.createTextNode( s ) );
			}
		    }
		}

		description.appendChild( name );
	    }

	    body.appendChild( label );
	    body.appendChild( checkbox );
	    body.appendChild( description );

	    cnt++;
	}
    },

    readFile: function(){
	let url = KanColleUtils.getUnicharPref('powerupdata.uri', this._file);
	let req = new XMLHttpRequest();
	if( !req ) return;

	debugprint('Powerup data: ' + url);

	req.open( 'GET', url );
	req.responseType = "text";

	req.onreadystatechange = function(){
	    if( req.readyState != 4 )
		return;
	    // req.status may become 0 for non-HTTP URLs.
	    // format will be checked later, but we do
	    // not have further data sources.
	    if ( req.status == 200 || req.status == 0 ){
		// GoogleDrive記載の装備名は
		// KanColleDatabase.masterShipのデータと
		// 異なるものがあるので調整が必要である
		let txt = req.responseText
		    .replace(/／/g, "/")
		    .replace(/10cm高角砲＋高射装置/g,"10cm連装高角砲+高射装置");
		Powerup.parse( txt );
		Powerup.createView();
		return;
	    }
	    debugprint('Failed to load powerup data.');
	};

	req.send( "" );
    },

    init: function(){
	this.readFile();
    }

};

window.addEventListener( "load", function( e ){
    Powerup.init();
}, false );
