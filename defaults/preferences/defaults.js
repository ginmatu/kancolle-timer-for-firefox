pref("extensions.kancolletimer.test",false);

pref("extensions.kancolletimer.time-offset", 0);

pref("extensions.kancolletimer.display.short",false);
pref("extensions.kancolletimer.display.font-color","");
pref("extensions.kancolletimer.display.font","");
pref("extensions.kancolletimer.display.font-size","");
pref("extensions.kancolletimer.display.ship-num-free",5);

pref("extensions.kancolletimer.default-action.toolbar", 0);

pref("extensions.kancolletimer.screenshot.path","");
pref("extensions.kancolletimer.screenshot.jpeg",false);
pref("extensions.kancolletimer.screenshot.mask-name",false);

pref("extensions.kancolletimer.window.auto-open",false);
pref("extensions.kancolletimer.window.auto-open-shiplist",false);

pref("extensions.kancolletimer.wallpaper","");
pref("extensions.kancolletimer.wallpaper.alpha",100);

pref("extensions.kancolletimer.sound.api",0);

pref("extensions.kancolletimer.sound.default","");
pref("extensions.kancolletimer.sound.damage-warning","");

pref("extensions.kancolletimer.sound.mission","");
pref("extensions.kancolletimer.sound.ndock","");
pref("extensions.kancolletimer.sound.kdock","");

pref("extensions.kancolletimer.sound.1min.mission","");
pref("extensions.kancolletimer.sound.1min.ndock","");
pref("extensions.kancolletimer.sound.1min.kdock","");

pref("extensions.kancolletimer.popup.general-timer",false);
pref("extensions.kancolletimer.popup.mission",false);
pref("extensions.kancolletimer.popup.ndock",false);
pref("extensions.kancolletimer.popup.kdock",false);
pref("extensions.kancolletimer.popup.damage-warning",false);
pref("extensions.kancolletimer.popup.1min-before",false);

pref("extensions.kancolletimer.twitter.screen_name","");
